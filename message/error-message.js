export const ErrorMessage = {
	/**
	 * 400:	Bad Request	잘못된 요청
	 * 401: Unauthorized	권한 없음
	 * 403: Forbidden	금지됨
	 * 452 ~ 499:	CUSTOME CODE
	 * 500:	Internal Server Error	내부 서버 오류
	 * 512 ~ 599:	CUSTOME CODE
	 */
	BAD_REQUEST: {
		status: 400,
		message: '잘못된 데이터입니다.',
	},
	EXPIRATION_TOKEN: {
		status: 401,
		message: '토큰 정보가 유효하지 않습니다.',
	},
	FORBIDDEN: {
		status: 403,
		message: '해당 요청에 대한 권한이 존재하지 않습니다.',
	},
	NOT_FOUND_TOKEN: {
		status: 404,
		message: '토큰이 존재하지 않습니다.',
	},
	NOT_ID_AND_PASSWORD: {
		status: 500,
		message: '아이디 또는 비밀번호가 잘못되었습니다.',
	},
	ACCOUNT_SUSPENDED: {
		status: 500,
		message: '해당 계정이 정지 상태 입니다.',
	},
	DUPLICATION_ID: {
		status: 512,
		message: '아이디가 존재합니다.',
	},
	EXPIRATION_PASSWORD: {
		status: 513,
		message: '비밀번호가 만료되었습니다.',
	},
};
