const ApiModel = {
	/** 기술수요조사서 - 공고 리스트 */
	DEMAND_NOTICE_LIST: 'DEMAND_NOTICE_LIST',
	/** 기술수요조사서 - 공고 별 접수서 건수 */
	DEMAND_SURVEY_COUNT: 'DEMAND_SURVEY_COUNT',
	/** 기술수요조사서 - 공고 상세정보 */
	DEMAND_NOTICE_DETAIL: 'DEMAND_NOTICE_DETAIL',
	/** 기술수요조사서 - 접수서 리스트 */
	DEMAND_SURVEY_LIST: 'DEMAND_SURVEY_LIST',
	/** 기술수요조사서 - 접수서 상세정보 */
	DEMAND_SURVEY_DETAIL: 'DEMAND_SURVEY_DETAIL',
	/** 기술수요조사서 - 그룹 리스트 */
	DEMAND_GROUP_LIST: 'DEMAND_GROUP_LIST',
	/** 기술수요조사서 - 그룹 접수서 리스트 */
	DEMAND_GROUP_SURVEY_LIST: 'DEMAND_GROUP_SURVEY_LIST',
	/** 10대 이슈 - 리스트 */
	ISSUE_LIST: 'ISSUE_LIST',
	/** 10대 이슈 - 상세정보 */
	ISSUE_DETAIL: 'ISSUE_DETAIL',
};

export default ApiModel;
