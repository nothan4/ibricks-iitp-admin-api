const FieldModel = {
	/** 기술수요조사서 연계대상 종류(ALL: 전체, EZONE: EZONE, IRIS: IRIS) */
	demandSystemTypes: ['ALL', 'EZONE', 'IRIS'],
	/** 기술수요조사서 공고 타입(REGULAR: 정기, IRREGULAR: 비정기, NONE: 해당없음) */
	demandNoticeTypeCodes: ['REGULAR', 'IRREGULAR', 'NONE'],
	demandNoticeTypeNames: ['정기', '비정기', '해당없음'],
	/** SURVEY_NAME: 접수명, ORGN_NAME: 기관명 */
	demandSurveyListSearchTypes: ['SURVEY_NAME', 'ORGN_NAME'],
	/** ICT 분류체계 타입 */
	ictCodeTypes: ['대', '중', '소', '세'],
	/** 계정 타입 */
	accountType: ['ADMIN', 'GUEST'],
};

export default FieldModel;
