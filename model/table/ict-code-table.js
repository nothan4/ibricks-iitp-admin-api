const IctCodeTable = {
	/** ICT 분류체계 TABLE */
	index: 'IITP_ICT_TCCLS_CD',
	/** ICT 기술 분류 코드 */
	ictTcclsCd: 'ICT_TCCLS_CD',
	/** ICT 상위 기술 분류 코드 */
	upprTcclsCd: 'UPPR_TCCLS_CD',
	/** ICT 기술 분류 명 */
	tcclsNm: 'TCCLS_NM',
	/** 레벨 */
	lv: 'LV',
	/** 분류년도 */
	year: 'YEAR',
	/** 유효 여부 */
	vlYn: 'VL_YN',
	/** 등록일 */
	regtDtm: 'REGT_DTM',
	/** 수록일 */
	mdfyDtm: 'MDFY_DTM',

	/** 상위 분류 명 */
	upprTcclsNm: 'UPPR_TCCLS_NM',
	/** 매핑 코드 */
	ictMappingCode: 'ICT_MAPPING_CODE',
	/** 매핑 명 */
	ictMappingName: 'ICT_MAPPING_NAME',
	/** 매핑 분류년도 */
	ictMappingYear: 'ICT_MAPPING_YEAR',
};

export default IctCodeTable;
