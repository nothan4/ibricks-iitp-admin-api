/** 첨부파일 */
const IitpAhflTable = {
	/** 기술수요조사서 공고  Index*/
	index: 'IITP_AHFL',
	fileId: 'FILE_ID',
	fileGroupId: 'FILE_GROUP_ID',
	fileTypeCd: 'FILE_TYPE_CODE',
	fileTypeNm: 'FILE_TYPE_NAME',
	deleteYn: 'DELETE_YN',
	originFileNm: 'ORIGIN_FILE_NAME',
	fileSize: 'FILE_SIZE',
	filePath: 'FILE_PATH',
	fileNm: 'FILE_NAME',
	fileExt: 'FILE_EXT',
	tskDgrId: 'TSK_DGR_ID',
};

export default IitpAhflTable;
