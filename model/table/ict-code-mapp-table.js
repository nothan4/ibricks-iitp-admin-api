const IctCodeTable = {
	/** ICT 분류체계 매핑 TABLE */
	index: 'IITP_ICT_CD_MAPP',
	/** ICT 기술 분류 코드 */
	code: 'CODE',
	/** ICT 매핑 기술 분류 코드 */
	upperCode: 'UPPER_CODE',
	/** 분류년도 */
	year: 'YEAR',
};

export default IctCodeTable;
