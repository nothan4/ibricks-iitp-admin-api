const DemandSurveyTable = {
	/** 기술수요조사서 접수서  Index*/
	index: 'DEMAND_SURVEY',
	/** 수요조사 공고 아이디 */
	dmsyPbncId: 'DMSY_PBNC_ID',
	/** 수요조사 접수 아이디 */
	dmsyRcipId: 'DMSY_RCIP_ID',
	/** 수요조사명 */
	dmndAplNm: 'DMND_APL_NM',
	/** 수요조사 진행상태 */
	prgsStcdNm: 'PRGS_STCD_NM',
	/** 수요조사 진행상태 코드 */
	prgsStcd: 'PRGS_STCD',
	/** 신청일시 */
	aplDtm: 'APL_DTM',
	/** 접수일시 */
	rcipDtm: 'RCIP_DTM',
	/** 기술성숙도(TRL) 코드(IRIS) */
	trlCd: 'TRL_CD',
	/** 기술성숙도(TRL) 명(IRIS) */
	trlNm: 'TRL_NM',
	/** 과학기술 소분류 코드 */
	tecSclsCd: 'TEC_SCLS_CD',
	/** 과학기술 소분류명 */
	tecSclsNm: 'TEC_SCLS_NM',
	/** ICT 기술 중분류 코드 */
	ictTecLclsCd: 'ICT_TEC_LCLS_CD',
	/** ICT 기술 소분류 코드 */
	ictTecMclsCd: 'ICT_TEC_MCLS_CD',
	/** ICT 기술 세분류 코드 */
	ictTecSclsCd: 'ICT_TEC_SCLS_CD',
	/** ICT 기술 확정 세분류 코드(관리자 입력) */
	ictTecConfirmCd: 'ICT_TEC_CONFIRM_CD',
	/** ICT 기술 확정 세분류 명(관리자 입력) */
	ictTecConfirmNm: 'ICT_TEC_CONFIRM_NM',
	/** 사업기획 기술 분류 코드(TF팀 수기 가공) */
	bizTecCd: 'BIZ_TEC_CD',
	/** 사업기획 기술 분류명(TF팀 수기 가공) */
	bizTecNm: 'BIZ_TEC_NM',
	/** 연구개발내용(IRIS) */
	rndOtlnCn: 'RND_OTLN_CN',
	/** 개인 식별자 */
	indvId: 'INDV_ID',
	/** 개인명 */
	indvNm: 'INDV_NM',
	/** 기관 식별자 */
	instId: 'INST_ID',
	/** 기관명 */
	instNm: 'INST_NM',
	/** 병합수요 그룹 식별자 */
	groupId: 'GROUP_ID',
	/** 병합수요 그룹 대표 유무 */
	groupLeaderYn: 'GROUP_LEADER_YN',
	/** 등록일 */
	regtDtm: 'REGT_DTM',
	/** 수정일 */
	mdfyDtm: 'MDFY_DTM',
	/** 첨부파일 식별자 */
	fileGroupId: 'FILE_GROUP_ID',
	/** 관리자 데이터 생성 유무 */
	adminInputYn: 'ADMIN_INPUT_YN',
	/** 수요조사 공고 삭제 유무 */
	deleteYn: 'DELETE_YN',
};

export default DemandSurveyTable;
