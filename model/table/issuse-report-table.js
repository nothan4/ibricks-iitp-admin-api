const IssueReportTable = {
	/** 10대이슈 TABLE */
	index: 'ISSUE_REPORT',
	/** 고유 식별자 */
	issueId: 'ISSUE_ID',
	/** 제목 */
	title: 'TITLE',
	/** 요약 */
	abstract: 'ABSTRACT',
	/** 10대 이슈 키워드 */
	keywords: 'KEYWORDS',
	/** 발행일 */
	publishDate: 'PUBLISH_DATE',
	/** 년도 */
	year: 'YEAR',
	/** 삭제유무 */
	deleteFlag: 'DELETE_FLAG',
	/** 파일 고유 식별자(파일명) */
	fileId: 'FILE_ID',
	/** 원본 파일명 */
	originFileName: 'ORIGIN_FILE_NAME',
	/** 파일 경로 */
	filePath: 'FILE_PATH',
	/** 파일 확장자 */
	fileExt: 'FILE_EXT',
	/** 등록일 */
	regDate: 'REG_DATE',
	/** 수정일 */
	modifiDate: 'MODIFI_DATE',
};

export default IssueReportTable;
