const DemandNoticeTable = {
	/** 기술수요조사서 공고  Index*/
	index: 'DEMAND_PBNC',
	/** 수요조사 공고 아이디 */
	dmsyPbncId: 'DMSY_PBNC_ID',
	/** 수요조사 공고 제목 */
	pbncNm: 'PBNC_NM',
	/** 수요조사 공고 년도 */
	pbncYy: 'PBNC_YY',
	/** 수요조사 공고 시작일 */
	pbncBegdtm: 'PBNC_BEGDTM',
	/** 수요조사 공고 종료일 */
	pbncEnddtm: 'PBNC_ENDDTM',
	/** 수요조사 공고 타입 코드 */
	typeCode: 'TYPE_CODE',
	/** 수요조사 공고 타입 명 */
	typeName: 'TYPE_NAME',
	/** 수요조사 공고 사업 기획 유무 */
	bizPlanYn: 'BIZ_PLAN_YN',
	/** 수요조사 공고 유무 */
	pbncYn: 'PBNC_YN',
	/** 수요조사 공고 삭제 유무 */
	deleteYn: 'DELETE_YN',
	/** 수요조사 공고 연계 대상 시스템 타입 */
	systemType: 'SYSTEM_TYPE',
	/** 수요조사 공고 등록일 */
	regtDtmDt: 'REGT_DTM',
	/** 수요조사 공고 수정일 */
	mdfyDtmDt: 'MDFY_DTM',
	/** 관리자 데이터 생성 유무 */
	adminInputYn: 'ADMIN_INPUT_YN',
};

export default DemandNoticeTable;
