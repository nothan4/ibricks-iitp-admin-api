const IssueReportIndex = {
	/** 10대이슈 TABLE */
	index: 'issue_report',
	/** 고유 식별자 */
	issueId: 'ISSUE_ID_k',
	/** 제목 */
	title: 'TITLE_kskn',
	/** 제목(keyword) */
	titleKeyword: 'TITLE_kskn.keyword',
	/** 제목(kobrick) */
	titleKobrick: 'TITLE_kskn.kobrick',
	/** 제목(ngram) */
	titleNgram: 'TITLE_kskn.ngram',
	/** 제목(standard) */
	titleStandard: 'TITLE_kskn.standard',
	/** 요약 */
	abstract: 'ABSTRACT_ko',
	/** 10대 이슈 키워드 */
	keywords: 'KEYWORDS_k',
	/** 발행일 */
	publishDate: 'PUBLISH_DATE_dt',
	/** 년도 */
	year: 'YEAR_k',
	/** 삭제유무 */
	deleteFlag: 'DELETE_FLAG_k',
	/** 파일 고유 식별자(파일명) */
	fileId: 'FILE_ID_k',
	/** 원본 파일명 */
	originFileName: 'ORIGIN_FILE_NAME_k',
	/** 파일 경로 */
	filePath: 'FILE_PATH_k',
	/** 파일 확장자 */
	fileExt: 'FILE_EXT_k',
	/** 등록일 */
	regDate: 'REG_DATE_dt',
	/** 수정일 */
	modifiDate: 'MODIFI_DATE_dt',
};

export default IssueReportIndex;
