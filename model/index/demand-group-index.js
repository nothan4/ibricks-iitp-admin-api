const DemandGroupIndex = {
	/** 기술수요조사서 접수서  Index*/
	index: 'demand_group',
	/** 기술수요조사서 그룹 아아디 */
	groupId: 'GROUP_ID_k',
	/** 기술수요조사서 그룹 명 */
	groupNm: 'GROUP_NM_k',
	/** 수요조사 공고 아이디 */
	dmsyPbncId: 'DMSY_PBNC_ID_k',
	/** 등록일 */
	regDt: 'REG_dt',
	/** 수정일 */
	updateDt: 'UPDATE_dt',
};

export default DemandGroupIndex;
