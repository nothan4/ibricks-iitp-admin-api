const DemandNoticeIndex = {
	/** 기술수요조사서 공고  Index*/
	index: 'demand_notice',
	/** 수요조사 공고 아이디 */
	dmsyPbncId: 'DMSY_PBNC_ID_k',
	/** 수요조사 공고 제목 */
	pbncNm: 'PBNC_NM_kskn',
	/** 수요조사 공고 제목(keyword) */
	pbncNmKeyword: 'PBNC_NM_kskn.keyword',
	/** 수요조사 공고 제목(kobrick) */
	pbncNmKobrick: 'PBNC_NM_kskn.kobrick',
	/** 수요조사 공고 제목(ngram) */
	pbncNmNgram: 'PBNC_NM_kskn.ngram',
	/** 수요조사 공고 제목(standard) */
	pbncNmStandard: 'PBNC_NM_kskn.standard',
	/** 수요조사 공고 년도 */
	pbncYy: 'PBNC_YY_k',
	/** 수요조사 공고 시작일 */
	pbncBegdtm: 'PBNC_BEGDTM_dt',
	/** 수요조사 공고 종료일 */
	pbncEnddtm: 'PBNC_ENDDTM_dt',
	/** 수요조사 공고 타입 코드 */
	typeCode: 'TYPE_CODE_k',
	/** 수요조사 공고 타입 명 */
	typeName: 'TYPE_NAME_k',
	/** 수요조사 공고 사업 기획 유무 */
	bizPlanYn: 'BIZ_PLAN_YN_k',
	/** 수요조사 공고 유무 */
	pbncYn: 'PBNC_YN_k',
	/** 수요조사 공고 삭제 유무 */
	deleteYn: 'DELETE_YN_k',
	/** 수요조사 공고 연계 대상 시스템 타입 */
	systemType: 'SYSTEM_TYPE_k',
	/** 수요조사 공고 등록일 */
	regtDtmDt: 'REGT_DTM_dt',
	/** 수요조사 공고 수정일 */
	mdfyDtmDt: 'MDFY_DTM_dt',
	/** 관리자 데이터 생성 유무 */
	adminInputYn: 'ADMIN_INPUT_YN_k',
};

export default DemandNoticeIndex;
