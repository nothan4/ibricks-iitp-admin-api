const DemandSurveyIndex = {
	/** 기술수요조사서 접수서  Index*/
	index: 'demand_survey',
	/** 기술수요조사서 접수서  pk*/
	pk: '_id',
	/** 기술수요조사서 공고 식별자 */
	dmsyPbncId: 'DMSY_PBNC_ID_k',
	/** 기술수요조사서 접수서 식별자 */
	dmsyRcipId: 'DMSY_RCIP_ID_k',
	/** 기술수요조사서 접수서 접수 명 */
	dmndAplNm: 'DMND_APL_NM_kskn',
	/** 기술수요조사서 접수서 접수 명 - keyword*/
	dmndAplNmKeyword: 'DMND_APL_NM_kskn.keyword',
	/** 기술수요조사서 접수서 접수 명 - kobrick */
	dmndAplNmKobrick: 'DMND_APL_NM_kskn.kobrick',
	/** 기술수요조사서 접수서 접수 명 - standard*/
	dmndAplNmStandard: 'DMND_APL_NM_kskn.standard',
	/** 기술수요조사서 접수서 접수 명 - ngram*/
	dmndAplNmNgram: 'DMND_APL_NM_kskn.ngram',
	/** 기술수요조사서 접수서 진행상태 명 */
	prgsStNm: 'PRGS_ST_NM_k',
	/** 기술수요조사서 접수서 진행상태 코드 */
	prgsStCd: 'PRGS_ST_CD_k',
	/** 기술수요조사서 접수서 신청일 */
	aplDt: 'APL_dt',
	/** 기술수요조사서 접수서 접수일 */
	rcipDt: 'RCIP_DTM_dt',
	/** 기술수요조사서 접수서 과학기술 소분류 코드 */
	tecSclsCd: 'TEC_SCLS_CD_k',
	/** 기술수요조사서 접수서 과학기술 소분류 명 */
	tecSclsNm: 'TEC_SCLS_NM_k',
	/** 기술수요조사서 접수서 ICT 기술 중분류 코드 */
	ictTecLclsCd: 'ICT_TEC_LCLS_CD_k',
	/** 기술수요조사서 접수서 ICT 기술  중분류 명 */
	ictTecLclsNm: 'ICT_TEC_LCLS_NM_k',
	/** 기술수요조사서 접수서 ICT 기술 소분류 코드 */
	ictTecMclsCd: 'ICT_TEC_MCLS_CD_k',
	/** 기술수요조사서 접수서 ICT 기술 소분류 명 */
	ictTecMclsNm: 'ICT_TEC_MCLS_NM_k',
	/** 기술수요조사서 접수서 ICT 기술 세분류 코드 */
	ictTecSclsCd: 'ICT_TEC_SCLS_CD_k',
	/** 기술수요조사서 접수서 ICT 기술 세분류 명 */
	ictTecSclsNm: 'ICT_TEC_SCLS_NM_k',
	/** 기술수요조사서 접수서 최신 ICT 기술 대분류 코드 */
	newIctTecBclsCd: 'NEW_ICT_TEC_BCLS_CD_k',
	/** 기술수요조사서 접수서 최신 ICT 기술 대분류 명 */
	newIctTecBclsNm: 'NEW_ICT_TEC_BCLS_NM_k',
	/** 기술수요조사서 접수서 최신 ICT 기술 중분류 코드 */
	newIctTecLclsCd: 'NEW_ICT_TEC_LCLS_CD_k',
	/** 기술수요조사서 접수서 최신 ICT 기술 중분류 명 */
	newIctTecLclsNm: 'NEW_ICT_TEC_LCLS_NM_k',
	/** 기술수요조사서 접수서 최신 ICT 기술 소분류 코드 */
	newIctTecMclsCd: 'NEW_ICT_TEC_MCLS_CD_k',
	/** 기술수요조사서 접수서 최신 ICT 기술 소분류 명 */
	newIctTecMclsNm: 'NEW_ICT_TEC_MCLS_NM_k',
	/** 기술수요조사서 접수서 최신 ICT 기술 세분류 코드 */
	newIctTecSclsCd: 'NEW_ICT_TEC_SCLS_CD_k',
	/** 기술수요조사서 접수서 최신 ICT 기술 세분류 명 */
	newIctTecSclsNm: 'NEW_ICT_TEC_SCLS_NM_k',
	/** 추천 ICT 기술 세분류 코드 - 1순위 */
	recommend1: 'recommendIctCode1',
	recommendIctTecSclsCd1: 'RECOMMEND_ICT_TEC_SCLS_CD_k',
	/** 추천 ICT 기술 세분류 명 - 1순위 */
	recommendIctTecSclsNm1: 'RECOMMEND_ICT_TEC_SCLS_NM_k',
	/** 추천 ICT 기술 세분류 코드 - 2순위 */
	recommend2: 'recommendIctCode2',
	/** 추천 ICT 기술 세분류 코드 - 2순위 */
	recommendIctTecSclsCd2: 'RECOMMEND_ICT_TEC_SCLS_CD_k',
	/** 추천 ICT 기술 세분류 명 - 2순위 */
	recommendIctTecSclsNm2: 'RECOMMEND_ICT_TEC_SCLS_NM_k',
	/** 추천 ICT 기술 세분류 코드 - 3순위 */
	recommend3: 'recommendIctCode3',
	/** 추천 ICT 기술 세분류 코드 - 3순위 */
	recommendIctTecSclsCd3: 'RECOMMEND_ICT_TEC_SCLS_CD_k',
	/** 추천 ICT 기술 세분류 명 - 3순위 */
	recommendIctTecSclsNm3: 'RECOMMEND_ICT_TEC_SCLS_NM_k',
	/** ICT 기술 확정 세분류 코드(관리자 입력) */
	ictTecConfirmCd: 'ICT_TEC_CONFIRM_CD_k',
	/** ICT 기술 확정 세분류 명(관리자 입력) */
	ictTecConfirmNm: 'ICT_TEC_CONFIRM_NM_k',
	/** 사업기획 기술 분류 코드(TF팀 수기 가공) */
	bizTecCd: 'BIZ_TEC_CD_k',
	/** 사업기획 기술 분류명(TF팀 수기 가공) */
	bizTecNm: 'BIZ_TEC_NM_k',
	/** 기술수요조사서 접수서 연구 개발 내용 */
	rndOtlnCn: 'RND_OTLN_CN_ko',
	/** 연구자 식별자 */
	indvId: 'INDV_ID_k',
	/** 연구자 명 */
	indvNm: 'INDV_NM_k',
	/** 기관 식별자 */
	instId: 'INST_ID_k',
	/** 기관 명 */
	instNm: 'INST_NM_ksk',
	/** 기관 명  - keyword */
	instNmKeyword: 'INST_NM_ksk.keyword',
	/** 기관 명  - kobrick */
	instNmKobrick: 'INST_NM_ksk.kobrick',
	/** 기관 명  - standard */
	instNmKtandard: 'INST_NM_ksk.standard',
	/** 등록일 */
	regtDt: 'REGT_DTM_dt',
	/** 기술성숙도 코드 */
	trlCd: 'TRL_CD_k',
	/** 기술성숙도 명 */
	trlNm: 'TRL_NM_k',
	/** 기술수요조사서 공고 명 */
	dmndNm: 'DMND_NM_kskn',
	/** 기술수요조사서 공고 년도 */
	pbncYy: 'PBNC_YY_k',
	/** 기술수요조사서 공고 시작일 */
	pbncBegdtmDt: 'PBNC_BEGDTM_dt',
	/** 기술수요조사서 공고 종료일 */
	pbncEnddtmDt: 'PBNC_ENDDTM_dt',
	/** 기술수요조사서 공고 타입 */
	typeCode: 'TYPE_CODE_k',
	/** 기술수요조사서 공고 타입 명 */
	typeName: 'TYPE_NAME_k',
	/** 기술수요조사서 삭제여부 */
	deleteYn: 'DELETE_YN_k',
	/** 병합수요 그룹 식별자 */
	groupId: 'GROUP_ID_k',
	/** 병합수요 그룹 대표 유무 */
	groupLeaderYn: 'GROUP_LEADER_YN_k',
	/** 관리자 데이터 생성 유무 */
	adminInputYn: 'ADMIN_INPUT_YN_k',
	/** 수요조사 공고 삭제 유무 */
	deleteYn: 'DELETE_YN_k',
	/** 첨부파일 파일 */
	fileList: 'FILE_LIST',
	/** 첨부파일 파일 경로 */
	filePath: 'FILE_LIST.FILE_PATH_k',
	/** 첨부파일 그룹 아이디 */
	fileGroupId: 'FILE_GROUP_ID_k',
};

export default DemandSurveyIndex;
