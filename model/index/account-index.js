const AccountIndex = {
	/** 10대이슈 TABLE */
	index: 'account',
	pk: 'pk_k',
	id: 'id_k',
	password: 'password_k',
	madeby: 'madeby_k',
	salt: 'salt_k',
	name: 'name_ksk',
	nameKeyword: 'name_ksk.keyword',
	nameKobrick: 'name_ksk.kobrick',
	menuAuth: 'menu_auth_k',
	regDate: 'reg_dt',
	activeStartDate: 'active_start_dt',
	activeEndDate: 'active_end_dt',
	activated: 'activated_k',
	passwordUpdateDate: 'password_update_dt',
};

export default AccountIndex;
