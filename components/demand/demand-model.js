import dayjs from 'dayjs';
import config from 'config';
import * as indexConfig from './config/index-config.js';
import { search, update, updateByQuery, write, elDelete, upsert } from '../../lib/elasticsearch/client.js';
import dbConnection from '../../lib/knex/db-connection.js';
import { isEmpty, uuid } from '../../util/index.js';
import {
	ApiModel,
	FieldModel,
	DemandNoticeTable,
	DemandNoticeIndex,
	DemandSurveyTable,
	DemandSurveyIndex,
	DemandGroupIndex,
	IitpAhflTable,
} from '../../model/index.js';

/** 필드 정보 */
const demandConfig = indexConfig.demandConfig();

/**  리스트 */
export const demandNotices = async (params) => {
	const { size, page, systemType, startDate, endDate, noticeName } = params;
	const demandField = demandConfig[ApiModel.DEMAND_NOTICE_LIST];
	const dynamicMustQuery = [];

	// 시스템 타입 Match Query
	const systemTypeMatchQuery =
		systemType === 'ALL'
			? null
			: {
					match: {
						[demandField.match.systemType]: systemType,
					},
			  };
	if (systemTypeMatchQuery != null) dynamicMustQuery.push(systemTypeMatchQuery);

	// 공고 시작일 Range Query
	const startRangeQuery =
		startDate === ''
			? null
			: {
					range: {
						[demandField.startRange]: {
							gte: startDate,
						},
					},
			  };
	if (startRangeQuery != null) dynamicMustQuery.push(startRangeQuery);

	// 공고 종료일 Range Query
	const endRangeQuery =
		endDate === ''
			? null
			: {
					range: {
						[demandField.endRange]: {
							lte: endDate,
						},
					},
			  };
	if (endRangeQuery != null) dynamicMustQuery.push(endRangeQuery);

	// 공고명 Simple Query
	const noticeNameSimpleQuery =
		noticeName === ''
			? null
			: {
					simple_query_string: {
						query: noticeName,
						fields: demandField.simpleQuery,
					},
			  };
	if (noticeNameSimpleQuery != null) dynamicMustQuery.push(noticeNameSimpleQuery);

	// 공고 목록 Query
	const demandNoitceListQuery = {
		index: demandField.index,
		body: {
			size: size,
			from: (page - 1) * size,
			query: {
				bool: {
					must: dynamicMustQuery,
				},
			},
			sort: {
				[demandField.sort]: {
					order: 'desc',
				},
			},
			_source: demandField.source,
		},
	};

	const searchResult = await search(demandNoitceListQuery);

	return { searchResult };
};

/**  공고별 접수서 카운트 */
export const demandNoticeSurveyCount = async (noticeIds) => {
	const demandField = demandConfig[ApiModel.DEMAND_SURVEY_COUNT];

	const noticeIdsLength = noticeIds.length;

	/** 공고 별 접수서 쿼리 */
	const surveyCountQuery = {
		index: demandField.index,
		body: {
			size: 0,
			query: {
				bool: {
					filter: {
						terms: {
							[demandField.filter]: noticeIds,
						},
					},
				},
			},
			aggs: {
				result: {
					terms: {
						field: demandField.aggregation,
						size: noticeIdsLength,
					},
				},
			},
		},
	};

	const searchResult = await search(surveyCountQuery);

	return { searchResult };
};

/**  공고 - 상세정보 */
export const demandNotice = async (noticeId) => {
	const demandField = demandConfig[ApiModel.DEMAND_NOTICE_DETAIL];

	/** 공고 정보 Query */
	const demandNoitceDetailQuery = {
		index: demandField.index,
		body: {
			query: {
				bool: {
					filter: [
						{
							term: {
								[demandField.filter]: noticeId,
							},
						},
					],
				},
			},
			_source: demandField.source,
		},
	};

	const searchResult = await search(demandNoitceDetailQuery);

	return { searchResult };
};

/**  공고 - 등록(MariaDB) */
export const demandNoticeDbUpsert = async (excelDataList) => {
	const inputDataList = [];
	const ids = [];
	for (const cnt in excelDataList) {
		const objItem = excelDataList[cnt];
		const dmsyPbncId = objItem.dmsyPbncId;
		const pbncNm = objItem.pbncNm;
		const pbncYy = objItem.pbncYy;
		const typeName = objItem.typeName;
		const typeIndex = isEmpty(typeName)
			? null
			: FieldModel.demandNoticeTypeNames.findIndex((value) => value == typeName);
		const typeCode = FieldModel.demandNoticeTypeCodes[typeIndex];
		const pbncBegdtm = objItem.pbncBegdtm;
		const pbncEnddtm = objItem.pbncEnddtm;
		const pbncYn = objItem.pbncYn;
		const deleteYn = objItem.deleteYn;
		const systemType = objItem.systemType;
		const bizPlanYn = objItem.bizPlanYn;
		const object = {
			[DemandNoticeTable.dmsyPbncId]: dmsyPbncId,
			[DemandNoticeTable.pbncNm]: isEmpty(pbncNm) ? null : pbncNm,
			[DemandNoticeTable.pbncYy]: isEmpty(pbncYy) ? null : pbncYy,
			[DemandNoticeTable.typeCode]: isEmpty(typeCode) ? null : typeCode,
			[DemandNoticeTable.typeName]: isEmpty(typeName) ? null : typeName,
			[DemandNoticeTable.pbncBegdtm]: isEmpty(pbncBegdtm) ? null : pbncBegdtm,
			[DemandNoticeTable.pbncEnddtm]: isEmpty(pbncEnddtm) ? null : pbncEnddtm,
			[DemandNoticeTable.pbncYn]: isEmpty(pbncYn) ? null : pbncYn,
			[DemandNoticeTable.deleteYn]: isEmpty(deleteYn) ? null : deleteYn,
			[DemandNoticeTable.systemType]: isEmpty(systemType) ? null : systemType,
			[DemandNoticeTable.bizPlanYn]: isEmpty(bizPlanYn) ? null : bizPlanYn,
		};
		ids.push(dmsyPbncId);
		inputDataList.push(object);
	}

	const result = await dbConnection.transaction(async (trx) => {
		await trx(DemandNoticeTable.index).insert(inputDataList).onConflict([DemandNoticeTable.dmsyPbncId]).merge();
		const selectedData = await trx(DemandNoticeTable.index).select('*').whereIn(DemandNoticeTable.dmsyPbncId, ids);
		return selectedData;
	});

	return result;
};

/**  공고 - 등록(EL) */
export const demandNoticeElUpsert = async (dbResult) => {
	for (const cnt in dbResult) {
		const item = dbResult[cnt];
		const index = DemandNoticeIndex.index;
		const _id = item[DemandNoticeTable.dmsyPbncId];
		const payload = {
			[DemandNoticeIndex.dmsyPbncId]: _id,
			[DemandNoticeIndex.pbncNm]: item[DemandNoticeTable.pbncNm],
			[DemandNoticeIndex.pbncYy]: item[DemandNoticeTable.pbncYy],
			[DemandNoticeIndex.pbncBegdtm]: item[DemandNoticeTable.pbncBegdtm],
			[DemandNoticeIndex.pbncEnddtm]: item[DemandNoticeTable.pbncEnddtm],
			[DemandNoticeIndex.typeCode]: item[DemandNoticeTable.typeCode],
			[DemandNoticeIndex.typeName]: item[DemandNoticeTable.typeName],
			[DemandNoticeIndex.bizPlanYn]: item[DemandNoticeTable.bizPlanYn],
			[DemandNoticeIndex.pbncYn]: item[DemandNoticeTable.pbncYn],
			[DemandNoticeIndex.deleteYn]: item[DemandNoticeTable.deleteYn],
			[DemandNoticeIndex.systemType]: item[DemandNoticeTable.systemType],
			[DemandNoticeIndex.adminInputYn]: item[DemandNoticeTable.adminInputYn],
			[DemandNoticeIndex.regtDtmDt]: item[DemandNoticeTable.regtDtmDt],
			[DemandNoticeIndex.mdfyDtmDt]: item[DemandNoticeTable.mdfyDtmDt],
		};
		await upsert(index, _id, payload);
	}
};

/**  공고 - 수정(MariaDB) */
export const demandNoticeDbUpdate = async (noticeId, updateParams) => {
	const { systemType, bizPlanYn, typeCode, typeName } = updateParams;

	// 업데이트
	const result = await dbConnection(DemandNoticeTable.index)
		.where(DemandNoticeTable.dmsyPbncId, '=', noticeId)
		.update({
			[DemandNoticeTable.systemType]: systemType,
			[DemandNoticeTable.bizPlanYn]: bizPlanYn,
			[DemandNoticeTable.typeCode]: typeCode,
			[DemandNoticeTable.typeName]: typeName,
		});

	return result;
};

/**  공고 - 수정(EL) */
export const demandNoticeElUpdate = async (noticeId, updateParams) => {
	const { systemType, bizPlanYn, typeCode, typeName } = updateParams;
	const index = DemandNoticeIndex.index;
	const _id = noticeId;
	const payload = {
		[DemandNoticeIndex.systemType]: systemType,
		[DemandNoticeIndex.bizPlanYn]: bizPlanYn,
		[DemandNoticeIndex.typeCode]: typeCode,
		[DemandNoticeIndex.typeName]: typeName,
	};
	const result = await update(index, _id, payload);

	return result;
};

/** 삭제가능 플래그 유무 공고 조회 */
export const findByAdminInputYnNotice = async (noticeId) => {
	const data = await dbConnection(DemandNoticeTable.index)
		.select(DemandNoticeTable.adminInputYn)
		.where(DemandNoticeTable.dmsyPbncId, '=', noticeId);

	return data;
};

/** 공고 - 삭제 플래그 업데이트(MariaDB) */
export const updateByDeleteFlagNoticeDb = async (noticeId) => {
	const result = await dbConnection(DemandNoticeTable.index)
		.where(DemandNoticeTable.dmsyPbncId, '=', noticeId)
		.update({
			[DemandNoticeTable.deleteYn]: 'Y',
		});

	return result;
};

/** 공고 - 삭제 플래그 업데이트(El) */
export const updateByDeleteFlagNoticeEl = async (noticeId) => {
	const index = DemandNoticeIndex.index;
	const _id = noticeId;
	const payload = {
		[DemandNoticeIndex.deleteYn]: 'Y',
	};
	const result = await update(index, _id, payload);

	return result;
};

/**  접수서 - 리스트 */
export const demandSurveys = async (params) => {
	const { size, page, searchType, searchKeyword, groupType, noticeId } = params;
	const demandField = demandConfig[ApiModel.DEMAND_SURVEY_LIST];
	const dynamicMustQuery = [];
	const dynamicMustNotQuery = [];
	// 병합 수요 그룹 여부 Match Query
	const groupTypeQuery =
		groupType === 'ALL'
			? null
			: {
					exists: {
						field: demandField.exists,
					},
			  };
	if (groupTypeQuery != null) dynamicMustNotQuery.push(groupTypeQuery);

	const noticeIdQuery = isEmpty(noticeId) ? null : { match: { [demandField.match]: noticeId } };
	if (noticeIdQuery != null) dynamicMustQuery.push(noticeIdQuery);

	// 접수명 Simple Query
	const serachkeywordSimpleQuery = isEmpty(searchKeyword)
		? null
		: {
				simple_query_string: {
					query: searchKeyword,
					fields: demandField.simpleQuery[searchType],
				},
		  };
	if (serachkeywordSimpleQuery != null) dynamicMustQuery.push(serachkeywordSimpleQuery);

	let demandSurveyListQuery;
	if (groupType === 'ALL') {
		demandSurveyListQuery = {
			index: demandField.index,
			body: {
				size: size,
				from: (page - 1) * size,
				sort: {
					[demandField.sort]: {
						order: 'desc',
					},
				},
				query: {
					bool: {
						must: dynamicMustQuery,
					},
				},
				_source: demandField.source,
			},
		};
	} else if (groupType === 'NONE') {
		demandSurveyListQuery = {
			index: demandField.index,
			track_total_hits: true,
			body: {
				size: 10000,
				sort: {
					[demandField.sort]: {
						order: 'desc',
					},
				},
				query: {
					bool: {
						must: dynamicMustQuery,
						must_not: dynamicMustNotQuery,
					},
				},
				_source: demandField.source,
			},
		};
	}
	const searchResult = await search(demandSurveyListQuery);

	return { searchResult };
};

/**  접수서 - 상세정보 */
export const demandSurvey = async (surveyId) => {
	const demandField = demandConfig[ApiModel.DEMAND_SURVEY_DETAIL];

	const query = {
		index: demandField.index,
		body: {
			query: {
				bool: {
					filter: [
						{
							term: {
								[demandField.filter]: surveyId,
							},
						},
					],
				},
			},
			_source: demandField.source,
		},
	};

	const searchResult = await search(query);

	return { searchResult };
};

/** 접수서 - 등록(MariaDB) */
export const demandSurveyDbUpsert = async (dataListSheet1, dataListSheet2) => {
	/** true: 접수서, 첨부파일 등록 성공, false: 실패 */
	const result = {
		status: false,
		surveyList: [],
		surveyAhflList: [],
	};
	const surveyEzoneFiltPath = config.get('upload.surveyEzoneFiltPath') + dayjs().format('YYYY/MM') + '/';
	const surveyIrisFiltPath = config.get('upload.surveyIrisFiltPath') + dayjs().format('YYYY/MM') + '/';
	// 1. 접수서
	for (const cnt in dataListSheet1) {
		const item = dataListSheet1[cnt];
		const systemType = isEmpty(item.systemType) ? item.systemType : String(item.systemType).trim().toUpperCase();
		const dmsyPbncId = systemType === 'IRIS' ? `${systemType}_${item.dmsyPbncId}` : item.dmsyPbncId;
		const dmsyRcipId = systemType === 'IRIS' ? `${systemType}_${item.dmsyRcipId}` : item.dmsyRcipId;
		const fileGroupId = `${dmsyPbncId}_${item.dmsyRcipId}`;
		const dmndAplNm = item.dmndAplNm;
		const prgsStNm = item.prgsStNm;
		const prgsStCd = item.prgsStCd;
		const rndOtlnCn = item.rndOtlnCn;
		const tecSclsCd = item.tecSclsCd;
		const tecSclsNm = item.tecSclsNm;
		const trlCd = item.trlCd;
		const trlNm = item.trlNm;
		const ictTecSclsCd = item.ictTecSclsCd;
		const indvId = item.indvId;
		const indvNm = item.indvNm;
		const instId = item.instId;
		const instNm = item.instNm;
		const rcipDt = dayjs(item.rcipDt).format('YYYY-MM-DD');
		const aplDt = dayjs(item.aplDt).format('YYYY-MM-DD');
		const obj = {
			[DemandSurveyTable.dmsyPbncId]: dmsyPbncId,
			[DemandSurveyTable.dmsyRcipId]: dmsyRcipId,
			[DemandSurveyTable.fileGroupId]: fileGroupId,
			[DemandSurveyTable.dmndAplNm]: dmndAplNm,
			[DemandSurveyTable.prgsStcd]: prgsStCd,
			[DemandSurveyTable.prgsStcdNm]: prgsStNm,
			[DemandSurveyTable.rndOtlnCn]: rndOtlnCn,
			[DemandSurveyTable.tecSclsCd]: tecSclsCd,
			[DemandSurveyTable.tecSclsNm]: tecSclsNm,
			[DemandSurveyTable.trlCd]: trlCd,
			[DemandSurveyTable.trlNm]: trlNm,
			[DemandSurveyTable.ictTecSclsCd]: ictTecSclsCd,
			[DemandSurveyTable.indvId]: indvId,
			[DemandSurveyTable.indvNm]: indvNm,
			[DemandSurveyTable.instId]: instId,
			[DemandSurveyTable.instNm]: instNm,
			[DemandSurveyTable.rcipDtm]: rcipDt,
			[DemandSurveyTable.aplDtm]: aplDt,
			[DemandSurveyTable.adminInputYn]: 'Y',
		};
		result.surveyList.push(obj);
	}
	const surveyResult = await dbConnection(DemandSurveyTable.index)
		.insert(result.surveyList)
		.onConflict([DemandSurveyTable.dmsyPbncId, DemandSurveyTable.dmsyRcipId])
		.merge();
	if (surveyResult[0] === 0) {
		// 2. 접수서 첨부파일
		for (const cnt in dataListSheet2) {
			const item = dataListSheet2[cnt];
			const systemType = isEmpty(item.systemType) ? item.systemType : String(item.systemType).trim().toUpperCase();
			const dmsyPbncId = systemType === 'IRIS' ? `${systemType}_${item.dmsyPbncId}` : item.dmsyPbncId;
			const dmsyRcipId = item.dmsyRcipId;
			const fileId = `${dmsyPbncId}_${dmsyRcipId}`;
			const fileGroupId = fileId;
			const fileName = item.fileName;
			const fileOriginName = fileName;
			const fileExt = fileName.substring(fileName.lastIndexOf('.') + 1, fileName.length).toLowerCase();
			const filePath = systemType === 'IRIS' ? surveyIrisFiltPath : surveyEzoneFiltPath;
			const newFileName = `${fileId}.${fileExt}`;
			const obj = {
				[IitpAhflTable.fileId]: fileId,
				[IitpAhflTable.fileGroupId]: fileGroupId,
				[IitpAhflTable.fileNm]: newFileName,
				[IitpAhflTable.originFileNm]: fileOriginName,
				[IitpAhflTable.fileTypeCd]: 'CMN009FILE161',
				[IitpAhflTable.fileTypeNm]: '수요조사공고 접수 첨부파일',
				[IitpAhflTable.fileExt]: fileExt,
				[IitpAhflTable.filePath]: filePath,
			};
			result.surveyAhflList.push(obj);
		}
		const dbResult = await dbConnection(IitpAhflTable.index)
			.insert(result.surveyAhflList)
			.onConflict([IitpAhflTable.fileId, IitpAhflTable.fileGroupId])
			.merge();
		if (dbResult[0] === 0) {
			result.status = true;
		}
	}

	return result;
};

/** 접수서 - 등록(El) */
export const demandSurveyElUpsert = async (dbResult) => {
	const { surveyList } = dbResult;
	const index = DemandSurveyIndex.index;
	for (const cnt in surveyList) {
		const item = surveyList[cnt];
		const _id = item[DemandSurveyTable.dmsyRcipId];
		const payload = {
			[DemandSurveyIndex.dmsyRcipId]: _id,
			[DemandSurveyIndex.dmsyPbncId]: item[DemandSurveyTable.dmsyPbncId],
			[DemandSurveyIndex.dmndAplNm]: item[DemandSurveyTable.dmndAplNm],
			[DemandSurveyIndex.prgsStCd]: item[DemandSurveyTable.prgsStcd],
			[DemandSurveyIndex.prgsStNm]: item[DemandSurveyTable.prgsStcdNm],
			[DemandSurveyIndex.rndOtlnCn]: item[DemandSurveyTable.rndOtlnCn],
			[DemandSurveyIndex.tecSclsCd]: item[DemandSurveyTable.tecSclsCd],
			[DemandSurveyIndex.tecSclsNm]: item[DemandSurveyTable.tecSclsNm],
			[DemandSurveyIndex.trlCd]: item[DemandSurveyTable.trlCd],
			[DemandSurveyIndex.trlNm]: item[DemandSurveyTable.trlNm],
			[DemandSurveyIndex.ictTecSclsCd]: item[DemandSurveyTable.ictTecSclsCd],
			[DemandSurveyIndex.indvId]: item[DemandSurveyTable.indvId],
			[DemandSurveyIndex.indvNm]: item[DemandSurveyTable.indvNm],
			[DemandSurveyIndex.instId]: item[DemandSurveyTable.instId],
			[DemandSurveyIndex.instNm]: item[DemandSurveyTable.instNm],
			[DemandSurveyIndex.aplDt]: item[DemandSurveyTable.aplDtm],
			[DemandSurveyIndex.rcipDt]: item[DemandSurveyTable.rcipDtm],
			[DemandSurveyIndex.deleteYn]: 'N',
			[DemandSurveyIndex.adminInputYn]: 'Y',
		};
		await upsert(index, _id, payload);
	}
};

/**  접수서 - 수정(MariaDB) */
export const demandSurveyDbUpdate = async (surveyId, updateParams) => {
	const { businessIctCode, businessIctName, confirmIctCode, confirmIctName } = updateParams;

	const result = await dbConnection(DemandSurveyTable.index)
		.where(DemandSurveyTable.dmsyRcipId, '=', surveyId)
		.update({
			[DemandSurveyTable.bizTecCd]: isEmpty(businessIctCode) ? null : businessIctCode,
			[DemandSurveyTable.bizTecNm]: isEmpty(businessIctName) ? null : businessIctName,
			[DemandSurveyTable.ictTecConfirmCd]: isEmpty(confirmIctCode) ? null : confirmIctCode,
			[DemandSurveyTable.ictTecConfirmNm]: isEmpty(confirmIctName) ? null : confirmIctName,
		});

	return result;
};

/**  접수서 - 수정(EL) */
export const demandSurveyElUpdate = async (surveyId, updateParams) => {
	const { businessIctCode, businessIctName, confirmIctCode, confirmIctName } = updateParams;

	const index = DemandSurveyIndex.index;
	const _id = surveyId;
	const payload = {
		[DemandSurveyIndex.bizTecCd]: isEmpty(businessIctCode) ? null : businessIctCode,
		[DemandSurveyIndex.bizTecNm]: isEmpty(businessIctName) ? null : businessIctName,
		[DemandSurveyIndex.ictTecConfirmCd]: isEmpty(confirmIctCode) ? null : confirmIctCode,
		[DemandSurveyIndex.ictTecConfirmNm]: isEmpty(confirmIctName) ? null : confirmIctName,
	};

	const result = await update(index, _id, payload);

	return result;
};

/**  접수서 - 일괄변경(MariaDB) */
export const demandSurveyDbBulkUpdate = async (surveyIds, updateParams) => {
	const { code, codeName, type } = updateParams;
	const updateQuery =
		type == DemandNoticeTable.bizTecCd
			? {
					[DemandSurveyTable.bizTecCd]: isEmpty(code) ? null : code,
					[DemandSurveyTable.bizTecNm]: isEmpty(codeName) ? null : codeName,
			  }
			: {
					[DemandSurveyTable.ictTecConfirmCd]: isEmpty(code) ? null : code,
					[DemandSurveyTable.ictTecConfirmNm]: isEmpty(codeName) ? null : codeName,
			  };

	const result = await dbConnection(DemandSurveyTable.index)
		.whereIn(DemandSurveyTable.dmsyRcipId, surveyIds)
		.update(updateQuery);

	return result;
};

/**  접수서 - 일괄변경(EL) */
export const demandSurveyElBulkUpdate = async (surveyIds, updateParams) => {
	const { code, codeName, type } = updateParams;
	const index = DemandSurveyIndex.index;
	const scriptQuery =
		type == DemandNoticeTable.bizTecCd
			? `ctx._source.${DemandSurveyIndex.bizTecCd} = '${code}'; ctx._source.${DemandSurveyIndex.bizTecNm} = '${codeName}';`
			: `ctx._source.${DemandSurveyIndex.ictTecConfirmCd} = '${code}'; ctx._source.${DemandSurveyIndex.ictTecConfirmNm} = '${codeName}';`;

	const payload = {
		query: {
			terms: {
				[DemandSurveyIndex.pk]: surveyIds,
			},
		},
		script: {
			source: scriptQuery,
		},
	};
	const result = await updateByQuery(index, payload);

	return result;
};

/** 접수서 - 삭제가능 플래그 유무 조회 */
export const findByAdminInputYnSurvey = async (surveyId) => {
	const data = await dbConnection(DemandSurveyTable.index)
		.select(DemandSurveyTable.adminInputYn)
		.where(DemandSurveyTable.dmsyRcipId, '=', surveyId);

	return data;
};

/** 접수서 - 삭제 플래그 업데이트(MariaDB) */
export const updateByDeleteFlagSurveyDb = async (surveyId) => {
	const result = await dbConnection(DemandSurveyTable.index)
		.where(DemandSurveyTable.dmsyRcipId, '=', surveyId)
		.update({
			[DemandSurveyTable.deleteYn]: 'Y',
		});

	return result;
};

/** 접수서 - 삭제 플래그 업데이트(El) */
export const updateByDeleteFlagSurveyEl = async (surveyId) => {
	const index = DemandSurveyIndex.index;
	const _id = surveyId;
	const payload = {
		[DemandSurveyIndex.deleteYn]: 'Y',
	};
	const result = await update(index, _id, payload);

	return result;
};

/** 접수서 - 첨부파일 보기 */
export const findBySurveyIdDb = async (surveyId) => {
	const selectQuery = `
		SELECT 
			B.FILE_NAME
			, C.SYSTEM_TYPE 
		FROM DEMAND_SURVEY A 
		LEFT JOIN IITP_AHFL B
			ON A.FILE_GROUP_ID = B.FILE_GROUP_ID
		LEFT JOIN DEMAND_PBNC C
			ON A.DMSY_PBNC_ID = C.DMSY_PBNC_ID 
		WHERE A.DMSY_RCIP_ID = ${surveyId}
		LIMIT 1
	`;
	const dataList = await dbConnection.raw(selectQuery);

	return dataList[0];
};

/**  병합 수요조사 그룹 등록 */
export const demandGroupElInsert = async (params) => {
	const { groupNm, dmsyPbncId } = params;
	const pk = uuid();
	const nowDate = dayjs().format('YYYY-MM-DD HH:mm:ss');

	const index = DemandGroupIndex.index;
	const _id = pk;
	const payload = {
		[DemandGroupIndex.groupId]: pk,
		[DemandGroupIndex.groupNm]: groupNm,
		[DemandGroupIndex.dmsyPbncId]: dmsyPbncId,
		[DemandGroupIndex.regDt]: nowDate,
		[DemandGroupIndex.updateDt]: nowDate,
	};

	const result = await write(index, '_doc', payload, _id);
	return result;
};

/**  병합 수요조사 그룹 리스트 */
export const demandGroupList = async (params) => {
	const { dmsyPbncId } = params;
	const demandField = demandConfig[ApiModel.DEMAND_GROUP_LIST];
	// 공고 목록 Query
	const query = {
		index: demandField.index,
		body: {
			size: 10000000,
			query: {
				match: {
					[demandField.match]: dmsyPbncId,
				},
			},
			sort: {
				[demandField.sort]: {
					order: 'desc',
				},
			},
			_source: demandField.source,
		},
	};

	const searchResult = await search(query);

	return { searchResult };
};

/** 병합 수요조사 그룹 수정 */
export const demandGroupElUpdate = async (groupId, params) => {
	const { groupNm } = params;
	const index = DemandGroupIndex.index;
	const _id = groupId;
	const payload = {
		[DemandGroupIndex.groupNm]: groupNm,
	};

	const result = await update(index, _id, payload);

	return result;
};

/** 병합수요조사 그룹 삭제 */
export const demandGroupElDelete = async (groupId) => {
	const index = DemandGroupIndex.index;
	const _id = groupId;
	const result = await elDelete(index, _id);

	return result;
};

/** 병합수요조사 그룹 삭제 후 이벤트 - 수정(MariaDB)*/
export const dbSurveyUpdateByGroup = async (groupId) => {
	const result = await dbConnection(DemandSurveyTable.index)
		.where(DemandSurveyTable.groupId, '=', groupId)
		.update({
			[DemandSurveyTable.groupId]: null,
			[DemandSurveyTable.groupLeaderYn]: 'N',
		});

	return result;
};

/** 병합수요조사 그룹 삭제 후 이벤트 - 수정(El)*/
export const elSurveyUpdateByGroup = async (groupId) => {
	const index = DemandSurveyIndex.index;
	const scriptQuery = `ctx._source.${DemandSurveyIndex.groupId} = null; ctx._source.${DemandSurveyIndex.groupLeaderYn} = 'N';`;

	const payload = {
		script: {
			source: scriptQuery,
		},
		query: {
			match: {
				[DemandSurveyIndex.groupId]: groupId,
			},
		},
	};

	const result = await updateByQuery(index, payload);

	return result;
};

/** 병합수요조사 그룹 접수서 리스트 */
export const demandGroupSurveys = async (params) => {
	const { size, page, dmsyPbncId, groupId } = params;
	const demandField = demandConfig[ApiModel.DEMAND_GROUP_SURVEY_LIST];

	const query = {
		index: demandField.index,
		// request_cache: false,
		body: {
			size: size,
			from: (page - 1) * size,
			query: {
				bool: {
					must: [
						{
							match: {
								[demandField.matchPbncId]: dmsyPbncId,
							},
						},
						{
							match: {
								[demandField.matchGroupId]: groupId,
							},
						},
					],
				},
			},
			_source: demandField.source,
		},
	};
	const searchResult = await search(query);

	return { searchResult };
};

/** 병합수요 접수서 매핑 삭제 */
export const groupSurveyMappingReomveUpdate = async (removeSurveyIds) => {
	// 1. DB
	const dbResult = await dbConnection(DemandSurveyTable.index)
		.whereIn(DemandSurveyTable.dmsyRcipId, removeSurveyIds)
		.update({
			[DemandSurveyTable.groupId]: null,
			[DemandSurveyTable.groupLeaderYn]: 'N',
		});
	if (dbResult > 0) {
		// 2. EL
		const index = DemandSurveyIndex.index;
		const scriptQuery = `ctx._source.${DemandSurveyIndex.groupId} = null; ctx._source.${DemandSurveyIndex.groupLeaderYn} = 'N';`;

		const payload = {
			script: {
				source: scriptQuery,
			},
			query: {
				terms: {
					[DemandSurveyIndex.pk]: removeSurveyIds,
				},
			},
		};
		await updateByQuery(index, payload);
	} else {
		throw new BadGatewayError();
	}
};

/** 병합수요 접수서 매핑 등록 */
export const groupSurveyMappingUpdate = async (addSurveyIds, groupId) => {
	// 1. DB
	const dbResult = await dbConnection(DemandSurveyTable.index)
		.whereIn(DemandSurveyTable.dmsyRcipId, addSurveyIds)
		.update({
			[DemandSurveyTable.groupId]: groupId,
			[DemandSurveyTable.groupLeaderYn]: 'N',
		});
	if (dbResult > 0) {
		const index = DemandSurveyIndex.index;
		const scriptQuery = `ctx._source.${DemandSurveyIndex.groupId} = '${groupId}'; ctx._source.${DemandSurveyIndex.groupLeaderYn} = 'N';`;

		const payload = {
			script: {
				source: scriptQuery,
			},
			query: {
				terms: {
					[DemandSurveyIndex.pk]: addSurveyIds,
				},
			},
		};

		await updateByQuery(index, payload);
	} else {
		throw new BadGatewayError();
	}
};

/** 병합수요 접수서 매핑 대표 등록 */
export const groupSurveyMappingUpdateByLeader = async (surveyLeaderId, groupId) => {
	// 1. DB(해당 그룹에 리더를 'N' 변경)
	await dbConnection(DemandSurveyTable.index)
		.where(DemandSurveyTable.groupId, '=', groupId)
		.update({
			[DemandSurveyTable.groupLeaderYn]: 'N',
		});

	// 2. DB(해당 그룹에 접수서 리더를 'Y' 변경)
	const dbResult = await dbConnection(DemandSurveyTable.index)
		.where(DemandSurveyTable.dmsyRcipId, '=', surveyLeaderId)
		.update({
			[DemandSurveyTable.groupLeaderYn]: 'Y',
		});
	if (dbResult > 0) {
		const index = DemandSurveyIndex.index;
		// 2. EL(해당 그룹에 접수서 리더를 'Y' 변경)
		const allScriptQuery = `ctx._source.${DemandSurveyIndex.groupLeaderYn} = 'N';`;
		const allPayload = {
			script: {
				source: allScriptQuery,
			},
			query: {
				term: {
					[DemandSurveyIndex.groupId]: groupId,
				},
			},
		};
		const elGroupResult = await updateByQuery(index, allPayload);
		if (elGroupResult.statusCode === 200) {
			const scriptQuery = `ctx._source.${DemandSurveyIndex.groupLeaderYn} = 'Y';`;
			const payload = {
				script: {
					source: scriptQuery,
				},
				query: {
					term: {
						[DemandSurveyIndex.pk]: surveyLeaderId,
					},
				},
			};
			await updateByQuery(index, payload);
		}
	} else {
		throw new BadGatewayError();
	}
};

/** 접수서 그룹 명 조회 */
export const findBySurveyGroupNm = async (groupId) => {
	const query = {
		index: DemandGroupIndex.index,
		body: {
			query: {
				term: {
					[DemandGroupIndex.groupId]: groupId,
				},
			},
			_source: [DemandGroupIndex.groupNm],
		},
	};
	const searchResult = await search(query);

	return { searchResult };
};
