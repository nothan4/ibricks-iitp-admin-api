import excelToJson from 'convert-excel-to-json';
import config from 'config';
import fs from 'fs';
import * as demandModel from './demand-model.js';
import * as ictCodesModel from '../ict-codes/ict-codes-model.js';
import * as responseSchema from './config/response-schema.js';
import {
	FieldModel,
	IctCodeTable,
	DemandNoticeTable,
	DemandSurveyIndex,
	DemandGroupIndex,
	IitpAhflTable,
} from '../../model/index.js';
import { isEmpty } from '../../util/index.js';
import baseRequestApi from '../../lib/axios/base-request-api.js';

/** 수요뱅킹 공고 목록 조회 */
export const getDemandNotices = async (params) => {
	// 수요뱅킹 공고 목록 데이터
	const demandNotices = await demandModel.demandNotices(params);
	const noticeList = demandNotices.searchResult.body.hits.hits;
	const noticeTotalCount = demandNotices.searchResult.body.hits?.total.value;

	// 수요뱅킹 공고 아이디(Array)
	const noticeIds = noticeList.map((item) => {
		return item._source.DMSY_PBNC_ID_k;
	});
	const noticeIdsSize = noticeIds.length;

	// Response 정의
	const result = {
		totalCount: noticeTotalCount,
		dataList: [],
	};

	if (noticeIdsSize > 0) {
		// 수요뱅킹 공고별 접수서 건수
		const demandNoticeSurveyCount = await demandModel.demandNoticeSurveyCount(noticeIds);
		const surveyCounts = demandNoticeSurveyCount.searchResult.body.aggregations.result.buckets;
		console.log(surveyCounts);

		// 공고 목록 과 접수서 건수 Merge
		noticeList.map((data, index) => {
			result.dataList.push(responseSchema.notices(data._source));

			const count = surveyCounts.filter((countData) => data._source.DMSY_PBNC_ID_k == countData.key);

			if (count[0]) result.dataList[index].surveyCount = count[0].doc_count;
			else result.dataList[index].surveyCount = 0;
		});
	}

	const response = {
		status: 200,
		message: 'Success',
		result,
	};

	return response;
};

/** 수요뱅킹 공고 목록 상세정보 */
export const getDemandNotice = async (params) => {
	const { noticeId } = params;
	// 수요뱅킹 공고 상세정보
	const demandNotice = await demandModel.demandNotice(noticeId);
	const notice = demandNotice.searchResult.body.hits.hits[0]._source;
	const noticeIds = [noticeId];
	// 수요뱅킹 해당공고 접수서 건수
	const demandNoticeSurveyCount = await demandModel.demandNoticeSurveyCount(noticeIds);
	const surveyCounts = demandNoticeSurveyCount.searchResult.body.aggregations.result.buckets[0];

	// Response 정의
	const result = responseSchema.noticeDetail(notice);
	result.surveyCount = surveyCounts === undefined ? 0 : surveyCounts.doc_count;

	const response = {
		status: 200,
		message: 'Success',
		result,
	};

	return response;
};

/** 수요뱅킹 공고 등록 */
export const insertNotices = async (file) => {
	const { path } = file;
	/**
	 * Header Sheet1
	 * 공고ID|공고제목|공고년도|기획 공고 타입 명(정기, 수시, 해당없음)|공고 시작일|공고 종료일|공고유무|공고삭제 유무|연계 대상 시스템(EZONE, IRIS)|사업기획 공고유무
	 */
	const excelToJsonData = excelToJson({
		sourceFile: path,
		columnToKey: {
			A: 'dmsyPbncId',
			B: 'pbncNm',
			C: 'pbncYy',
			D: 'typeName',
			E: 'pbncBegdtm',
			F: 'pbncEnddtm',
			G: 'pbncYn',
			H: 'deleteYn',
			I: 'systemType',
			J: 'bizPlanYn',
		},
		header: {
			rows: 1,
		},
	});

	const sheetList = excelToJsonData.Sheet1;
	const excelDataList = [];

	for (const cnt in sheetList) {
		const item = sheetList[cnt];
		const pk = item.dmsyPbncId;
		const systemType = isEmpty(item.systemType) ? item.systemType : String(item.systemType).trim().toUpperCase();
		const dmsyPbncId = systemType === 'IRIS' ? `${systemType}_${pk}` : pk;
		const obj = { ...item, systemType, dmsyPbncId };
		excelDataList.push(obj);
	}

	// 업로드 파일 삭제
	fs.rm(path, function (err) {
		if (err) throw err;
	});

	// db 저장
	const dbResult = await demandModel.demandNoticeDbUpsert(excelDataList);
	if (dbResult.length > 0) {
		demandModel.demandNoticeElUpsert(dbResult);
	}

	const response = {
		status: 200,
		message: 'Success',
	};

	return response;
};

/** 수요뱅킹 공고 수정 */
export const updateDemandNotice = async (noticeId, params) => {
	const response = {
		status: 200,
		message: 'Success',
	};

	// 타입명 가져오기
	const typeName = FieldModel[params.typeCode];
	const updateParams = { ...params, typeName: typeName };

	// Maraia DB 수정( dbResult = 1:성공)
	const dbResult = await demandModel.demandNoticeDbUpdate(noticeId, updateParams);

	if (dbResult === 1) {
		// EL 수정( elResult = 200: 성공 )
		const elResult = await demandModel.demandNoticeElUpdate(noticeId, updateParams);
	} else {
		response.status = 500;
		response.message = 'Fail';
	}

	return response;
};

/** 수요뱅킹 공고 삭제 */
export const deleteNotice = async (noticeId) => {
	const response = {
		status: 200,
		message: 'Success',
	};
	const adminInputYnData = await demandModel.findByAdminInputYnNotice(noticeId);
	const adminInputYnObject = Object.assign({}, ...adminInputYnData);

	if (adminInputYnObject[DemandNoticeTable.adminInputYn] === 'Y') {
		const dbResult = await demandModel.updateByDeleteFlagNoticeDb(noticeId);
		if (dbResult === 1) {
			await demandModel.updateByDeleteFlagNoticeEl(noticeId);
		}
	} else {
		response.status = 500;
		response.message = 'Fail';
	}

	return response;
};

/** 기술수요조사서 접수서 리스트 */
export const getDemandSurveys = async (params) => {
	// 수요뱅킹 접수 목록 데이터
	const demandSurveys = await demandModel.demandSurveys(params);
	const surveyList = demandSurveys.searchResult.body.hits.hits;
	const surveyTotalCount = demandSurveys.searchResult.body.hits?.total.value;

	// Response 정의
	const result = {
		totalCount: surveyTotalCount,
		dataList: [],
	};

	surveyList.map((data, index) => {
		result.dataList.push(responseSchema.surveys(data._source));
	});

	const response = {
		status: 200,
		message: 'Success',
		result,
	};

	return response;
};

/** 기술수요조사서 접수서 상세 정보 */
export const getDemandSurvey = async (params) => {
	const { surveyId } = params;
	// 접수서 데이터
	const demandSurvey = await demandModel.demandSurvey(surveyId);
	const surveyObject = demandSurvey.searchResult.body.hits.hits[0]._source;
	const groupId = surveyObject[DemandSurveyIndex.groupId];

	// 접수서 그룹 아이디 존재 시 그룹명 가져오기
	if (!isEmpty(groupId)) {
		const surveyGroupInfo = await demandModel.findBySurveyGroupNm(groupId);
		const groupResult = surveyGroupInfo.searchResult.body.hits.hits[0]._source;
		surveyObject[DemandGroupIndex.groupNm] = groupResult[DemandGroupIndex.groupNm];
	}

	// Response 정의
	const result = responseSchema.surveyDetail(surveyObject);

	const response = {
		status: 200,
		message: 'Success',
		result,
	};

	return response;
};

/** 기술수요조사 접수서 등록(업로드) */
export const insertSurveys = async (file) => {
	const { path } = file;
	const sheet1 = '접수서 요청 양식';
	const sheet2 = '접수서 첨부파일';
	/**
	 * Header Sheet1(접수서 리스트)
	 * 공고ID|수요조사ID|수요조사명|수요조사 접수상태 코드|수요조사 접수상태 명|연구개발내용
	 * |국가과학기술표준분류 코드|국가과학기술표준분류 명|기술성숙도 코드|기술성숙도 명|ICT분류코드
	 * |ICT분류명|제안자ID|제안자명|제안자소속기관ID|제안자소속기관명|제출일|신청일|연계 대상 시스템(EZONE, IRIS)
	 * Header Sheet2(접수서 첨부파일 리스트)
	 * 공고ID|수요조사ID|첨부파일종류|첨부파일명|연계 대상 시스템(EZONE, IRIS)
	 */
	const excelToJsonData = excelToJson({
		sourceFile: path,
		sheets: [
			{
				name: sheet1,
				header: {
					rows: 1,
				},
				columnToKey: {
					A: 'dmsyPbncId',
					B: 'dmsyRcipId',
					C: 'dmndAplNm',
					D: 'prgsStNm',
					E: 'prgsStCd',
					F: 'rndOtlnCn',
					G: 'tecSclsCd',
					H: 'tecSclsNm',
					I: 'trlCd',
					J: 'trlNm',
					K: 'ictTecSclsCd',
					L: 'ictTecSclsNm',
					M: 'indvId',
					N: 'indvNm',
					O: 'instId',
					P: 'instNm',
					Q: 'rcipDt',
					R: 'aplDt',
					S: 'systemType',
				},
			},
			{
				name: sheet2,
				header: {
					rows: 1,
				},
				columnToKey: {
					A: 'dmsyPbncId',
					B: 'dmsyRcipId',
					C: 'fileTypeName',
					D: 'fileName',
					E: 'systemType',
				},
			},
		],
	});
	const dataListSheet1 = excelToJsonData[sheet1];
	const dataListSheet2 = excelToJsonData[sheet2].filter((data) => data.fileTypeName === '수요조사서');

	// 업로드 파일 삭제
	fs.rm(path, function (err) {
		if (err) throw err;
	});

	// db 저장
	const dbResult = await demandModel.demandSurveyDbUpsert(dataListSheet1, dataListSheet2);
	if (dbResult) {
		demandModel.demandSurveyElUpsert(dbResult);
	}

	const response = {
		status: 200,
		message: 'Success',
	};

	return response;
};

/** 기술수요조사 접수서 수정 */
export const updateDemandSurvey = async (surveyId, params) => {
	const { businessIctCode, confirmIctCode } = params;

	const response = {
		status: 200,
		message: 'Success',
	};

	const ictCodes = [businessIctCode, confirmIctCode];
	const ictCodesNotEmpty = ictCodes.filter(Boolean);
	// ICT 분류명 가져오기
	if (!isEmpty(ictCodesNotEmpty)) {
		const ictCodeNames = await ictCodesModel.ictCodeNames(ictCodesNotEmpty);

		if (!isEmpty(ictCodeNames)) {
			const updateParams = {
				businessIctCode: businessIctCode,
				businessIctName: '',
				confirmIctCode: confirmIctCode,
				confirmIctName: '',
			};

			for (let items in ictCodeNames) {
				const item = ictCodeNames[items];
				const ictCode = item[IctCodeTable.ictTcclsCd];
				const ictCodeName = item[IctCodeTable.tcclsNm];
				if (businessIctCode == confirmIctCode) {
					updateParams.businessIctName = ictCodeName;
					updateParams.confirmIctName = ictCodeName;
				} else if (businessIctCode == ictCode) {
					updateParams.businessIctName = ictCodeName;
				} else if (confirmIctCode == ictCode) {
					updateParams.confirmIctName = ictCodeName;
				}
			}

			// Maraia DB 수정( dbResult = 1:성공)
			const dbResult = await demandModel.demandSurveyDbUpdate(surveyId, updateParams);

			if (dbResult === 1) {
				// EL 수정( elResult = 200: 성공 )
				const elResult = await demandModel.demandSurveyElUpdate(surveyId, updateParams);
			} else {
				response.status = 500;
				response.message = 'Fail';
			}
		}
	} else {
		response.status = 500;
		response.message = 'Fail';
	}

	return response;
};

/** 기술수요조사서 접수서 일괄변경 */
export const updateBulkDemandSurvey = async (params) => {
	const { surveyIds, type, changeIctCode } = params;

	const response = {
		status: 200,
		message: 'Success',
	};

	const ictCodes = [changeIctCode];
	const ictCodesNotEmpty = ictCodes.filter(Boolean);
	// ICT 분류명 가져오기
	if (!isEmpty(ictCodesNotEmpty)) {
		const ictCodeNames = await ictCodesModel.ictCodeNames(ictCodesNotEmpty);

		if (!isEmpty(ictCodeNames)) {
			const updateParams = {
				code: '',
				codeName: '',
				type: type,
			};

			for (let items in ictCodeNames) {
				const item = ictCodeNames[items];
				const ictCode = item[IctCodeTable.ictTcclsCd];
				const ictCodeName = item[IctCodeTable.tcclsNm];
				updateParams.code = ictCode;
				updateParams.codeName = ictCodeName;
			}

			const newSurveyIds = JSON.parse(surveyIds);
			// Maraia DB 수정( dbResult = 1:성공)
			const dbResult = await demandModel.demandSurveyDbBulkUpdate(newSurveyIds, updateParams);

			if (dbResult >= 1) {
				// EL 수정( elResult = 200: 성공 )
				const elResult = await demandModel.demandSurveyElBulkUpdate(newSurveyIds, updateParams);
			} else {
				response.status = 500;
				response.message = 'Fail';
			}
		}
	} else {
		response.status = 500;
		response.message = 'Fail';
	}

	return response;
};

/** 접수서 - 삭제 */
export const deleteSurvey = async (surveyId) => {
	const response = {
		status: 200,
		message: 'Success',
	};
	const adminInputYnData = await demandModel.findByAdminInputYnSurvey(surveyId);
	const adminInputYnObject = Object.assign({}, ...adminInputYnData);

	if (adminInputYnObject[DemandNoticeTable.adminInputYn] === 'Y') {
		const dbResult = await demandModel.updateByDeleteFlagSurveyDb(surveyId);
		if (dbResult === 1) {
			await demandModel.updateByDeleteFlagSurveyEl(surveyId);
		}
	} else {
		response.status = 500;
		response.message = 'Fail';
	}

	return response;
};

/** 기술수요조사 접수서 첨부파일(기술수요조사서) 보기 */
export const viewSurvey = async (surveyId) => {
	const filePath = config.get('streamdocs.surveyFiltPath');
	const streamDocsUrl = config.get('streamdocs.url');

	const dbResult = await demandModel.findBySurveyIdDb(surveyId);

	let fileName = '';
	let systemType = '';
	for (const index in dbResult) {
		const item = dbResult[index];
		fileName = item[IitpAhflTable.fileNm];
		systemType = item[DemandNoticeTable.systemType];
	}

	const response = {
		status: 200,
		message: 'Success',
		result: {
			url: '',
		},
	};

	const params = {
		externalResource: `file:${filePath}${systemType}/${fileName}`,
	};

	const returnUrl = await baseRequestApi('POST', streamDocsUrl, params);
	response.result.url = returnUrl.data.alink;

	return response;
};

/** 기술수요조사 병합 수요조사 그룹 목록 */
export const getDemandGroups = async (params) => {
	const demandGroups = await demandModel.demandGroupList(params);
	const groupList = demandGroups.searchResult.body.hits.hits;
	const groupTotalCount = demandGroups.searchResult.body.hits?.total.value;

	// Response 정의
	const result = {
		totalCount: groupTotalCount,
		dataList: [],
	};

	groupList.map((data, index) => {
		result.dataList.push(responseSchema.groupList(data._source));
	});

	const response = {
		status: 200,
		message: 'Success',
		result,
	};

	return response;
};

/** 기술수요조사 병합 수요조사 그룹 등록 */
export const insertDemandGroup = async (params) => {
	const response = {
		status: 200,
		message: 'Success',
	};

	const elResult = await demandModel.demandGroupElInsert(params);

	return response;
};

/** 기술수요조사 병합 수요조사 그룹 수정 */
export const updateDemandGroup = async (groupId, params) => {
	const response = {
		status: 200,
		message: 'Success',
	};

	const elResult = await demandModel.demandGroupElUpdate(groupId, params);

	return response;
};

/** 기술수요조사 병합 수요조사 그룹 삭제 */
export const deleteDemandGroup = async (groupId) => {
	// 1. EL GROUP 삭제
	await demandModel.demandGroupElDelete(groupId);

	// 2. SURVEY DB 업데이트
	await demandModel.dbSurveyUpdateByGroup(groupId);

	// 3. EL SURVEY 업데이트
	await demandModel.elSurveyUpdateByGroup(groupId);

	const response = {
		status: 200,
		message: 'Success',
	};

	return response;
};

/** 기술수요조사 병합 수요조사 - 접수서 그룹 목록 */
export const getDemandGroupSurveys = async (params) => {
	const surveyGroups = await demandModel.demandGroupSurveys(params);
	const surveyGroupList = surveyGroups.searchResult.body.hits.hits;
	const surveyGroupTotalCount = surveyGroups.searchResult.body.hits?.total.value;

	// Response 정의
	const result = {
		totalCount: surveyGroupTotalCount,
		dataList: [],
	};

	// 공고 목록 과 접수서 건수 Merge
	surveyGroupList.map((data, index) => {
		result.dataList.push(responseSchema.groupSurveyList(data._source));
	});

	const response = {
		status: 200,
		message: 'Success',
		result,
	};

	return response;
};

/** 기술수요조사 병합 수요조사 - 접수서 그룹 등록(수정) */
export const upsertDemandGroupSurvey = async (params) => {
	const { removeSurveyIds, addSurveyIds, surveyLeaderId, groupId } = params;
	const newRemoveSurveyIds = !isEmpty(removeSurveyIds) ? JSON.parse(JSON.stringify(removeSurveyIds)) : [];
	const newAddSurveyIds = !isEmpty(addSurveyIds) ? JSON.parse(JSON.stringify(addSurveyIds)) : [];

	// 매핑 삭제된 거
	if (!isEmpty(newRemoveSurveyIds)) await demandModel.groupSurveyMappingReomveUpdate(newRemoveSurveyIds);

	// 매핑 추가된 거
	if (!isEmpty(newAddSurveyIds)) await demandModel.groupSurveyMappingUpdate(newAddSurveyIds, groupId);

	// 매핑 대표
	if (!isEmpty(surveyLeaderId)) await demandModel.groupSurveyMappingUpdateByLeader(surveyLeaderId, groupId);

	const response = {
		status: 200,
		message: 'Success',
	};

	return response;
};
