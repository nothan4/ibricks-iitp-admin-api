import dayjs from 'dayjs';
import { DemandNoticeIndex, DemandSurveyIndex, DemandGroupIndex } from '../../../model/index.js';
import { isEmpty } from '../../../util/index.js';

/** 
 * 수요뱅킹 공고 목록 조회 Response
 *  noticeId	String	공고 식별자
    noticeNm	String	공고명
    noticeYear	String	공고년도
    startDate	String	공고 시작일
    endDate	String	공고 종료일
    systemType	String	연계대상 시스템
    typeCode	String	공고 유형 코드
    type	String	공고 유형명
    surveyCount	String	접수서 건수
    bizPlanYn	String	사업기획 유무
    registrDate	Date	등록일
*/
export const notices = (data) => {
	return {
		noticeId: data[DemandNoticeIndex.dmsyPbncId],
		noticeNm: data[DemandNoticeIndex.pbncNm],
		noticeYear: data[DemandNoticeIndex.pbncYy],
		startDate: data[DemandNoticeIndex.pbncBegdtm] ? dayjs(data[DemandNoticeIndex.pbncBegdtm]).format('YYYY-MM-DD') : '',
		endDate: data[DemandNoticeIndex.pbncEnddtm] ? dayjs(data[DemandNoticeIndex.pbncEnddtm]).format('YYYY-MM-DD') : '',
		systemType: data[DemandNoticeIndex.systemType],
		typeCode: data[DemandNoticeIndex.typeCode],
		type: data[DemandNoticeIndex.typeName],
		bizPlanYn: data[DemandNoticeIndex.bizPlanYn],
		adminInputYn: data[DemandNoticeIndex.adminInputYn],
		registrDate: data[DemandNoticeIndex.regtDtmDt] ? dayjs(data[DemandNoticeIndex.regtDtmDt]).format('YYYY-MM-DD') : '',
	};
};

/**
 * 수요뱅킹 공고 목록 상세정보 Response
 *  noticeId	String	공고 식별자
    noticeNm	String	공고명
    noticeYear	String	공고년도
    startDate	String	공고 시작일
    endDate	String	공고 종료일
    systemType	String	연계대상 시스템
    typeCode	String	공고 유형 코드
    type	String	공고 유형명
    surveyCount	String	접수서 건수
    bizPlanYn	String	사업기획 유무
    registrDate	Date	등록일
 * */
export const noticeDetail = (data) => {
	return {
		noticeId: data[DemandNoticeIndex.dmsyPbncId],
		noticeNm: data[DemandNoticeIndex.pbncNm],
		noticeYear: data[DemandNoticeIndex.pbncYy],
		startDate: data[DemandNoticeIndex.pbncBegdtm] ? dayjs(data[DemandNoticeIndex.pbncBegdtm]).format('YYYY-MM-DD') : '',
		endDate: data[DemandNoticeIndex.pbncEnddtm] ? dayjs(data[DemandNoticeIndex.pbncEnddtm]).format('YYYY-MM-DD') : '',
		systemType: data[DemandNoticeIndex.systemType],
		typeCode: data[DemandNoticeIndex.typeCode],
		type: data[DemandNoticeIndex.typeName],
		bizPlanYn: data[DemandNoticeIndex.bizPlanYn],
		registrDate: data[DemandNoticeIndex.regtDtmDt] ? dayjs(data[DemandNoticeIndex.regtDtmDt]).format('YYYY-MM-DD') : '',
	};
};

/** 
 * 수요뱅킹 접수서 목록 Response
 *  surveyId	String	접수 식별자
		surveyName	String	접수명
		rcipDate	String	접수일
		orgnName	String	기관명
		appliacnt	String	신청인
		ictCode	String	등록ICT 분류 코드
		ictName	String	등록ICT 분류 명
		suggestionIctCode	String	추천ICT 분류 코드
		suggestionIctName	String	추천ICT 분류 명
		confirmIctCode	String	확정ICT 분류 코드
		confirmIctName	String	확정ICT 분류 명
		businessIctCode	String	사업기획 ICT 분류 코드
		businessIctName	String	사업기획 ICT 분류 명
		mergeFlag	String	병합 수요 그룹 여부
		registrDate	Date	등록일
*/
export const surveys = (data) => {
	const newIctTecSclsCd = isEmpty(data[DemandSurveyIndex.newIctTecSclsCd])
		? ''
		: data[DemandSurveyIndex.newIctTecSclsCd];
	const newIctTecSclsNm = isEmpty(data[DemandSurveyIndex.newIctTecSclsNm])
		? ''
		: data[DemandSurveyIndex.newIctTecSclsNm];
	const recommend1 = isEmpty(data[DemandSurveyIndex.recommend1]) ? '' : data[DemandSurveyIndex.recommend1];
	const recommendIctTecSclsCd1 = isEmpty(recommend1[DemandSurveyIndex.recommendIctTecSclsCd1])
		? ''
		: recommend1[DemandSurveyIndex.recommendIctTecSclsCd1];
	const recommendIctTecSclsNm1 = isEmpty(recommend1[DemandSurveyIndex.recommendIctTecSclsNm1])
		? ''
		: recommend1[DemandSurveyIndex.recommendIctTecSclsNm1];
	const ictTecConfirmCd = isEmpty(data[DemandSurveyIndex.ictTecConfirmCd])
		? ''
		: data[DemandSurveyIndex.ictTecConfirmCd];
	const ictTecConfirmNm = isEmpty(data[DemandSurveyIndex.ictTecConfirmNm])
		? ''
		: data[DemandSurveyIndex.ictTecConfirmNm];
	const bizTecCd = isEmpty(data[DemandSurveyIndex.bizTecCd]) ? '' : data[DemandSurveyIndex.bizTecCd];
	const bizTecNm = isEmpty(data[DemandSurveyIndex.bizTecNm]) ? '' : data[DemandSurveyIndex.bizTecNm];
	const groupId = isEmpty(data[DemandSurveyIndex.groupId]) ? '' : data[DemandSurveyIndex.groupId];
	const trlCode = isEmpty(data[DemandSurveyIndex.trlCd]) ? '' : data[DemandSurveyIndex.trlCd];
	const trlName = isEmpty(data[DemandSurveyIndex.trlNm]) ? '' : data[DemandSurveyIndex.trlNm];
	const tecSclsCode = isEmpty(data[DemandSurveyIndex.tecSclsCd]) ? '' : data[DemandSurveyIndex.tecSclsCd];
	const tecSclsName = isEmpty(data[DemandSurveyIndex.tecSclsNm]) ? '' : data[DemandSurveyIndex.tecSclsNm];
	const fileGroupId = isEmpty(data[DemandSurveyIndex.fileGroupId]) ? '' : data[DemandSurveyIndex.fileGroupId];
	const orgnName = isEmpty(data[DemandSurveyIndex.instNm]) ? '' : data[DemandSurveyIndex.instNm];
	const appliacnt = isEmpty(data[DemandSurveyIndex.indvNm]) ? '' : data[DemandSurveyIndex.indvNm];
	return {
		noticeId: data[DemandSurveyIndex.dmsyPbncId],
		surveyId: data[DemandSurveyIndex.dmsyRcipId],
		surveyName: data[DemandSurveyIndex.dmndAplNm],
		rcipDate: data[DemandSurveyIndex.rcipDt],
		orgnName: orgnName,
		appliacnt: appliacnt,
		ictCode: newIctTecSclsCd,
		ictName: newIctTecSclsNm,
		suggestionIctCode: recommendIctTecSclsCd1,
		suggestionIctName: recommendIctTecSclsNm1,
		confirmIctCode: ictTecConfirmCd,
		confirmIctName: ictTecConfirmNm,
		businessIctCode: bizTecCd,
		businessIctName: bizTecNm,
		surveyGroupId: groupId,
		trlCode: trlCode,
		trlName: trlName,
		tecSclsCode: tecSclsCode,
		tecSclsName: tecSclsName,
		adminInputYn: data[DemandSurveyIndex.adminInputYn],
		fileGroupId: fileGroupId,
		registrDate: dayjs(data[DemandSurveyIndex.regtDt]).format('YYYY-MM-DD'),
	};
};

/** 
 * 수요뱅킹 접수서 상세정보 Response
 *  surveyId	String	접수 식별자
		surveyName	String	접수명
		rcipDate	String	접수일
		orgnName	String	기관명
		appliacnt	String	신청인
		ictCode	String	등록ICT 분류 코드
		ictName	String	등록ICT 분류 명
		suggestionIctCode	String	추천ICT 분류 코드
		suggestionIctName	String	추천ICT 분류 명
		confirmIctCode	String	확정ICT 분류 코드
		confirmIctName	String	확정ICT 분류 명
		businessIctCode	String	사업기획 ICT 분류 코드
		businessIctName	String	사업기획 ICT 분류 명
		mergeFlag	String	병합 수요 그룹 여부
		registrDate	Date	등록일
*/
export const surveyDetail = (data) => {
	const newIctTecSclsCd = isEmpty(data[DemandSurveyIndex.newIctTecSclsCd])
		? ''
		: data[DemandSurveyIndex.newIctTecSclsCd];
	const newIctTecSclsNm = isEmpty(data[DemandSurveyIndex.newIctTecSclsNm])
		? ''
		: data[DemandSurveyIndex.newIctTecSclsNm];
	const recommend1 = isEmpty(data[DemandSurveyIndex.recommend1]) ? '' : data[DemandSurveyIndex.recommend1];
	const recommendIctTecSclsCd1 = isEmpty(recommend1[DemandSurveyIndex.recommendIctTecSclsCd1])
		? ''
		: recommend1[DemandSurveyIndex.recommendIctTecSclsCd1];
	const recommendIctTecSclsNm1 = isEmpty(recommend1[DemandSurveyIndex.recommendIctTecSclsNm1])
		? ''
		: recommend1[DemandSurveyIndex.recommendIctTecSclsNm1];
	const ictTecConfirmCd = isEmpty(data[DemandSurveyIndex.ictTecConfirmCd])
		? ''
		: data[DemandSurveyIndex.ictTecConfirmCd];
	const ictTecConfirmNm = isEmpty(data[DemandSurveyIndex.ictTecConfirmNm])
		? ''
		: data[DemandSurveyIndex.ictTecConfirmNm];
	const bizTecCd = isEmpty(data[DemandSurveyIndex.bizTecCd]) ? '' : data[DemandSurveyIndex.bizTecCd];
	const bizTecNm = isEmpty(data[DemandSurveyIndex.bizTecNm]) ? '' : data[DemandSurveyIndex.bizTecNm];
	const groupId = isEmpty(data[DemandSurveyIndex.groupId]) ? '' : data[DemandSurveyIndex.groupId];
	const trlCode = isEmpty(data[DemandSurveyIndex.trlCd]) ? '' : data[DemandSurveyIndex.trlCd];
	const trlName = isEmpty(data[DemandSurveyIndex.trlNm]) ? '' : data[DemandSurveyIndex.trlNm];
	const tecSclsCode = isEmpty(data[DemandSurveyIndex.tecSclsCd]) ? '' : data[DemandSurveyIndex.tecSclsCd];
	const tecSclsName = isEmpty(data[DemandSurveyIndex.tecSclsNm]) ? '' : data[DemandSurveyIndex.tecSclsNm];
	const fileGroupId = isEmpty(data[DemandSurveyIndex.fileGroupId]) ? '' : data[DemandSurveyIndex.fileGroupId];
	const orgnName = isEmpty(data[DemandSurveyIndex.instNm]) ? '' : data[DemandSurveyIndex.instNm];
	const appliacnt = isEmpty(data[DemandSurveyIndex.indvNm]) ? '' : data[DemandSurveyIndex.indvNm];
	return {
		noticeId: data[DemandSurveyIndex.dmsyPbncId],
		surveyId: data[DemandSurveyIndex.dmsyRcipId],
		surveyName: data[DemandSurveyIndex.dmndAplNm],
		rcipDate: data[DemandSurveyIndex.rcipDt],
		orgnName: orgnName,
		appliacnt: appliacnt,
		ictCode: newIctTecSclsCd,
		ictName: newIctTecSclsNm,
		suggestionIctCode: recommendIctTecSclsCd1,
		suggestionIctName: recommendIctTecSclsNm1,
		confirmIctCode: ictTecConfirmCd,
		confirmIctName: ictTecConfirmNm,
		businessIctCode: bizTecCd,
		businessIctName: bizTecNm,
		trlCode: trlCode,
		trlName: trlName,
		tecSclsCode: tecSclsCode,
		tecSclsName: tecSclsName,
		adminInputYn: data[DemandSurveyIndex.adminInputYn],
		fileGroupId: fileGroupId,
		registrDate: dayjs(data[DemandSurveyIndex.regtDt]).format('YYYY-MM-DD'),
		rndContent: isEmpty(data[DemandSurveyIndex.rndOtlnCn]) ? '' : data[DemandSurveyIndex.rndOtlnCn],
		surveyGroupId: groupId,
		surveyGroupNm: isEmpty(data[DemandGroupIndex.groupNm]) ? '' : data[DemandGroupIndex.groupNm],
		surveyGroupLeaderYn: data[DemandSurveyIndex.groupLeaderYn],
	};
};

/** 병합수요 그룹 리스트 Response */
export const groupList = (data) => {
	return {
		groupId: data[DemandGroupIndex.groupId],
		groupNm: data[DemandGroupIndex.groupNm],
		noticeId: data[DemandGroupIndex.dmsyPbncId],
	};
};

/** 병합수요 그룹 접수서 리스트 Response */
export const groupSurveyList = (data) => {
	return {
		noticeId: data[DemandSurveyIndex.dmsyPbncId],
		surveyId: data[DemandSurveyIndex.dmsyRcipId],
		surveyName: data[DemandSurveyIndex.dmndAplNm],
		orgnName: data[DemandSurveyIndex.instNm],
		appliacnt: data[DemandSurveyIndex.indvNm],
		groupLeaderYn: data[DemandSurveyIndex.groupLeaderYn],
	};
};
