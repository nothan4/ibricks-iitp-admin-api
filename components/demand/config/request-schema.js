import * as yup from 'yup';
// db에 있는 값이 데이터 필드면 dayjs 사용 할 것
import dayjs from 'dayjs';
import { FieldModel, DemandSurveyTable } from '../../../model/index.js';

/* 기술수요조사 공고 리스트 Request */
export const noticeList = {
	body: yup.object({
		size: yup.number().positive().integer().default(10),
		page: yup.number().positive().integer().default(1),
		systemType: yup
			.string()
			.trim()
			.default(FieldModel.demandSystemTypes[0])
			.required()
			.oneOf(FieldModel.demandSystemTypes),
		startDate: yup.string().trim(),
		endDate: yup.string().trim(),
		noticeName: yup.string().trim(),
	}),
};

/* 기술수요조사 공고 상세정보 Request */
export const noticeId = {
	params: yup.object({
		noticeId: yup.string().trim().required(),
	}),
};

/* 기술수요조사 공고 수정 Request */
export const noticeUpdate = {
	params: yup.object({
		systemType: yup.string().trim().oneOf(FieldModel.demandSystemTypes.splice(FieldModel.demandSystemTypes[0])),
		bizPlanYn: yup.string().trim().uppercase().oneOf(['Y', 'N']),
		typeCode: yup.string().trim().oneOf(FieldModel.demandNoticeTypeCodes),
	}),
};

/* 기술수요조사 접수서 리스트 Request */
export const surveyList = {
	body: yup.object({
		size: yup.number().positive().integer().default(10),
		page: yup.number().positive().integer().default(1),
		searchType: yup.string().trim(),
		searchKeyword: yup.string().trim(),
		groupType: yup.string().trim().oneOf(['ALL', 'NONE']).default('ALL'),
		noticeId: yup.string().trim(),
	}),
};

/* 기술수요조사 접수서 상세정보 Request */
export const surveyId = {
	params: yup.object({
		surveyId: yup.string().trim().required(),
	}),
};

/** 기술수요조사 접수서 수정 Request */
export const surveyUpdate = {
	params: yup.object({
		confirmIctCode: yup.string().trim(),
		businessIctCode: yup.string().trim(),
	}),
};

/** 기술수요조사 접수서 일괄변경 Request */
export const surveyBulkUpdate = {
	params: yup.object({
		surveyIds: yup.array(),
		type: yup.string().trim().oneOf([DemandSurveyTable.ictTecConfirmCd, DemandSurveyTable.bizTecCd]),
		changeIctCode: yup.string().trim(),
	}),
};

/** 병합수요 그룹 등록 */
export const demandGroupInsert = {
	body: yup.object({
		groupNm: yup.string().trim().required(),
		dmsyPbncId: yup.string().trim().required(),
	}),
};

/** 병합수요 그룹 리스트 */
export const demandGroupList = {
	query: yup.object({
		dmsyPbncId: yup.string().trim().required(),
	}),
};

/** 병합수요 그룹 아이디 */
export const groupId = {
	params: yup.object({
		groupId: yup.string().trim().required(),
	}),
};

/** 병합수요 그룹 수정 */
export const demandGroupUpdate = {
	params: yup.object({
		groupNm: yup.string().trim(),
	}),
};

/** 병합수요 접수서 그룹 리스트 */
export const groupSurveyList = {
	query: yup.object({
		size: yup.number().positive().integer().default(10),
		page: yup.number().positive().integer().default(1),
		noticeId: yup.string().trim(),
		groupId: yup.string().trim(),
	}),
};

/** 병합 수요조사 그룹 접수서 등록 Request */
export const surveyMappingUpsert = {
	body: yup.object({
		removeSurveyIds: yup.array(),
		addSurveyIds: yup.array(),
		surveyLeaderId: yup.string().trim(),
		groupId: yup.string().trim(),
	}),
};
