import express from 'express';
import multer from 'multer';
import * as schema from './config/request-schema.js';
import * as demandController from './demand-controller.js';
import { validate } from '../../lib/middleware/validate.js';
import fileUploadStorage from '../../lib/middleware/file-upload-storage.js';

export const router = express.Router();

const upload = multer({ storage: fileUploadStorage });

/* 수요조사 공고 목록[IF-BO-001] */
router.post(
	'/notice',
	validate(schema.noticeList),
	demandController.noticeList
	/*
  #swagger.tags = ['demand']
  #swagger.summary = '수요조사 공고 목록'
  #swagger.description = '수요조사 공고 목록'
  #swagger.parameters['body'] = {
    in: 'body',
    description: 'request body',
    schema: {
      size: 10,
      page: 1,
      systemType: 'ALL',
      startDate: '2023-01-01',
      endDate: '2023-01-30',
      noticeName: '공고 제목'
    }
  }
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS',
      result: {
        totalCount: 10,
        itemList: [{
          noticeId: 'DSP2017004',
          noticeNm: '양자정보통신 중장기 기술개발 수요조사 공고',
          noticeYear: '2017',
          startDate: '2017-06-09',
          endDate: '2017-06-19',
          systemType: 'EZONE',
          typeCode: 'IRREGULAR',
          type: '비정기',
          surveyCount: 3,
          bizPlanYn: 'N',
          registrDate: '2017-06-09'
        }]
      },
    }
  }
  */
);

/* 수요조사 공고 상세 정보[IF-BO-002] */
router.get(
	'/notice/:noticeId',
	validate(schema.noticeId),
	demandController.noticeDetail
	/*
  #swagger.tags = ['demand']
  #swagger.summary = '수요조사 공고 상세 정보'
  #swagger.description = '수요조사 공고 상세 정보'
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS',
      result: {
        noticeId: 'DSP2017004',
        noticeNm: '양자정보통신 중장기 기술개발 수요조사 공고',
        noticeYear: '2017',
        startDate: '2017-06-09',
        endDate: '2017-06-19',
        systemType: 'EZONE',
        typeCode: 'IRREGULAR',
        type: '비정기',
        surveyCount: 3,
        bizPlanYn: 'N',
        registrDate: '2017-06-09'
      },
    }
  }
  */
);

/* 수요조사 공고 업로드(엑셀 파일)[IF-BO-003] */
router.post(
	'/notice/upload',
	upload.single('noticeFile'),
	demandController.noticeUpload
	/*
  #swagger.tags = ['demand']
  #swagger.summary = '수요조사 공고 업로드(엑셀 파일)'
  #swagger.description = '수요조사 공고 업로드(엑셀 파일)'
  #swagger.consumes = ['multipart/form-data']
  #swagger.parameters['file'] = {
    in: 'formData',
    type: 'file',
    required: 'true',
    description: '공고 등록 엑셀파일',
  }
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS'
    }
  }
  */
);

/* 수요조사 공고 수정[IF-BO-004] */
router.put(
	'/notice/:noticeId',
	validate(schema.noticeId),
	validate(schema.noticeUpdate),
	demandController.noticeUpdate
	/*
  #swagger.tags = ['demand']
  #swagger.summary = '수요조사 공고 수정'
  #swagger.description = '수요조사 공고 수정'
  #swagger.parameters['body'] = {
    in: 'body',
    description: 'request body',
    schema: {
      systemType: 'ALL',
      bizCode: '',
      typeCode: ''
    }
  }
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS'
    }
  }
  */
);

/* 수요조사 공고 삭제[IF-BO-005] */
router.patch(
	'/notice/:noticeId',
	validate(schema.noticeId),
	demandController.noticeDelete
	/*
  #swagger.tags = ['demand']
  #swagger.summary = '수요조사 공고 삭제'
  #swagger.description = '수요조사 공고 삭제'
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS'
    }
  }
  */
);

/* 수요조사 접수서 목록[IF-BO-006] */
router.post(
	'/survey',
	validate(schema.surveyList),
	demandController.surveyList
	/*
  #swagger.tags = ['demand']
  #swagger.summary = '수요조사 접수서 목록'
  #swagger.description = '수요조사 접수서 목록'
  #swagger.parameters['body'] = {
    in: 'body',
    description: 'request body',
    schema: {
      size: 10,
      page: 1,
      searchType: 'ALL',
      searchKeyword: '',
      groupType: ''
    }
  }
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS',
      result: {
        itemList: [
          {
            surveyId: '201700000440',
            surveyName: '공증된 양자 암호 키 분배 시스템 개발',
            rcipDate: '2017-06-21',
            orgnName: '한국과학기술연구원(KIST)',
            appliacnt: '강민성',
            ictCode: '10020102',
            ictName: '양자통신',
            suggestionIctCode: '10020102',
            suggestionIctName: '양자통신',
            confirmIctCode: '10020102',
            confirmIctName: '양자통신',
            businessIctCode: '10020102',
            businessIctName: '양자통신',
            mergeFlag: 'N',
            registrDate: '2017-06-16'
          }
        ]
      }
    }
  }
  */
);

/* 수요조사 접수서 상세 정보[IF-BO-007] */
router.get(
	'/survey/:surveyId',
	validate(schema.surveyId),
	demandController.surveyDetail
	/*
  #swagger.tags = ['demand']
  #swagger.summary = '수요조사 접수서 상세 정보'
  #swagger.description = '수요조사 접수서 상세 정보'
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS',
      result: {
        itemList: [{
          surveyId: '201700000440',
          surveyName: '공증된 양자 암호 키 분배 시스템 개발',
          rcipDate: '2017-06-21',
          orgnName: '한국과학기술연구원(KIST)',
          appliacnt: '강민성',
          ictCode: '10020102',
          ictName: '양자통신',
          suggestionIctCode: '10020102',
          suggestionIctName: '양자통신',
          confirmIctCode: '10020102',
          confirmIctName: '양자통신',
          businessIctCode: '10020102',
          businessIctName: '양자통신',
          technologyCode: '',
          technologyName: '',
          trlCode: '',
          trlName: '',
          contents: '',
          mergeFlag: 'N',
          repMergeFlag: 'N',
          registrDate: '2017-06-16'
        }]
      }
    }
  }
  */
);

/* 수요조사 접수서 업로드(엑셀 파일)[IF-BO-008] */
router.post(
	'/survey/upload',
	upload.single('surveyFile'),
	demandController.surveyUpload
	/*
  #swagger.tags = ['demand']
  #swagger.summary = '수요조사 접수서 업로드(엑셀 파일)'
  #swagger.description = '수요조사 접수서 업로드(엑셀 파일)'
  #swagger.consumes = ['multipart/form-data']
  #swagger.parameters['file'] = {
    in: 'formData',
    type: 'file',
    required: 'true',
    description: '접수서 등록 엑셀파일'
  }
  #swagger.parameters['body'] = {
    in: 'body',
    description: 'request body',
    schema: {
      systemType: 'ALL'
    }
  }
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS'
    }
  }
  */
);

/* 수요조사 접수서 수정[IF-BO-009] */
router.put(
	'/survey/:surveyId',
	validate(schema.surveyId),
	validate(schema.surveyUpdate),
	demandController.surveyUpdate
	/*
  #swagger.tags = ['demand']
  #swagger.summary = '수요조사 접수서 수정'
  #swagger.description = '수요조사 접수서 수정'
  #swagger.consumes = ['multipart/form-data']
  #swagger.parameters['file'] = {
    in: 'formData',
    type: 'file',
    required: 'true',
    description: '접수서 등록 엑셀파일'
  }
  #swagger.parameters['body'] = {
    in: 'body',
    description: 'request body',
    schema: {
      systemType: 'ALL'
    }
  }
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS'
    }
  }
  */
);

/* 수요조사 접수서 일괄 변경[IF-BO-010] */
router.post(
	'/survey/change',
	validate(schema.surveyBulkUpdate),
	demandController.surveyBulkUpdate
	/*
  #swagger.tags = ['demand']
  #swagger.summary = '수요조사 접수서 일괄 변경'
  #swagger.description = '수요조사 접수서 일괄 변경'
  #swagger.parameters['body'] = {
    in: 'body',
    description: 'request body',
    schema: {
      surveyIds: ['201700000440'],
      type: 'CONFIRM ITC_TYPE',
      changeIctCode: '10020102'
    }
  }
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS'
    }
  }
  */
);

/* 수요조사 접수서 삭제[IF-BO-011] */
router.patch(
	'/survey/:surveyId',
	validate(schema.surveyId),
	demandController.surveyDelete
	/*
  #swagger.tags = ['demand']
  #swagger.summary = '수요조사 접수서 삭제'
  #swagger.description = '수요조사 접수서 삭제'
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS'
    }
  }
  */
);

/* 수요조사 접수서 첨부파일 보기[IF-BO-012] */
router.get(
	'/survey/view/:surveyId',
	validate(schema.surveyId),
	demandController.surveyView
	/*
  #swagger.tags = ['demand']
  #swagger.summary = '수요조사 접수서 첨부파일 보기'
  #swagger.description = '수요조사 접수서 첨부파일 보기'
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS',
      result: {
        url: ''
      }
    }
  }
  */
);

/* 병합 수요조사 그룹 목록[IF-BO-013] */
router.get(
	'/group',
	validate(schema.demandGroupList),
	demandController.demandGroupList
	/*
  #swagger.tags = ['demand']
  #swagger.summary = '병합 수요조사 그룹 목록'
  #swagger.description = '병합 수요조사 그룹 목록'
  #swagger.parameters['size'] = { 
    in: 'query',
    description: '응답 결과수',
    required: false,
    type: 'integer',
    default: 10
  }
  #swagger.parameters['page'] = { 
    in: 'query',
    description: '페이지',
    required: false,
    type: 'integer',
    default: 1
  }
  #swagger.parameters['keyword'] = { 
    in: 'query',
    description: '그룹명 검색 키워드',
    required: false,
    type: 'string'
  }
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS',
      result: {
        itemList: [
          {
            groupId: '',
            groupName: ''
          }
        ]
      }
    }
  }
  */
);

/* 병합 수요조사 그룹 등록 */
router.post(
	'/group',
	validate(schema.demandGroupInsert),
	demandController.demandGroupInsert
	/*
  #swagger.tags = ['demand']
  #swagger.summary = '병합 수요조사 그룹 등록'
  #swagger.description = '병합 수요조사 그룹 등록'
  #swagger.parameters['body'] = {
    in: 'body',
    description: 'request body',
    schema: {
      groupName: '변경할 그룹명'
    }
  }
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS'
    }
  }
  */
);

/* 병합 수요조사 그룹 수정[IF-BO-014] */
router.put(
	'/group/:groupId',
	validate(schema.groupId),
	validate(schema.demandGroupUpdate),
	demandController.demandGroupUpdate
	/*
  #swagger.tags = ['demand']
  #swagger.summary = '병합 수요조사 그룹 수정'
  #swagger.description = '병합 수요조사 그룹 수정'
  #swagger.parameters['body'] = {
    in: 'body',
    description: 'request body',
    schema: {
      groupName: '변경할 그룹명'
    }
  }
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS'
    }
  }
  */
);

/* 병합 수요조사 그룹 삭제[IF-BO-015] */
router.delete(
	'/group/:groupId',
	validate(schema.groupId),
	demandController.demandGroupDelete
	/*
  #swagger.tags = ['demand']
  #swagger.summary = '병합 수요조사 그룹 삭제'
  #swagger.description = '병합 수요조사 그룹 삭제'
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS'
    }
  }
  */
);

/* 병합 수요조사 그룹 접수서 목록[IF-BO-016] */
router.get(
	'/group-survey',
	validate(schema.groupSurveyList),
	demandController.demandGroupSurveyList
	/*
  #swagger.tags = ['demand']
  #swagger.summary = '병합 수요조사 그룹 접수서 목록'
  #swagger.description = '병합 수요조사 그룹 접수서 목록'
  #swagger.parameters['size'] = { 
    in: 'query',
    description: '응답 결과수',
    required: false,
    type: 'integer',
    default: 10
  }
  #swagger.parameters['page'] = { 
    in: 'query',
    description: '페이지',
    required: false,
    type: 'integer',
    default: 1
  }
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS',
      result: {
        itemList: [
          {
            surveyId: '201700000440',
            surveyName: '공증된 양자 암호 키 분배 시스템 개발',
            orgnName: '한국과학기술연구원(KIST)',
            appliacnt: '강민성',
            repMergeFlag: 'N'
          }
        ]
      }
    }
  }
  */
);

/* 병합 수요조사 그룹 접수서 등록[IF-BO-017] */
router.post(
	'/group-survey',
	validate(schema.surveyMappingUpsert),
	demandController.demandGroupSurveyUpsert
	/*
  #swagger.tags = ['demand']
  #swagger.summary = '병합 수요조사 그룹 접수서 등록'
  #swagger.description = '병합 수요조사 그룹 접수서 등록'
  #swagger.parameters['body'] = {
    in: 'body',
    description: 'request body',
    schema: {
      surveyIds: ['201700000440'],
      groupId: ''
    }
  }
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS'
    }
  }
  */
);
