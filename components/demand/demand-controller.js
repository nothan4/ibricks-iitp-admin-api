import { asyncWrapper } from '../../lib/middleware/async-wrapper.js';
import * as demandService from './demand-service.js';

/** 기술수요조사 공고 리스트 */
export const noticeList = asyncWrapper(async (req, res, next) => {
	const params = req.validated.body;
	const response = await demandService.getDemandNotices(params);
	res.send(response);
});

/** 기술수요조사 공고 상세 정보 */
export const noticeDetail = asyncWrapper(async (req, res, next) => {
	const params = req.validated.params;
	const response = await demandService.getDemandNotice(params);
	res.send(response);
});

/** 기술수요조사 공고 업로드(엑셀 파일) */
export const noticeUpload = asyncWrapper(async (req, res, next) => {
	const file = req.file;
	const response = await demandService.insertNotices(file);
	res.send(response);
});

/** 기술수요조사 공고 수정 */
export const noticeUpdate = asyncWrapper(async (req, res, next) => {
	const noticeId = req.validated.params.noticeId;
	const params = req.validated.body;
	const response = await demandService.updateDemandNotice(noticeId, params);
	res.send(response);
});

/** 기술수요조사 공고 삭제 */
export const noticeDelete = asyncWrapper(async (req, res, next) => {
	const noticeId = req.validated.params.noticeId;
	const response = await demandService.deleteNotice(noticeId);
	res.send(response);
});

/** 기술수요조사 접수서 목록 */
export const surveyList = asyncWrapper(async (req, res, next) => {
	const params = req.validated.body;
	const response = await demandService.getDemandSurveys(params);
	res.send(response);
});

/** 기술수요조사 접수서 상세 정보 */
export const surveyDetail = asyncWrapper(async (req, res, next) => {
	const params = req.validated.params;
	const response = await demandService.getDemandSurvey(params);
	res.send(response);
});

/** 기술수요조사 접수서 업로드(엑셀 파일) */
export const surveyUpload = asyncWrapper(async (req, res, next) => {
	const file = req.file;
	const response = await demandService.insertSurveys(file);
	res.send(response);
});

/** 기술수요조사 접수서 수정 */
export const surveyUpdate = asyncWrapper(async (req, res, next) => {
	const surveyId = req.validated.params.surveyId;
	const params = req.validated.body;
	const response = await demandService.updateDemandSurvey(surveyId, params);
	res.send(response);
});

/** 기술수요조사 접수서 일괄 변경 */
export const surveyBulkUpdate = asyncWrapper(async (req, res, next) => {
	const params = req.validated.body;
	const response = await demandService.updateBulkDemandSurvey(params);
	res.send(response);
});

/** 기술수요조사 접수서 삭제 */
export const surveyDelete = asyncWrapper(async (req, res, next) => {
	const surveyId = req.validated.params.surveyId;
	const response = await demandService.deleteSurvey(surveyId);
	res.send(response);
});

/** 기술수요조사 접수서 첨부파일(기술수요조사서) 보기 */
export const surveyView = asyncWrapper(async (req, res, next) => {
	const surveyId = req.validated.params.surveyId;
	const response = await demandService.viewSurvey(surveyId);
	res.send(response);
});

/** 기술수요조사 병합 수요조사 그룹 목록 */
export const demandGroupList = asyncWrapper(async (req, res, next) => {
	const params = req.validated.query;
	const response = await demandService.getDemandGroups(params);
	res.send(response);
});

/** 기술수요조사 병합 수요조사 그룹 등록 */
export const demandGroupInsert = asyncWrapper(async (req, res, next) => {
	const params = req.validated.body;
	const response = await demandService.insertDemandGroup(params);
	res.send(response);
});

/** 기술수요조사 병합 수요조사 그룹 수정 */
export const demandGroupUpdate = asyncWrapper(async (req, res, next) => {
	const groupId = req.validated.params.groupId;
	const params = req.validated.body;
	const response = await demandService.updateDemandGroup(groupId, params);
	res.send(response);
});

/** 기술수요조사 병합 수요조사 그룹 삭제 */
export const demandGroupDelete = asyncWrapper(async (req, res, next) => {
	const groupId = req.validated.params.groupId;
	const response = await demandService.deleteDemandGroup(groupId);
	res.send(response);
});

/** 기술수요조사 병합 수요조사 접수서 그룹 목록 */
export const demandGroupSurveyList = asyncWrapper(async (req, res, next) => {
	const params = req.validated.query;
	const response = await demandService.getDemandGroupSurveys(params);
	res.send(response);
});

/** 기술수요조사 병합 수요조사 접수서 그룹 등록(수정) */
export const demandGroupSurveyUpsert = asyncWrapper(async (req, res, next) => {
	const params = req.validated.body;
	const response = await demandService.upsertDemandGroupSurvey(params);
	res.send(response);
});
