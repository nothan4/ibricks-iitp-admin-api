import dayjs from 'dayjs';
import { search, update, write, searchCount, elDelete } from '../../lib/elasticsearch/client.js';
import { isEmpty, uuid } from '../../util/index.js';
import { ApiModel, AccountIndex, FieldModel } from '../../model/index.js';

/** 로그인 유저 정보 조회 */
export const findByUser = async (userId) => {
	const query = {
		index: AccountIndex.index,
		body: {
			query: {
				bool: {
					must: [
						{
							match: {
								[AccountIndex.id]: userId,
							},
						},
						{
							match: {
								[AccountIndex.madeby]: FieldModel.accountType[0],
							},
						},
					],
				},
			},
			_source: [
				AccountIndex.pk,
				AccountIndex.id,
				AccountIndex.password,
				AccountIndex.salt,
				AccountIndex.activated,
				AccountIndex.passwordUpdateDate,
				AccountIndex.menuAuth,
				AccountIndex.name,
			],
		},
	};
	const searchResult = await search(query);

	return { searchResult };
};

/** 계정 salt, 패스워드 조회 */
export const findByPasswordAndSalt = async (userId) => {
	const query = {
		index: AccountIndex.index,
		body: {
			query: {
				bool: {
					must: [
						{
							match: {
								[AccountIndex.id]: userId,
							},
						},
						{
							match: {
								[AccountIndex.madeby]: FieldModel.accountType[0],
							},
						},
					],
				},
			},
			_source: [AccountIndex.password, AccountIndex.salt, AccountIndex.pk],
		},
	};
	const searchResult = await search(query);

	return { searchResult };
};

/** 관리자 비밀번호 변경 */
export const updateByPasswordAndSalt = async (params) => {
	const { password, pk, salt } = params;
	const index = AccountIndex.index;
	const _id = pk;
	const nowDate = dayjs().format('YYYY-MM-DD HH:mm:ss');
	const payload = {
		[AccountIndex.password]: password,
		[AccountIndex.salt]: salt,
		[AccountIndex.passwordUpdateDate]: nowDate,
	};
	const result = await update(index, _id, payload);
	return result;
};
