import express from 'express';
import * as schema from './config/request-schema.js';
import { validate } from '../../lib/middleware/validate.js';
import * as accountsController from './accounts-controller.js';

export const router = express.Router();

/* 로그인 */
router.post(
	'/login',
	validate(schema.loginRequest),
	accountsController.login
	/*
  #swagger.tags = ['accounts']
  #swagger.summary = '로그인'
  #swagger.description = '로그인'
  #swagger.parameters['body'] = {
    in: 'body',
    description: 'request body',
    schema: {
      userId: '',
      userPw: ''
    }
  }
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS',
      result: {
        userId: '',
        userName: '',
        menuAuth: '',
        accessToken: ''
      }
    }
  }
  */
);

/* 로그아웃 */
router.post(
	'/logout',
	accountsController.logout
	/*
    #swagger.tags = ["accounts"]
    #swagger.summary = "로그아웃"
    #swagger.parameters["body"] = {
      in: "body",
      description: "request body",
      schema: {
        "id": "test",
      }
    }
    #swagger.responses[200] = {
      description: "success",
      schema: {
        "message": "Logout"
      }
    }
  */
);

/* 토큰연장 */
router.post(
	'/token',
	accountsController.refreshToken
	/*
  #swagger.tags = ['accounts']
  #swagger.summary = '토큰연장'
  #swagger.description = '토크연장'
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS',
      result: {
        userId: '',
        userName: '',
        menuAuth: '',
        accessToken: ''
      }
    }
  }
  */
);

/* 패스워드 변경 */
router.put(
	'/password',
	validate(schema.updatePasswordRequest),
	accountsController.passwordUpdate
	/*
  #swagger.tags = ['accounts']
  #swagger.summary = '패스워드 변경'
  #swagger.description = '패스워드 변경'
  #swagger.parameters['body'] = {
    in: 'body',
    description: 'request body',
    schema: {
      userId: '',
      userPw: '',
      userNewPw: ''
    }
  }
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS'
    }
  }
  */
);
