import dayjs from 'dayjs';
import { AccountIndex } from '../../../model/index.js';
import { isEmpty } from '../../../util/index.js';

/** 로그인 */
export const login = (data) => {
	return {
		userId: data.userId,
		userName: data.name,
		menuAuth: data.menuAuth,
		token: data.accesstoken,
	};
};
