import * as yup from 'yup';
import { FieldModel } from '../../../model/index.js';
import { ValidateMessage } from '../../../message/validate-message.js';

/** 로그인 Request */
export const loginRequest = {
	body: yup.object({
		userId: yup.string().trim().required(),
		userPw: yup.string().trim().required(),
	}),
};

/* 계정 아이디 Request */
export const accountId = {
	params: yup.object({
		accountId: yup.string().trim().required(),
	}),
};

/** 비밀번호 변경 Request */
export const updatePasswordRequest = {
	body: yup.object({
		userId: yup.string().trim().required(),
		userPw: yup.string().trim().required(),
		userNewPw: yup
			.string()
			.min(8, ValidateMessage.MINIMUM_PASSWORD)
			.max(16, ValidateMessage.MAXIMUM_PASSWORD)
			.required(ValidateMessage.REQUIRED_PASSWORD)
			.matches(
				/^(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?])[^\s]*$/,
				ValidateMessage.ACCEPTED_FORMAT_PASSWORD
			),
	}),
};
