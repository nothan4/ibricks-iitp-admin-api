import { asyncWrapper } from '../../lib/middleware/async-wrapper.js';
import * as accountsService from './accounts-service.js';
import account from './account.js';

/** 관리자 - 로그인 */
export const login = asyncWrapper(async (req, res, next) => {
	const params = req.validated.body;
	const response = await accountsService.login(params, res);
	res.send(response);
});

/** 로그아웃 */
export const logout = asyncWrapper((req, res, next) => {
	const { id } = req.body;
	if (account[id]) {
		delete account[id];
	}
	const response = {
		status: 200,
		message: 'Success',
	};
	res.clearCookie('authToken').send(response);
});

/** 토큰 연장 */
export const refreshToken = asyncWrapper(async (req, res, next) => {
	const response = await accountsService.refreshToken(req, res);
	res.send(response);
});

/** 관리자 - 비밀번호 변경 */
export const passwordUpdate = asyncWrapper(async (req, res, next) => {
	const params = req.validated.body;
	const response = await accountsService.updateByPasswordAndSalt(params);
	res.send(response);
});
