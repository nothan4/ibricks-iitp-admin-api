import dayjs from 'dayjs';
import jwt from 'jsonwebtoken';
import { AccountIndex, FieldModel } from '../../model/index.js';
import * as accountModel from './accounts-model.js';
import { pbkdf2Encryption } from '../../lib/crypto/pbkdf.js';
import { CustomError } from '../../lib/errors/custom-error.js';
import { ErrorMessage } from '../../message/index.js';
import * as responseSchema from './config/response-schema.js';
import account from './account.js';
import isEmpty from '../../util/is-empty.js';

/** 관리자 - 로그인 */
export const login = async (params, res) => {
	const { userId, userPw } = params;
	const userRes = await accountModel.findByUser(userId);
	const userInfo = userRes.searchResult.body.hits.hits[0]._source;
	const id = userInfo[AccountIndex.id];
	const password = userInfo[AccountIndex.password];
	const salt = userInfo[AccountIndex.salt];
	const passwordUpdateDate = userInfo[AccountIndex.passwordUpdateDate];
	const name = userInfo[AccountIndex.name];
	const activated = userInfo[AccountIndex.activated];
	const menuAuth = userInfo[AccountIndex.menuAuth];
	const encryptPasswordInfo = await pbkdf2Encryption(userPw, salt);
	if (encryptPasswordInfo.password === password) {
		if (activated) {
			const today = dayjs();
			const expiredAt = dayjs(passwordUpdateDate);
			const result = expiredAt.diff(today, 'day', true);
			const dDay = Math.abs(Math.floor(result));
			if (dDay <= 90) {
				// 엑세스 토큰 발급
				const accesstoken = createAccessToken(userId, name, menuAuth);
				// 리프레쉬 토큰 발급
				const refreshtoken = createRefreshToken(userId, name, menuAuth);
				// 쿠키 저장
				res.cookie('authToken', refreshtoken, {
					httpOnly: true,
				});

				const expireDate = await createExpireDate();

				account[id] = {
					id: id,
					name: name,
					auth: menuAuth,
					refreshtoken,
				};

				const resParams = {
					accesstoken: accesstoken,
					refreshtoken: refreshtoken,
					expireDate: expireDate,
					name: name,
					menuAuth: menuAuth,
					userId: userId,
				};

				const reslut = responseSchema.login(resParams);

				const response = {
					status: 200,
					message: 'Success',
					result: reslut,
				};

				return response;
			} else {
				throw new CustomError(ErrorMessage.EXPIRATION_PASSWORD.status, ErrorMessage.EXPIRATION_PASSWORD.message);
			}
		} else {
			throw new CustomError(ErrorMessage.ACCOUNT_SUSPENDED.status, ErrorMessage.ACCOUNT_SUSPENDED.message);
		}
	} else {
		throw new CustomError(ErrorMessage.NOT_ID_AND_PASSWORD.status, ErrorMessage.NOT_ID_AND_PASSWORD.message);
	}
};

/** 토큰 연장 */
export const refreshToken = async (req, res) => {
	const token = req.cookies.refreshtoken;

	if (!token) throw new CustomError(ErrorMessage.NOT_FOUND_TOKEN.status, ErrorMessage.NOT_FOUND_TOKEN.message);

	let payload = null;

	try {
		payload = jwt.verify(token, process.env.REFRESH_TOKEN_SECRET);
	} catch (err) {
		throw new CustomError(ErrorMessage.EXPIRATION_TOKEN.status, ErrorMessage.EXPIRATION_TOKEN.message);
	}
	const user = account[payload.userId];
	if (!user) throw new CustomError(ErrorMessage.EXPIRATION_TOKEN.status, ErrorMessage.EXPIRATION_TOKEN.message);

	if (user.refreshtoken !== token)
		throw new CustomError(ErrorMessage.EXPIRATION_TOKEN.status, ErrorMessage.EXPIRATION_TOKEN.message);

	const accesstoken = createAccessToken(user.id, user.name, user.auth);
	const userId = user.id;
	const userNm = user.name;
	const menuAuth = user.auth;
	// const refreshtoken = jwt.sign({ userId, userNm, auth: menuAuth }, process.env.REFRESH_TOKEN_SECRET, {
	// 	expiresIn: process.env.REFRESH_TOKEN_TIMEOUT,
	// });

	const expireDate = await createExpireDate();

	// user.refreshtoken = refreshtoken;
	// res.cookie('refreshtoken', token, {
	// 	httpOnly: true,
	// });

	const resParams = {
		accesstoken: accesstoken,
		refreshtoken: token,
		expireDate: expireDate,
		name: userNm,
		menuAuth: menuAuth,
		userId: userId,
	};

	const reslut = responseSchema.login(resParams);

	const response = {
		status: 200,
		message: 'Success',
		result: reslut,
	};

	return response;
};

/** 엑세스 토큰 생성 */
const createAccessToken = (userId, name, menuAuth) => {
	return jwt.sign({ userId, name, auth: menuAuth }, process.env.ACCESS_TOKEN_SECRET, {
		expiresIn: process.env.ACCESS_TOKEN_TIMEOUT,
	});
};

/** 라프레쉬 토큰 생성 */
const createRefreshToken = (userId, name, menuAuth) => {
	return jwt.sign({ userId, name, auth: menuAuth }, process.env.REFRESH_TOKEN_SECRET, {
		expiresIn: process.env.REFRESH_TOKEN_TIMEOUT,
	});
};

/** 엑세스 토큰 만료일 생성 */
const createExpireDate = async () => {
	return dayjs().add(1, 'hour').format('YYYY-MM-DD HH:mm:ss');
};

/** 관리자 - 비밀번호 변경 */
export const updateByPasswordAndSalt = async (params) => {
	const { userId, userPw, userNewPw } = params;
	const userInfo = await accountModel.findByPasswordAndSalt(userId);
	const passwordInfo = userInfo.searchResult.body.hits.hits[0]._source;
	const pk = passwordInfo[AccountIndex.pk];
	const oldPassword = passwordInfo[AccountIndex.password];
	const oldSalt = passwordInfo[AccountIndex.salt];
	const confirmPassword = await pbkdf2Encryption(userPw, oldSalt);
	if (oldPassword === confirmPassword.password) {
		const newPasswordInfo = await pbkdf2Encryption(userNewPw, null);
		params = { pk, ...newPasswordInfo };
		await accountModel.updateByPasswordAndSalt(params);
	} else {
		throw new CustomError(ErrorMessage.NOT_ID_AND_PASSWORD.status, ErrorMessage.NOT_ID_AND_PASSWORD.message);
	}
	const response = {
		status: 200,
		message: 'Success',
	};

	return response;
};
