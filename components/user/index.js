import express from 'express';
import * as schema from './config/request-schema.js';
import { validate } from '../../lib/middleware/validate.js';
import * as usersController from './users-controller.js';

export const router = express.Router();

/* 관리자 계정 리스트 */
router.get(
	'',
	validate(schema.listRequest),
	usersController.userList
	/*
  #swagger.tags = ['users']
  #swagger.summary = '관리자 계정 목록'
  #swagger.description = '관리자 계정 목록'
  #swagger.parameters['size'] = { 
    in: 'query',
    description: '응답 결과수',
    required: false,
    type: 'integer',
    default: 10
  }
  #swagger.parameters['page'] = { 
    in: 'query',
    description: '페이지',
    required: false,
    type: 'integer',
    default: 1
  }
  #swagger.parameters['keyword'] = { 
    in: 'query',
    description: '관리자명',
    required: false,
    type: 'string'
  }
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS',
      result: [
        {
          userId: '',
          userName: '',
          menuAuth: '',
          registrDate: ''
        }
      ]
    }
  }
  */
);

/* 관리자 상세 정보 */
router.get(
	'/:accountId',
	validate(schema.accountId),
	usersController.userDetail
	/*
  #swagger.tags = ['users']
  #swagger.summary = '관리자 계정 상세 정보'
  #swagger.description = '관리자 계정 상세 정보'
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS',
      result: {
        itemList: [
          {
            userId: '',
            userName: '',
            menuAuth: '',
            registrDate: ''
          }
        ]
      }
    }
  }
  */
);

/* 관리자 계정 수정 */
router.put(
	'/:accountId',
	validate(schema.accountId),
	usersController.userUpdate
	/*
  #swagger.tags = ['users']
  #swagger.summary = '관리자 계정 수정'
  #swagger.description = '관리자 계정 수정'
  #swagger.parameters['body'] = {
    in: 'body',
    description: 'request body',
    schema: {
      password: '',
      menuAuth: ''
    }
  }
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS',
      result: [
        {
          userId: '',
          userName: '',
          menuAuth: '',
          registrDate: ''
        }
      ]
    }
  }
  */
);

/* 관리자 계정 등록 */
router.post(
	'',
	validate(schema.insertRequest),
	usersController.userInsert
	/*
  #swagger.tags = ['users']
  #swagger.summary = '관리자 계정 등록'
  #swagger.description = '관리자 계정 등록'
  #swagger.parameters['body'] = {
    in: 'body',
    description: 'request body',
    schema: {
      userId: '',
      userPw: '',
      userName: '',
      menuAuth: '',
      registrDate: ''
    }
  }
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS'
    }
  }
  */
);

/* 관리자 계정 삭제 */
router.delete(
	'/:accountId',
	validate(schema.accountId),
	usersController.userDelete
	/*
  #swagger.tags = ['users']
  #swagger.summary = '관리자 계정 삭제'
  #swagger.description = '관리자 계정 삭제'
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS'
    }
  }
  */
);

/* 관리자 아이디 중복체크 */
router.get(
	'/check/:accountId',
	validate(schema.accountId),
	usersController.userIdCheck
	/*
  #swagger.tags = ['users']
  #swagger.summary = '관리자 아이디 중복체크'
  #swagger.description = '관리자 아이디 중복체크'
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS'
    }
  }
  */
);

/* 관리자 사용/중지 */
router.patch(
	'/:accountId',
	validate(schema.accountId),
	validate(schema.activatedRequest),
	usersController.userActive
	/*
  #swagger.tags = ['users']
  #swagger.summary = '관리자 사용/중지'
  #swagger.description = '관리자 사용/중지'
  #swagger.parameters['body'] = {
    in: 'body',
    description: 'request body',
    schema: {
      activated: true
    }
  }
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS'
    }
  }
  */
);
