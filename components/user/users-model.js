import dayjs from 'dayjs';
import { search, update, write, searchCount, elDelete } from '../../lib/elasticsearch/client.js';
import { isEmpty, uuid } from '../../util/index.js';
import { AccountIndex } from '../../model/index.js';

/** 관리자 - 리스트 */
export const getUsers = async (params) => {
	const { size, page, keyword } = params;
	const dynamicMustQuery = [];

	// 이름 검색
	const keywordQuery = !isEmpty(keyword)
		? {
				simple_query_string: {
					query: keyword,
					fields: [AccountIndex.nameKeyword, AccountIndex.nameKobrick],
				},
		  }
		: null;
	if (keywordQuery != null) dynamicMustQuery.push(keywordQuery);

	// 계정 타입
	const madebyQuery = {
		match: {
			[AccountIndex.madeby]: 'ADMIN',
		},
	};
	dynamicMustQuery.push(madebyQuery);

	const query = {
		index: AccountIndex.index,
		body: {
			size: size,
			from: (page - 1) * size,
			sort: {
				[AccountIndex.regDate]: {
					order: 'desc',
				},
			},
			query: {
				bool: {
					must: dynamicMustQuery,
				},
			},
			_source: [
				AccountIndex.pk,
				AccountIndex.id,
				AccountIndex.name,
				AccountIndex.madeby,
				AccountIndex.menuAuth,
				AccountIndex.activated,
				AccountIndex.regDate,
			],
		},
	};
	const searchResult = await search(query);

	return { searchResult };
};

/** 관리자 - 상세정보 */
export const findByUser = async (accountId) => {
	const query = {
		index: AccountIndex.index,
		body: {
			query: {
				bool: {
					filter: [
						{
							term: {
								[AccountIndex.pk]: accountId,
							},
						},
					],
				},
			},
			_source: [
				AccountIndex.pk,
				AccountIndex.id,
				AccountIndex.name,
				AccountIndex.madeby,
				AccountIndex.menuAuth,
				AccountIndex.activated,
				AccountIndex.regDate,
			],
		},
	};
	const searchResult = await search(query);

	return { searchResult };
};

/** 관리자 계정 - 수정 */
export const updateByAdmin = async (accountId, params) => {
	const { password, menuAuth, salt } = params;
	const index = AccountIndex.index;
	const _id = accountId;
	const payload = isEmpty(password)
		? {
				[AccountIndex.menuAuth]: menuAuth,
		  }
		: {
				[AccountIndex.password]: password,
				[AccountIndex.salt]: salt,
				[AccountIndex.menuAuth]: menuAuth,
		  };
	const result = await update(index, _id, payload);

	return result;
};

/** 관리자 - 등록 */
export const adminInsert = async (params) => {
	const { id, password, name, menuAuth, salt } = params;
	const index = AccountIndex.index;
	const madeby = 'ADMIN';
	const _id = `${madeby}-${uuid()}`;
	const nowDate = dayjs().format('YYYY-MM-DD HH:mm:ss');
	const payload = {
		[AccountIndex.pk]: _id,
		[AccountIndex.id]: id,
		[AccountIndex.password]: password,
		[AccountIndex.salt]: salt,
		[AccountIndex.madeby]: madeby,
		[AccountIndex.name]: name,
		[AccountIndex.menuAuth]: menuAuth,
		[AccountIndex.activated]: true,
		[AccountIndex.regDate]: nowDate,
		[AccountIndex.passwordUpdateDate]: nowDate,
	};
	const result = await write(index, '_doc', payload, _id);

	return result;
};

/** 계정 삭제 */
export const deleteUser = async (accountId) => {
	const index = AccountIndex.index;
	const _id = accountId;
	const result = await elDelete(index, _id);

	return result;
};

/** 계정 아이디 중복체크 */
export const findByIdCheck = async (id) => {
	const index = AccountIndex.index;
	const payload = {
		query: {
			bool: {
				must: [
					{
						match: {
							[AccountIndex.id]: id,
						},
					},
					{
						match: {
							[AccountIndex.madeby]: 'ADMIN',
						},
					},
				],
			},
		},
	};
	const result = await searchCount(index, payload);

	return result;
};

/** 계정 사용/중지 업데이트 */
export const updateByActivated = async (accountId, params) => {
	const { activated } = params;
	const index = AccountIndex.index;
	const _id = accountId;
	const userYn = activated === 'Y' ? true : false;
	const payload = {
		[AccountIndex.activated]: userYn,
	};
	const result = await update(index, _id, payload);

	return result;
};
