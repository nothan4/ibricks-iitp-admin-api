import * as userModel from './users-model.js';
import { pbkdf2Encryption } from '../../lib/crypto/pbkdf.js';
import { CustomError } from '../../lib/errors/custom-error.js';
import { ErrorMessage } from '../../message/error-message.js';
import * as responseSchema from './config/response-schema.js';
import isEmpty from '../../util/is-empty.js';

/** 관리자/게스트 - 리스트 */
export const getUserList = async (params) => {
	const listDataResult = await userModel.getUsers(params);
	const accountList = listDataResult.searchResult.body.hits.hits;
	const accountTotalCount = listDataResult.searchResult.body.hits?.total.value;

	// Response 정의
	const result = {
		totalCount: accountTotalCount,
		dataList: [],
	};

	accountList.map((data, index) => {
		result.dataList.push(responseSchema.userList(data._source));
	});

	const response = {
		status: 200,
		message: 'Success',
		result,
	};

	return response;
};

/** 관리자/게스트 - 상세정보 */
export const getUserDetail = async (accountId) => {
	const dataResult = await userModel.findByUser(accountId);
	const account = dataResult.searchResult.body.hits.hits[0]._source;

	// Response 정의
	const result = responseSchema.userDetail(account);

	const response = {
		status: 200,
		message: 'Success',
		result,
	};

	return response;
};

/** 관리자/게스트 - 수정 */
export const updateUser = async (accountId, params) => {
	const { password, menuAuth } = params;

	// 비밀번호 변경이 있다면
	if (!isEmpty(password)) {
		const passwordInfo = await pbkdf2Encryption(password, null);
		params = { ...params, ...passwordInfo };
	}
	await userModel.updateByAdmin(accountId, params);
	const response = {
		status: 200,
		message: 'Success',
	};

	return response;
};

/** 계정 - 등록 */
export const insertUser = async (params) => {
	const { password, id } = params;

	// 아이디 중복체크
	const idCheckResult = await userModel.findByIdCheck(id);
	const idCount = idCheckResult.body.count;

	if (idCount === 0) {
		// 패스워드 암호화
		const passwordInfo = await pbkdf2Encryption(password, null);
		params = { ...params, ...passwordInfo };
		await userModel.adminInsert(params);
	} else {
		throw new CustomError(ErrorMessage.DUPLICATION_ID.status, ErrorMessage.DUPLICATION_ID.message);
	}

	const response = {
		status: 200,
		message: 'Success',
	};

	return response;
};

/** 관리자/게스트 - 삭제 */
export const deleteUser = async (accountId) => {
	await userModel.deleteUser(accountId);
	const response = {
		status: 200,
		message: 'Success',
	};

	return response;
};

/** 아이디 중복체크 */
export const checkUserId = async (accountId, params) => {
	const idCheckResult = await userModel.findByIdCheck(accountId);
	const idCount = idCheckResult.body.count;

	const response = {
		status: 200,
		message: 'Success',
		reslut: {
			accountCount: idCount,
		},
	};

	return response;
};

/** 계정 사용/중지 업데이트 */
export const updateByActivated = async (accountId, params) => {
	await userModel.updateByActivated(accountId, params);
	const response = {
		status: 200,
		message: 'Success',
	};

	return response;
};
