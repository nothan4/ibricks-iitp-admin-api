import * as yup from 'yup';
import { ValidateMessage } from '../../../message/validate-message.js';

/* 계정 아이디 Request */
export const accountId = {
	params: yup.object({
		accountId: yup.string().trim().required(),
	}),
};

/** 계정 사용/중지 Request */
export const activatedRequest = {
	body: yup.object({
		activated: yup.string().trim().oneOf(['Y', 'N']).required(),
	}),
};

/** 계정 리스트 Request */
export const listRequest = {
	query: yup.object({
		size: yup.number().positive().integer().default(10),
		page: yup.number().positive().integer().default(1),
		keyword: yup.string().trim(),
	}),
};

/** 등록 Request */
export const insertRequest = {
	body: yup.object({
		id: yup
			.string()
			.trim()
			.min(2, ValidateMessage.MINIMUM_ID)
			.max(10, ValidateMessage.MAXIMUM_ID)
			.required(ValidateMessage.REQUIRED_ID)
			.matches(/^[a-zA-Z][^!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?\s]*$/, ValidateMessage.ACCEPTED_FORMAT_ID),
		password: yup
			.string()
			.min(8, ValidateMessage.MINIMUM_PASSWORD)
			.max(16, ValidateMessage.MAXIMUM_PASSWORD)
			.required(ValidateMessage.REQUIRED_PASSWORD)
			.matches(
				/^(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?])[^\s]*$/,
				ValidateMessage.ACCEPTED_FORMAT_PASSWORD
			),
		name: yup
			.string()
			.trim()
			.min(2, ValidateMessage.MINIMUM_NAME)
			.max(10, ValidateMessage.MAXIMUM_NAME)
			.required(ValidateMessage.REQUIRED_NAME)
			.matches(/^[가-힣a-zA-Z][^!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?\s]*$/, ValidateMessage.ACCEPTED_FORMAT_NAME),
		menuAuth: yup.array(),
	}),
};

/** 비밀번호 변경 Request */
export const updatePasswordRequest = {
	body: yup.object({
		userId: yup
			.string()
			.trim()
			.min(2, ValidateMessage.MINIMUM_ID)
			.max(10, ValidateMessage.MAXIMUM_ID)
			.required(ValidateMessage.REQUIRED_ID)
			.matches(/^[a-zA-Z][^!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?\s]*$/, ValidateMessage.ACCEPTED_FORMAT_ID),
		userPw: yup
			.string()
			.min(8, ValidateMessage.MINIMUM_PASSWORD)
			.max(16, ValidateMessage.MAXIMUM_PASSWORD)
			.required(ValidateMessage.REQUIRED_PASSWORD)
			.matches(
				/^(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?])[^\s]*$/,
				ValidateMessage.ACCEPTED_FORMAT_PASSWORD
			),
		userNewPw: yup
			.string()
			.min(8, ValidateMessage.MINIMUM_PASSWORD)
			.max(16, ValidateMessage.MAXIMUM_PASSWORD)
			.required(ValidateMessage.REQUIRED_PASSWORD)
			.matches(
				/^(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?])[^\s]*$/,
				ValidateMessage.ACCEPTED_FORMAT_PASSWORD
			),
	}),
};
