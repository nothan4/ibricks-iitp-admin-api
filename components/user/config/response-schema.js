import dayjs from 'dayjs';
import { AccountIndex } from '../../../model/index.js';

/** 계정 리스트 */
export const userList = (data) => {
	return {
		pk: data[AccountIndex.pk],
		id: data[AccountIndex.id],
		name: data[AccountIndex.name],
		madeby: data[AccountIndex.madeby],
		menuAuth: data[AccountIndex.menuAuth],
		activated: data[AccountIndex.activated],
		regDate: dayjs(data[AccountIndex.regDate]).format('YYYY-MM-DD'),
	};
};

/** 계정 상세 정보 */
export const userDetail = (data) => {
	return {
		pk: data[AccountIndex.pk],
		id: data[AccountIndex.id],
		name: data[AccountIndex.name],
		madeby: data[AccountIndex.madeby],
		menuAuth: data[AccountIndex.menuAuth],
		activated: data[AccountIndex.activated],
		regDate: dayjs(data[AccountIndex.regDate]).format('YYYY-MM-DD'),
	};
};
