import { asyncWrapper } from '../../lib/middleware/async-wrapper.js';
import * as usersService from './users-service.js';
import * as responseSchema from './config/response-schema.js';

/** 관리자 - 리스트 */
export const userList = asyncWrapper(async (req, res, next) => {
	const params = req.validated.query;
	const response = await usersService.getUserList(params);
	res.send(response);
});

/** 관리자 - 상세정보 */
export const userDetail = asyncWrapper(async (req, res, next) => {
	const accountId = req.validated.params.accountId;
	const response = await usersService.getUserDetail(accountId);
	res.send(response);
});

/** 관리자 - 수정 */
export const userUpdate = asyncWrapper(async (req, res, next) => {
	const accountId = req.validated.params.accountId;
	const params = req.validated.body;
	const response = await usersService.updateUser(accountId, params);
	res.send(response);
});

/** 관리자 - 등록 */
export const userInsert = asyncWrapper(async (req, res, next) => {
	const params = req.validated.body;
	const response = await usersService.insertUser(params);
	res.send(response);
});

/** 관리자 - 삭제 */
export const userDelete = asyncWrapper(async (req, res, next) => {
	const accountId = req.validated.params.accountId;
	const response = await usersService.deleteUser(accountId);
	res.send(response);
});

/** 관리자/게스트 - 아이디 중복체크 */
export const userIdCheck = asyncWrapper(async (req, res, next) => {
	const accountId = req.validated.params.accountId;
	const params = req.validated.query;
	const response = await usersService.checkUserId(accountId, params);
	res.send(response);
});

/** 관리자/게스트 - 사용/중지 */
export const userActive = asyncWrapper(async (req, res, next) => {
	const accountId = req.validated.params.accountId;
	const params = req.validated.body;
	const response = await usersService.updateByActivated(accountId, params);
	res.send(response);
});
