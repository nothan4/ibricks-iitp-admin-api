import * as dotenv from 'dotenv';
import express from 'express';
import cookieParser from 'cookie-parser';
import swaggerUi from 'swagger-ui-express';
import morganMiddleware from '../lib/logger/morgan.js';
import jwtMiddleware from '../lib/middleware/jwt-middleware.js';
import { router as demandRouter } from './demand/index.js';
import { router as ictCodesRouter } from './ict-codes/index.js';
import { router as issuesRouter } from './issues/index.js';
import { router as accountsRouter } from './accounts/index.js';
import { router as guestsRouter } from './guests/index.js';
import { router as usersRouter } from './user/index.js';
import { errorHandler } from '../lib/middleware/error-handler.js';
import { NotFoundError } from '../lib/errors/not-found-error.js';
import { promises as fs } from 'fs';
import { Logger } from '../lib/logger/logger.js';
import cors from 'cors';

dotenv.config();

const logger = Logger(import.meta.url);
const swaggerDocument = await fs.readFile('./swagger/swagger-output.json');

export const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(morganMiddleware);
app.use(
	cors({
		origin: 'http://localhost:22000',
		credentials: true,
	})
);

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(JSON.parse(swaggerDocument)));
app.use('/accounts', accountsRouter);
app.use('/', jwtMiddleware);
app.use('/demand', demandRouter);
app.use('/ict-codes', ictCodesRouter);
app.use('/issues', issuesRouter);
app.use('/guests', guestsRouter);
app.use('/users', usersRouter);

app.use((req, res, next) => {
	next(new NotFoundError(`Cannot ${req.method} ${req.path}`));
});

app.use(errorHandler);

process.on('uncaughtException', (err) => {
	logger.error(`uncaughtException ${JSON.stringify(err)}`);
	process.exit(1);
});

process.on('unhandledRejection', (reason) => {
	logger.error(`unhandledRejection ${JSON.stringify(reason)}`);
	process.exit(1);
});
