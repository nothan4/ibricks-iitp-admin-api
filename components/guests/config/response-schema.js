import dayjs from 'dayjs';
import { AccountIndex } from '../../../model/index.js';
import { isEmpty } from '../../../util/index.js';

/** 계정 리스트 */
export const guestsList = (data) => {
	const activeStartDate = isEmpty(data[AccountIndex.activeStartDate])
		? ''
		: dayjs(data[AccountIndex.activeStartDate]).format('YYYY-MM-DD');
	const activeEndDate = isEmpty(data[AccountIndex.activeEndDate])
		? ''
		: dayjs(data[AccountIndex.activeEndDate]).format('YYYY-MM-DD');
	return {
		pk: data[AccountIndex.pk],
		id: data[AccountIndex.id],
		name: data[AccountIndex.name],
		madeby: data[AccountIndex.madeby],
		menuAuth: data[AccountIndex.menuAuth],
		activated: data[AccountIndex.activated],
		activeStartDate: activeStartDate,
		activeEndDate: activeEndDate,
		regDate: dayjs(data[AccountIndex.regDate]).format('YYYY-MM-DD'),
	};
};

/** 계정 상세 정보 */
export const guestDetail = (data) => {
	const activeStartDate = isEmpty(data[AccountIndex.activeStartDate])
		? ''
		: dayjs(data[AccountIndex.activeStartDate]).format('YYYY-MM-DD');
	const activeEndDate = isEmpty(data[AccountIndex.activeEndDate])
		? ''
		: dayjs(data[AccountIndex.activeEndDate]).format('YYYY-MM-DD');
	return {
		pk: data[AccountIndex.pk],
		id: data[AccountIndex.id],
		name: data[AccountIndex.name],
		madeby: data[AccountIndex.madeby],
		menuAuth: data[AccountIndex.menuAuth],
		activated: data[AccountIndex.activated],
		activeStartDate: activeStartDate,
		activeEndDate: activeEndDate,
		regDate: dayjs(data[AccountIndex.regDate]).format('YYYY-MM-DD'),
	};
};
