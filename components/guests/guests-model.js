import dayjs from 'dayjs';
import { search, update, write, searchCount, elDelete } from '../../lib/elasticsearch/client.js';
import { isEmpty, uuid } from '../../util/index.js';
import { ApiModel, AccountIndex, FieldModel } from '../../model/index.js';

/** 게스트 - 리스트 */
export const getGuests = async (params) => {
	const { size, page, keyword, activeStartDate, activeEndDate } = params;
	const dynamicMustQuery = [];

	// 이름 검색
	const keywordQuery = !isEmpty(keyword)
		? {
				simple_query_string: {
					query: keyword,
					fields: [AccountIndex.nameKeyword, AccountIndex.nameKobrick],
				},
		  }
		: null;
	if (keywordQuery != null) dynamicMustQuery.push(keywordQuery);

	// 공고 시작일 Range Query
	const startRangeQuery = isEmpty(activeStartDate)
		? null
		: {
				range: {
					[AccountIndex.activeEndDate]: {
						gte: activeStartDate,
					},
				},
		  };
	if (startRangeQuery != null) dynamicMustQuery.push(startRangeQuery);

	// 공고 종료일 Range Query
	const endRangeQuery = isEmpty(activeEndDate)
		? null
		: {
				range: {
					[AccountIndex.activeStartDate]: {
						lte: activeEndDate,
					},
				},
		  };
	if (endRangeQuery != null) dynamicMustQuery.push(endRangeQuery);

	// 계정 타입
	const madebyQuery = {
		match: {
			[AccountIndex.madeby]: 'GUEST',
		},
	};
	dynamicMustQuery.push(madebyQuery);

	const query = {
		index: AccountIndex.index,
		body: {
			size: size,
			from: (page - 1) * size,
			sort: {
				[AccountIndex.regDate]: {
					order: 'desc',
				},
			},
			query: {
				bool: {
					must: dynamicMustQuery,
				},
			},
			_source: [
				AccountIndex.pk,
				AccountIndex.id,
				AccountIndex.name,
				AccountIndex.madeby,
				AccountIndex.menuAuth,
				AccountIndex.activated,
				AccountIndex.activeStartDate,
				AccountIndex.activeEndDate,
				AccountIndex.regDate,
			],
		},
	};
	const searchResult = await search(query);

	return { searchResult };
};

/** 게스트 - 상세정보 */
export const findByGuest = async (accountId) => {
	const query = {
		index: AccountIndex.index,
		body: {
			query: {
				bool: {
					filter: [
						{
							term: {
								[AccountIndex.pk]: accountId,
							},
						},
					],
				},
			},
			_source: [
				AccountIndex.pk,
				AccountIndex.id,
				AccountIndex.name,
				AccountIndex.madeby,
				AccountIndex.menuAuth,
				AccountIndex.activated,
				AccountIndex.activeStartDate,
				AccountIndex.activeEndDate,
				AccountIndex.regDate,
			],
		},
	};
	const searchResult = await search(query);

	return { searchResult };
};

/** 게스트 계정 - 수정 */
export const updateByAccountGuest = async (accountId, params) => {
	const { password, menuAuth, useStartDt, useEndDt, salt } = params;
	const index = AccountIndex.index;
	const _id = accountId;
	const payload = isEmpty(password)
		? {
				[AccountIndex.menuAuth]: menuAuth,
				[AccountIndex.activeStartDate]: isEmpty(useStartDt) ? null : useStartDt,
				[AccountIndex.activeEndDate]: isEmpty(useEndDt) ? null : useEndDt,
		  }
		: {
				[AccountIndex.password]: password,
				[AccountIndex.salt]: salt,
				[AccountIndex.menuAuth]: menuAuth,
				[AccountIndex.activeStartDate]: isEmpty(useStartDt) ? null : useStartDt,
				[AccountIndex.activeEndDate]: isEmpty(useEndDt) ? null : useEndDt,
		  };

	const result = await update(index, _id, payload);

	return result;
};

/** 계정 아이디 중복체크 */
export const findByIdCheck = async (id) => {
	const index = AccountIndex.index;
	const payload = {
		query: {
			bool: {
				must: [
					{
						match: {
							[AccountIndex.id]: id,
						},
					},
					{
						match: {
							[AccountIndex.madeby]: 'GUEST',
						},
					},
				],
			},
		},
	};
	const result = await searchCount(index, payload);

	return result;
};

/** 게스트 - 등록 */
export const guestInsert = async (params) => {
	const { id, password, name, menuAuth, salt, useStartDt, useEndDt } = params;
	const index = AccountIndex.index;
	const madeby = 'GUEST';
	const _id = `${madeby}-${uuid()}`;
	const nowDate = dayjs().format('YYYY-MM-DD HH:mm:ss');
	const payload = {
		[AccountIndex.pk]: _id,
		[AccountIndex.id]: id,
		[AccountIndex.password]: password,
		[AccountIndex.salt]: salt,
		[AccountIndex.madeby]: madeby,
		[AccountIndex.name]: name,
		[AccountIndex.menuAuth]: menuAuth,
		[AccountIndex.regDate]: nowDate,
		[AccountIndex.activated]: true,
		[AccountIndex.activeStartDate]: isEmpty(useStartDt) ? null : useStartDt,
		[AccountIndex.activeEndDate]: isEmpty(useEndDt) ? null : useEndDt,
	};
	const result = await write(index, '_doc', payload, _id);

	return result;
};

/** 계정 삭제 */
export const deleteGuest = async (accountId) => {
	const index = AccountIndex.index;
	const _id = accountId;
	const result = await elDelete(index, _id);

	return result;
};

/** 계정 사용/중지 업데이트 */
export const updateByActivated = async (accountId, params) => {
	const { activated } = params;
	const index = AccountIndex.index;
	const _id = accountId;
	const userYn = activated === 'Y' ? true : false;
	const payload = {
		[AccountIndex.activated]: userYn,
	};
	const result = await update(index, _id, payload);

	return result;
};
