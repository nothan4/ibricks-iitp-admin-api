import { asyncWrapper } from '../../lib/middleware/async-wrapper.js';
import * as guestsService from './guests-service.js';

/** 게스트 - 리스트 */
export const guestList = asyncWrapper(async (req, res, next) => {
	const params = req.validated.query;
	const response = await guestsService.getGuestList(params);
	res.send(response);
});

/** 게스트 - 상세정보 */
export const guestDetail = asyncWrapper(async (req, res, next) => {
	const accountId = req.validated.params.accountId;
	const response = await guestsService.getGuestDetail(accountId);
	res.send(response);
});

/** 게스트 - 수정 */
export const guestUpdate = asyncWrapper(async (req, res, next) => {
	const accountId = req.validated.params.accountId;
	const params = req.validated.body;
	const response = await guestsService.updateGuest(accountId, params);
	res.send(response);
});

/** 게스트 - 등록 */
export const guestInsert = asyncWrapper(async (req, res, next) => {
	const params = req.validated.body;
	const response = await guestsService.insertGuest(params);
	res.send(response);
});

/** 게스트 - 삭제 */
export const guestDelete = asyncWrapper(async (req, res, next) => {
	const accountId = req.validated.params.accountId;
	const response = await guestsService.deleteGuest(accountId);
	res.send(response);
});

/** 게스트 - 아이디 중복체크 */
export const guestIdCheck = asyncWrapper(async (req, res, next) => {
	const accountId = req.validated.params.accountId;
	const response = await guestsService.checkAccountId(accountId);
	res.send(response);
});

/** 게스트 - 사용/중지 */
export const guestActive = asyncWrapper(async (req, res, next) => {
	const accountId = req.validated.params.accountId;
	const params = req.validated.body;
	const response = await guestsService.updateByActivated(accountId, params);
	res.send(response);
});
