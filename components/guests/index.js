import express from 'express';
import * as schema from './config/request-schema.js';
import { validate } from '../../lib/middleware/validate.js';
import * as guestsController from './guests-controller.js';

export const router = express.Router();

/* 게스트 계정 리스트 */
router.get(
	'',
	validate(schema.listRequest),
	guestsController.guestList
	/*
  #swagger.tags = ['guests']
  #swagger.summary = '게스트 계정 목록'
  #swagger.description = '게스트 계정 목록'
  #swagger.parameters['size'] = { 
    in: 'query',
    description: '응답 결과수',
    required: false,
    type: 'integer',
    default: 10
  }
  #swagger.parameters['page'] = { 
    in: 'query',
    description: '페이지',
    required: false,
    type: 'integer',
    default: 1
  }
  #swagger.parameters['keyword'] = { 
    in: 'query',
    description: '게스트명',
    required: false,
    type: 'string'
  }
  #swagger.parameters['activeStartDate'] = { 
    in: 'query',
    description: '사용 시작일',
    required: false,
    type: 'string'
  }
  #swagger.parameters['activeEndDate'] = { 
    in: 'query',
    description: '사용 종료일',
    required: false,
    type: 'string'
  }
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS',
      result: [
        {
          userId: '',
          userName: '',
          menuAuth: '',
          registrDate: ''
        }
      ]
    }
  }
  */
);

/* 게스트 상세 정보 */
router.get(
	'/:accountId',
	validate(schema.accountId),
	guestsController.guestDetail
	/*
  #swagger.tags = ['guests']
  #swagger.summary = '게스트 계정 상세 정보'
  #swagger.description = '게스트 계정 상세 정보'
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS',
      result: {
        itemList: [
          {
            userId: '',
            userName: '',
            menuAuth: '',
            registrDate: ''
          }
        ]
      }
    }
  }
  */
);

/* 게스트 계정 수정 */
router.put(
	'/:accountId',
	validate(schema.accountId),
	guestsController.guestUpdate
	/*
  #swagger.tags = ['guests']
  #swagger.summary = '게스트 계정 수정'
  #swagger.description = '게스트 계정 수정'
  #swagger.parameters['body'] = {
    in: 'body',
    description: 'request body',
    schema: {
      password: '',
      menuAuth: '',
      useStartDt: '',
      useEndDt: ''
    }
  }
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS',
      result: [
        {
          userId: '',
          userName: '',
          menuAuth: '',
          registrDate: ''
        }
      ]
    }
  }
  */
);

/* 게스트 계정 등록 */
router.post(
	'',
	validate(schema.insertRequest),
	guestsController.guestInsert
	/*
  #swagger.tags = ['guests']
  #swagger.summary = '게스트 계정 등록'
  #swagger.description = '게스트 계정 등록'
  #swagger.parameters['body'] = {
    in: 'body',
    description: 'request body',
    schema: {
      userId: '',
      userPw: '',
      userName: '',
      menuAuth: '',
      registrDate: ''
    }
  }
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS'
    }
  }
  */
);

/* 게스트 계정 삭제 */
router.delete(
	'/:accountId',
	validate(schema.accountId),
	guestsController.guestDelete
	/*
  #swagger.tags = ['guests']
  #swagger.summary = '게스트 계정 삭제'
  #swagger.description = '게스트 계정 삭제'
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS'
    }
  }
  */
);

/* 게스트 아이디 중복체크 */
router.get(
	'/check/:accountId',
	validate(schema.accountId),
	guestsController.guestIdCheck
	/*
  #swagger.tags = ['guests']
  #swagger.summary = '게스트 아이디 중복체크'
  #swagger.description = '게스트 아이디 중복체크'
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS'
    }
  }
  */
);

/* 게스트 사용/중지 */
router.patch(
	'/:accountId',
	validate(schema.accountId),
	validate(schema.activatedRequest),
	guestsController.guestActive
	/*
  #swagger.tags = ['guests']
  #swagger.summary = '게스트 사용/중지'
  #swagger.description = '게스트 사용/중지'
  #swagger.parameters['body'] = {
    in: 'body',
    description: 'request body',
    schema: {
      activated: true
    }
  }
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS'
    }
  }
  */
);
