import fs from 'fs';
import excelJs from 'exceljs';
import excelToJson from 'convert-excel-to-json';
import * as ictCodesModel from './ict-codes-model.js';
import { IctCodeTable, IctCodeMappTable } from '../../model/index.js';
import * as responseSchema from './config/response-schema.js';
import uuid from '../../util/uuid.js';

/** ICT 분류체계 년도 리스트 */
export const getIctCodeYears = async () => {
	const result = {
		dataList: [],
	};
	const yearList = await ictCodesModel.years();

	yearList.map((data, index) => {
		result.dataList.push(data[IctCodeTable.year]);
	});

	const response = {
		status: 200,
		message: 'Success',
		result,
	};

	return response;
};

/** ICT 분류체계 관리 목록 */
export const getIctCodes = async (params) => {
	// Response 정의
	const result = {
		totalCount: 0,
		dataList: [],
	};
	const dataObject = await ictCodesModel.ictCodes(params);
	const totalCount = dataObject.totalCount[0].CNT;
	result.totalCount = totalCount;
	const dataList = dataObject.dataList;

	dataList.map((data, index) => {
		result.dataList.push(responseSchema.ictCodes(data));
	});

	const response = {
		status: 200,
		message: 'Success',
		result,
	};

	return response;
};

/** 분류체계 삭제 */
export const deleteIctCodes = async (params) => {
	const { ictCodes } = params;
	const response = {
		status: 200,
		message: 'Success',
	};
	const newIctCodes = JSON.parse(ictCodes);
	// 1. 분류체계 삭제
	await ictCodesModel.deleteCodes(newIctCodes);

	// 2. 매핑된 분류체계 업데이트
	await ictCodesModel.updateByMappingCodes(newIctCodes);

	return response;
};

/** 분류체계 업로드 */
export const uploadIctCodes = async (file) => {
	const { path } = file;
	const sheet1 = 'ICT분류코드상세';
	const sheet2 = 'ICT분류매핑코드';
	/**
	 * Header Sheet1(접수서 리스트)
	 * 분류코드|코드명|대중소세구분|ICT분류년도|상위분류코드
	 * Header Sheet2(접수서 첨부파일 리스트)
	 * 분류코드|ICT분류년도|매핑코드
	 */
	const excelToJsonData = excelToJson({
		sourceFile: path,
		sheets: [
			{
				name: sheet1,
				header: {
					rows: 1,
				},
				columnToKey: {
					A: 'ictCd',
					B: 'ictNn',
					C: 'type',
					D: 'year',
					E: 'upperIctCd',
				},
			},
			{
				name: sheet2,
				header: {
					rows: 1,
				},
				columnToKey: {
					A: 'ictCd',
					B: 'year',
					C: 'mappingIctCd',
				},
			},
		],
	});
	const dataListSheet1 = excelToJsonData[sheet1];
	const dataListSheet2 = excelToJsonData[sheet2];

	const response = {
		status: 200,
		message: 'Success',
	};

	// 업로드 파일 삭제
	fs.rm(path, function (err) {
		if (err) throw err;
	});

	// db 저장
	const dbResult = await ictCodesModel.uploadByIctCodes(dataListSheet1, dataListSheet2);

	if (!dbResult) {
		response.status = 500;
		response.message = 'Fail';
	}

	return response;
};

/** 최신 ICT 분류체계 관리 목록 */
export const getLastIctCodes = async (keyword) => {
	// Response 정의
	const result = {
		dataList: [],
	};
	const dataObject = await ictCodesModel.lastIctCodes(keyword);
	const dataList = dataObject.dataList;

	dataList.map((data, index) => {
		result.dataList.push(responseSchema.lastIctCodes(data));
	});

	const response = {
		status: 200,
		message: 'Success',
		result,
	};

	return response;
};

/** 분류체계 전체 매핑 다운로드 */
export const downloadIctCodes = async () => {
	const codeList = await ictCodesModel.findByLastCodes();
	const codesLv1 = codeList.filter((val) => val[IctCodeTable.lv] === 1);
	const codesLv2 = codeList.filter((val) => val[IctCodeTable.lv] === 2);
	const codesLv3 = codeList.filter((val) => val[IctCodeTable.lv] === 3);
	const codesLv4 = codeList.filter((val) => val[IctCodeTable.lv] === 4);
	const yearList = await ictCodesModel.findByNotLastYears();
	const codeMappList = await ictCodesModel.findByNotLastCodeMappings();
	const years = yearList.map((obj) => obj[IctCodeMappTable.year]);
	const resultList = [];

	// Ready to Excel Sheet
	const wb = new excelJs.Workbook();
	const ws = wb.addWorksheet('sheet1');
	const header = ['대분류', '중분류', '소분류', '세분류', ...years];
	// Add to Header
	resultList.push(header);

	// Add to Body
	for (const cnt4 in codesLv4) {
		const lv4 = codesLv4[cnt4];
		const cdLv4 = lv4[IctCodeTable.ictTcclsCd];
		const nmLv4 = lv4[IctCodeTable.tcclsNm];
		const upperLv4 = lv4[IctCodeTable.upprTcclsCd];
		const lv4Data = `${nmLv4}(${cdLv4})`;
		for (const cnt3 in codesLv3) {
			const lv3 = codesLv3[cnt3];
			const cdLv3 = lv3[IctCodeTable.ictTcclsCd];
			const nmLv3 = lv3[IctCodeTable.tcclsNm];
			const upperLv3 = lv3[IctCodeTable.upprTcclsCd];
			const lv3Data = `${nmLv3}(${cdLv3})`;
			if (upperLv4 === cdLv3) {
				for (const cnt2 in codesLv2) {
					const lv2 = codesLv2[cnt2];
					const cdLv2 = lv2[IctCodeTable.ictTcclsCd];
					const nmLv2 = lv2[IctCodeTable.tcclsNm];
					const upperLv2 = lv2[IctCodeTable.upprTcclsCd];
					const lv2Data = `${nmLv2}(${cdLv2})`;
					if (upperLv3 === cdLv2) {
						for (const cnt1 in codesLv1) {
							const lv1 = codesLv1[cnt1];
							const cdLv1 = lv1[IctCodeTable.ictTcclsCd];
							const nmLv1 = lv1[IctCodeTable.tcclsNm];
							const lv1Data = `${nmLv1}(${cdLv1})`;
							const yearList = [];
							if (upperLv2 === cdLv1) {
								for (const index in years) {
									let oldString = '';
									const year = years[index];
									const yearCount = Number(index) + 1;
									for (const cnt in codeMappList) {
										const mappingObj = codeMappList[cnt];
										const oldCd = mappingObj[IctCodeMappTable.code];
										const oldNm = mappingObj[IctCodeTable.tcclsNm];
										const oldUpperCd = mappingObj[IctCodeMappTable.upperCode];
										if (yearCount === 1) {
											if (cdLv4 === oldUpperCd) {
												oldString = `${oldString} ${oldNm}(${oldCd})`;
											}
										} else {
											// 이후 추가 되는 년도 계산 필요 일단 개발시간이 부족해 추후에 개발...
										}
									}
									yearList.push(oldString);
								}
								resultList.push([lv1Data, lv2Data, lv3Data, lv4Data, ...yearList]);
							}
						}
					}
				}
			}
		}
	}
	ws.addRows(resultList);
	ws.columns = header.map((column) => ({
		header: column,
		key: column,
		width: 30,
		style: {
			font: {
				size: 12,
			},
			alignment: {
				vertical: 'middle',
				horizontal: 'center',
				wrapText: true,
			},
		},
	}));

	return wb;
};
