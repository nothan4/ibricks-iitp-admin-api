import express from 'express';
import multer from 'multer';
import * as schema from './config/request-schema.js';
import * as ictCodesController from './ict-codes-controller.js';
import { validate } from '../../lib/middleware/validate.js';
import fileUploadStorage from '../../lib/middleware/file-upload-storage.js';

export const router = express.Router();

const upload = multer({ storage: fileUploadStorage });

/* ICT 분류체계 년도 리스트 */
router.get(
	'/year',
	ictCodesController.ictCodesYear
	/*
  #swagger.tags = ['ict-codes']
  #swagger.summary = 'ICT 분류체계 년도 리스트'
  #swagger.description = 'ICT 분류체계 년도 리스트'
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS',
      result: {
        itemList: [
          '2018',
          '2019'
        ]
      }
    }
  }
  */
);

/* ICT 분류체계 관리 목록 */
router.post(
	'',
	validate(schema.ictCodeList),
	ictCodesController.ictCodesList
	/*
  #swagger.tags = ['ict-codes']
  #swagger.summary = 'ICT 분류체계 관리 목록'
  #swagger.description = 'ICT 분류체계 관리 목록'
  #swagger.parameters['body'] = {
    in: 'body',
    description: 'request body',
    schema: {
      size: 10,
      page: 1,
      year: '',
      searchKeyword: '',
    }
  }
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS',
      result: {
        itemList: [
          {
            ictCode: '10010303',
            ictName: '데이터센터 네트워크 시스템',
            ictYear: '2019',
            ictTypeName: '세분류',
            ictUpperCode: '10010300',
            ictMappingCode: '10010303',
            ictMappingName: '데이터센터 네트워크 시스템'
          }
        ]
      }
    }
  }
  */
);

/* ICT 분류체계 관리 삭제 */
router.delete(
	'',
	validate(schema.ictCodes),
	ictCodesController.ictCodesDelete
	/*
  #swagger.tags = ['ict-codes']
  #swagger.summary = 'ICT 분류체계 삭제'
  #swagger.description = 'ICT 분류체계 삭제'
  #swagger.parameters['body'] = {
    in: 'body',
    description: 'request body',
    schema: {
      ictCodes: []
    }
  }
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS'
    }
  }
  */
);

/* ICT 분류체계 업로드(엑셀 파일) */
router.post(
	'/upload',
	upload.single('ictFile'),
	ictCodesController.ictCodesUpload
	/*
  #swagger.tags = ['ict-codes']
  #swagger.summary = 'ICT 분류체계 업로드(엑셀 파일)'
  #swagger.description = 'ICT 분류체계 업로드(엑셀 파일)'
  #swagger.consumes = ['multipart/form-data']
  #swagger.parameters['ictFile'] = {
    in: 'formData',
    type: 'file',
    required: 'true',
    description: '분류체계 등록 엑셀파일',
  }
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS'
    }
  }
  */
);

/* 최신 ICT 분류체계 목록 */
router.get(
	'/last',
	validate(schema.keyword),
	ictCodesController.ictCodesLastList
	/*
  #swagger.tags = ['ict-codes']
  #swagger.summary = '최신 ICT 분류체계 목록'
  #swagger.description = '최신 ICT 분류체계 목록'
  #swagger.parameters['keyword'] = { 
    in: 'query',
    description: '세분류 검색',
    required: false,
    type: 'string'
  }
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS',
      result: {
        itemList: [
          {
            ictCode: '10010303',
            ictName: '데이터센터 네트워크 시스템',
            lv: 4
          }
        ]
      }
    }
  }
  */
);

/* ICT 분류체계 매핑정보 다운로드 */
router.post(
	'/download',
	ictCodesController.ictCodesDownload
	/*
  #swagger.tags = ['ict-codes']
  #swagger.summary = 'ICT 분류체계 매핑정보 다운로드'
  #swagger.description = 'ICT 분류체계 매핑정보 다운로드'
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS'
    }
  }
  */
);
