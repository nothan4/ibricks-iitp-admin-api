import dayjs from 'dayjs';
import { DemandNoticeIndex, DemandSurveyIndex, IctCodeTable } from '../../../model/index.js';

/** ICT 분류체계 목록 */
export const ictCodes = (data) => {
	return {
		ictCode: data[IctCodeTable.ictTcclsCd],
		ictName: data[IctCodeTable.tcclsNm],
		ictYear: data[IctCodeTable.year],
		ictTypeName: data[IctCodeTable.lv],
		ictUpperCode: data[IctCodeTable.upprTcclsCd],
		ictUpperName: data[IctCodeTable.upprTcclsNm],
		ictMappingCode: data[IctCodeTable.ictMappingCode],
		ictMappingName: data[IctCodeTable.ictMappingName],
		ictMappingYear: data[IctCodeTable.ictMappingYear],
	};
};

/** 최신 ICT 분류체계 목록 */
export const lastIctCodes = (data) => {
	return {
		ictCode: data[IctCodeTable.ictTcclsCd],
		ictName: data[IctCodeTable.tcclsNm],
		ictYear: data[IctCodeTable.year],
		ictTypeName: data[IctCodeTable.lv],
		ictUpperCode: data[IctCodeTable.upprTcclsCd],
	};
};
