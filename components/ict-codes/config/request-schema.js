import * as yup from 'yup';

/* ICT 분류체계 관리 목록 Request */
export const ictCodeList = {
	body: yup.object({
		size: yup.number().positive().integer().default(10),
		page: yup.number().positive().integer().default(1),
		year: yup.string().trim(),
		searchKeyword: yup.string().trim(),
	}),
};

/** 최신 ICT 분류체계 목록 Request */
export const keyword = {
	query: yup.object({
		keyword: yup.string().trim(),
	}),
};

/** 분류체계 삭제 Request */
export const ictCodes = {
	params: yup.object({
		ictCodes: yup.array(),
	}),
};
