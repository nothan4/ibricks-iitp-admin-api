import dbConnection from '../../lib/knex/db-connection.js';
import { FieldModel, IctCodeTable, IctCodeMappTable } from '../../model/index.js';
import { isEmpty } from '../../util/index.js';

/** ICT 분류체계명 조회 */
export const ictCodeNames = async (codes) => {
	const result = await dbConnection(IctCodeTable.index)
		.select(IctCodeTable.ictTcclsCd, IctCodeTable.tcclsNm)
		.whereIn(IctCodeTable.ictTcclsCd, codes);
	return result;
};

/** ICT 분류체계 년도 리스트 */
export const years = async () => {
	const result = await dbConnection(IctCodeTable.index).distinct(IctCodeTable.year).orderBy(IctCodeTable.year, 'asc');

	return result;
};

/** ICT 분류체계 업로드(MariaDb) */
export const uploadByIctCodes = async (dataListSheet1, dataListSheet2) => {
	let result = false;
	const codeList = [];
	// 1. 분류체계 등록
	for (const cnt in dataListSheet1) {
		const item = dataListSheet1[cnt];
		const ictCd = item.ictCd;
		const ictNn = item.ictNn;
		const lv = FieldModel.ictCodeTypes.findIndex((value) => value == item.type);
		const year = item.year;
		const upperIctCd = item.upperIctCd;
		const obj = {
			[IctCodeTable.ictTcclsCd]: ictCd,
			[IctCodeTable.tcclsNm]: ictNn,
			[IctCodeTable.lv]: lv + 1,
			[IctCodeTable.year]: year,
			[IctCodeTable.upprTcclsCd]: upperIctCd,
		};
		codeList.push(obj);
	}
	const codesResult = await dbConnection(IctCodeTable.index)
		.insert(codeList)
		.onConflict([IctCodeTable.ictTcclsCd])
		.merge();

	// 2. 분류체계 매핑 등록
	if (codesResult[0] === 0) {
		const codeMappingList = [];
		// 2.1 기존 과거 매핑 코드 업데이트
		const dbResult = await dbConnection.transaction(async (trx) => {
			for (const cnt in dataListSheet2) {
				const item = dataListSheet2[cnt];
				const ictCd = item.ictCd;
				const mappingIctCd = item.mappingIctCd;
				const year = item.year;
				const obj = {
					[IctCodeMappTable.code]: ictCd,
					[IctCodeMappTable.year]: year,
				};
				codeMappingList.push(obj);
				await trx(IctCodeMappTable.index)
					.where(IctCodeMappTable.code, '=', mappingIctCd)
					.update({ [IctCodeMappTable.upperCode]: ictCd });
			}
		});
		// 2.2. 신규 코드 등록
		const codeMappResult = await dbConnection(IctCodeMappTable.index)
			.insert(codeMappingList)
			.onConflict([IctCodeMappTable.code])
			.merge();
		if (codeMappResult[0] === 0) {
			result = true;
		}
	}

	return result;
};

/** ICT 분류체계 관리 목록 */
export const ictCodes = async (params) => {
	const { size, page, year, searchKeyword } = params;
	const pageQuery = (page - 1) * size;
	let yearQuery = '';
	if (!isEmpty(year) && year != 'ALL') {
		yearQuery = ` AND A.YEAR = '${year}'`;
	}

	let searchQuery = '';
	if (!isEmpty(searchKeyword)) {
		searchQuery = ` AND A.TCCLS_NM like '%${searchKeyword}%'`;
	}

	// 1. 카운트
	const countQuery = `
		SELECT
			COUNT(*) AS CNT
		FROM IITP_ICT_TCCLS_CD A
		LEFT JOIN IITP_ICT_CD_MAPP B 
			ON A.ICT_TCCLS_CD = B.CODE 
		WHERE A.LV != 0
		${yearQuery}
		${searchQuery}
	`;

	const totalCount = await dbConnection.raw(countQuery);

	// 2. 데이터
	const selectQuery = `
		SELECT
			A.ICT_TCCLS_CD AS ICT_TCCLS_CD
			, A.TCCLS_NM AS TCCLS_NM
			, A.YEAR AS YEAR
			, CASE 
				WHEN A.LV = 4 THEN '세분류'
				WHEN A.LV = 3 THEN '소분류'
				WHEN A.LV = 2 THEN '중분류'
				WHEN A.LV = 1 THEN '대분류'
			END AS LV
			, A.UPPR_TCCLS_CD AS UPPR_TCCLS_CD
			, ( SELECT TCCLS_NM FROM IITP_ICT_TCCLS_CD WHERE ICT_TCCLS_CD = A.UPPR_TCCLS_CD ) AS UPPR_TCCLS_NM
			, B.UPPER_CODE AS ICT_MAPPING_CODE
			, ( SELECT TCCLS_NM FROM IITP_ICT_TCCLS_CD WHERE ICT_TCCLS_CD = B.UPPER_CODE ) AS ICT_MAPPING_NAME
			, ( SELECT YEAR FROM IITP_ICT_TCCLS_CD WHERE ICT_TCCLS_CD = B.UPPER_CODE ) AS ICT_MAPPING_YEAR
		FROM IITP_ICT_TCCLS_CD A
		LEFT JOIN IITP_ICT_CD_MAPP B 
			ON A.ICT_TCCLS_CD = B.CODE 
		WHERE A.LV != 0
		${yearQuery}
		${searchQuery}
		LIMIT ${size}
		OFFSET ${pageQuery}
	`;
	const dataList = await dbConnection.raw(selectQuery);

	const result = {
		totalCount: totalCount[0],
		dataList: dataList[0],
	};

	return result;
};

/** 분류체계 삭제 */
export const deleteCodes = async (ictCodes) => {
	await dbConnection(IctCodeTable.index).whereIn(IctCodeTable.ictTcclsCd, ictCodes).del();
	const result = await dbConnection(IctCodeMappTable.index).whereIn(IctCodeMappTable.code, ictCodes).del();

	return result;
};

/** 매핑 분류체계 업데이트 */
export const updateByMappingCodes = async (ictCodes) => {
	const result = await dbConnection(IctCodeMappTable.index)
		.whereIn(IctCodeMappTable.upperCode, ictCodes)
		.update({
			[IctCodeMappTable.upperCode]: null,
		});

	return result;
};

/** 최신 ict 분류체계 목록 */
export const lastIctCodes = async (keyword) => {
	let selectQuery = '';
	if (isEmpty(keyword)) {
		selectQuery = `
			SELECT
				A.ICT_TCCLS_CD AS ICT_TCCLS_CD
				, A.TCCLS_NM AS TCCLS_NM
				, A.YEAR AS YEAR
				, A.LV AS LV
				, A.UPPR_TCCLS_CD AS UPPR_TCCLS_CD
			FROM IITP_ICT_TCCLS_CD A
			WHERE YEAR = ( SELECT DISTINCT YEAR FROM IITP_ICT_TCCLS_CD ORDER BY YEAR DESC LIMIT 1)
				AND A.LV != 0
			ORDER BY A.ICT_TCCLS_CD ASC
		`;
	} else {
		selectQuery = `
			WITH RECURSIVE TREE AS (
				SELECT 
						A.ICT_TCCLS_CD
						, A.TCCLS_NM
						, A.UPPR_TCCLS_CD
						, A.LV 
						, A.YEAR
				FROM IITP_ICT_TCCLS_CD A
				WHERE ICT_TCCLS_CD IN (
					SELECT
					A.ICT_TCCLS_CD AS ICT_TCCLS_CD
				FROM IITP_ICT_TCCLS_CD A
				WHERE YEAR = ( SELECT DISTINCT YEAR FROM IITP_ICT_TCCLS_CD ORDER BY YEAR DESC LIMIT 1)
					AND A.LV = ( SELECT DISTINCT LV FROM IITP_ICT_TCCLS_CD ORDER BY YEAR DESC, LV DESC LIMIT 1 )
					AND A.TCCLS_NM LIKE '%${keyword}%'
				ORDER BY A.ICT_TCCLS_CD ASC
				)
				UNION ALL
				SELECT 
						B.ICT_TCCLS_CD
						, B.TCCLS_NM
						, B.UPPR_TCCLS_CD
						, B.LV 
						, B.YEAR
				FROM IITP_ICT_TCCLS_CD B
				INNER JOIN TREE C 
					ON B.ICT_TCCLS_CD = C.UPPR_TCCLS_CD
				WHERE B.LV >= 1
			) 
			SELECT 
				DISTINCT R.ICT_TCCLS_CD -- ICT 분류 코드
				, R.TCCLS_NM -- 분류명
				, R.YEAR
				, R.LV -- 단계
				, R.UPPR_TCCLS_CD
			FROM TREE R
			ORDER BY R.ICT_TCCLS_CD ASC
		`;
	}

	const dataList = await dbConnection.raw(selectQuery);

	const result = {
		dataList: dataList[0],
	};

	return result;
};

/** 마지막 분류체계 리스트 */
export const findByLastCodes = async () => {
	const selectQuery = `
		SELECT 
			A.ICT_TCCLS_CD AS ICT_TCCLS_CD
			, A.TCCLS_NM AS TCCLS_NM 
			, A.UPPR_TCCLS_CD AS UPPR_TCCLS_CD 
			, A.LV AS LV
			, A.YEAR AS YEAR
		FROM IITP_ICT_TCCLS_CD A
		WHERE YEAR = ( SELECT YEAR FROM IITP_ICT_TCCLS_CD ORDER BY YEAR DESC LIMIT 1 )
			AND LV != 0
		ORDER BY ICT_TCCLS_CD ASC
	`;
	const dataList = await dbConnection.raw(selectQuery);

	return dataList[0];
};

/** 전체 분류체계 매핑 리스트(최신년도 제거) */
export const findByNotLastCodeMappings = async () => {
	const selectQuery = `
		SELECT 
			A.CODE AS CODE
			, B.TCCLS_NM AS TCCLS_NM
			, A.UPPER_CODE AS UPPER_CODE
			, A.YEAR AS YEAR
		FROM IITP_ICT_CD_MAPP A
		LEFT JOIN IITP_ICT_TCCLS_CD B
			ON A.CODE = B.ICT_TCCLS_CD 
		WHERE A.YEAR != ( SELECT YEAR FROM IITP_ICT_TCCLS_CD ORDER BY YEAR DESC LIMIT 1 ) 
		ORDER BY A.YEAR, A.UPPER_CODE ASC
	`;
	const dataList = await dbConnection.raw(selectQuery);

	return dataList[0];
};

/** 전체 분류체계 년도(최신년도 제거) */
export const findByNotLastYears = async () => {
	const selectQuery = `
		SELECT 
			DISTINCT A.YEAR AS YEAR
		FROM IITP_ICT_CD_MAPP A
		WHERE A.YEAR != ( SELECT YEAR FROM IITP_ICT_TCCLS_CD ORDER BY YEAR DESC LIMIT 1 ) 
		ORDER BY YEAR DESC
	`;
	const dataList = await dbConnection.raw(selectQuery);

	return dataList[0];
};
