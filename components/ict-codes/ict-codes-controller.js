import dayjs from 'dayjs';
import contentDisposition from 'content-disposition';
import { asyncWrapper } from '../../lib/middleware/async-wrapper.js';
import * as ictCodesService from './ict-codes-service.js';

/** ICT 분류체계 년도 리스트 */
export const ictCodesYear = asyncWrapper(async (req, res, next) => {
	const response = await ictCodesService.getIctCodeYears();
	res.send(response);
});

/** ICT 분류체계 관리 목록 */
export const ictCodesList = asyncWrapper(async (req, res, next) => {
	const params = req.validated.body;
	const response = await ictCodesService.getIctCodes(params);
	res.send(response);
});

/** ICT 분류체계 삭제 */
export const ictCodesDelete = asyncWrapper(async (req, res, next) => {
	const params = req.validated.body;
	const response = await ictCodesService.deleteIctCodes(params);
	res.send(response);
});

/** ICT 분류체계 업로드(엑셀 파일) */
export const ictCodesUpload = asyncWrapper(async (req, res, next) => {
	const file = req.file;
	const response = await ictCodesService.uploadIctCodes(file);
	res.send(response);
});

/** 최신 ICT 분류체계 목록 */
export const ictCodesLastList = asyncWrapper(async (req, res, next) => {
	const keyword = req.validated.query.keyword;
	const response = await ictCodesService.getLastIctCodes(keyword);
	res.send(response);
});

/** ICT 분류체계 매핑정보 다운로드 */
export const ictCodesDownload = asyncWrapper(async (req, res, next) => {
	const date = dayjs().format('YYYYMMDD');
	const fileName = `${date}_ICT_MAPPING_CODE.xlsx`;
	res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	res.setHeader('Content-Disposition', contentDisposition(fileName));
	res.setHeader('Access-Control-Expose-Headers', '*');
	const response = await ictCodesService.downloadIctCodes();
	await response.xlsx.write(res);
	res.end();
});
