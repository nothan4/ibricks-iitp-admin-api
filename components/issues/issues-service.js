import dayjs from 'dayjs';
import * as issuesModel from './issues-model.js';
import { IssueReportTable } from '../../model/index.js';
import * as responseSchema from './config/response-schema.js';
import formidable from 'formidable';
import { isEmpty, uuid } from '../../util/index.js';
import config from 'config';
import fs from 'fs';

/** 10대 이슈 리스트 */
export const getIssues = async (params) => {
	const issuesList = await issuesModel.issues(params);
	const issues = issuesList.searchResult.body.hits.hits;
	const issuesCount = issuesList.searchResult.body.hits?.total.value;

	const result = {
		totalCount: issuesCount,
		dataList: [],
	};

	issues.map((data, index) => {
		result.dataList.push(responseSchema.issues(data._source));
	});

	const response = {
		status: 200,
		message: 'Success',
		result,
	};

	return response;
};

/** 10대 이슈 상세정보 */
export const getIssue = async (issueId) => {
	const issue = await issuesModel.issue(issueId);
	const issueObject = issue.searchResult.body.hits.hits[0]._source;
	// Response 정의
	const result = responseSchema.issue(issueObject);

	const response = {
		status: 200,
		message: 'Success',
		result,
	};

	return response;
};

/** 10대 이슈 등록 */
export const insertIssue = async (req) => {
	const uploadPath = config.get('upload.issueFilePath');
	const option = {
		/** 파일생성 경로 */
		uploadDir: uploadPath,
		/** 파일 확장자 유무 */
		keepExtensions: false,
	};
	const form = new formidable.IncomingForm(option);
	const params = await new Promise(function (resolve, reject) {
		form.parse(req, function (err, fields, files) {
			const oldFile = files.issueFile.filepath;
			const originalFilename = files.issueFile.originalFilename;
			const fileId = uuid();
			const fileExt = originalFilename
				.substring(originalFilename.lastIndexOf('.') + 1, originalFilename.length)
				.toLowerCase();
			const newFile = `${uploadPath}${fileId}.${fileExt}`;
			fs.rename(oldFile, newFile, function (err) {
				if (err) throw err;
			});

			if (err) {
				reject(err);
				return;
			}

			const dataObject = {
				issueId: uuid(),
				issueKeywords: fields.issueKeywords,
				publishDate: fields.publishDate,
				issueTitle: fields.issueTitle,
				summaryContents: fields.summaryContents,
				issueYear: fields.issueYear,
				fileId: fileId,
				filePath: uploadPath,
				fileExt: fileExt,
				originalFilename: originalFilename,
				nowDate: dayjs().format('YYYY-MM-DD HH:mm:ss'),
			};
			resolve(dataObject);
		});
	});

	const response = {
		status: 200,
		message: 'Success',
	};

	// Maraia DB 저장( dbResult = 0:성공)
	const dbResult = await issuesModel.issueDbInsert(params);
	if (dbResult[0] === 0) {
		// EL 등록( elResult = 200: 성공 )
		const elResult = await issuesModel.issueElInsert(params);
	} else {
		response.status = 500;
		response.message = 'Fail';
	}

	return response;
};

/** 10대 이슈 수정 */
export const updateIssue = async (req, issueId) => {
	const uploadPath = config.get('upload.issueFilePath');
	const option = {
		/** 파일생성 경로 */
		uploadDir: uploadPath,
		/** 파일 확장자 유무 */
		keepExtensions: false,
	};
	const form = new formidable.IncomingForm(option);
	const params = await new Promise(function (resolve, reject) {
		form.parse(req, function (err, fields, files) {
			let originalFilename = null;
			let fileId = null;
			let fileExt = null;
			if (!isEmpty(files)) {
				const oldFile = files.issueFile.filepath;
				originalFilename = files.issueFile.originalFilename;
				fileId = uuid();
				fileExt = originalFilename
					.substring(originalFilename.lastIndexOf('.') + 1, originalFilename.length)
					.toLowerCase();
				const newFile = `${uploadPath}${fileId}.${fileExt}`;
				fs.rename(oldFile, newFile, function (err) {
					if (err) throw err;
				});

				if (err) {
					reject(err);
					return;
				}
			}

			const dataObject = {
				issueKeywords: fields.issueKeywords,
				publishDate: fields.publishDate,
				issueTitle: fields.issueTitle,
				summaryContents: fields.summaryContents,
				issueYear: fields.issueYear,
				fileId: isEmpty(files) ? null : fileId,
				filePath: isEmpty(files) ? null : uploadPath,
				fileExt: isEmpty(files) ? null : fileExt,
				originalFilename: isEmpty(files) ? null : originalFilename,
				nowDate: dayjs().format('YYYY-MM-DD HH:mm:ss'),
			};
			resolve(dataObject);
		});
	});

	// Maraia DB 수정( dbResult = 1:성공)
	const dbResult = await issuesModel.issueDbUpdate(params, issueId);
	if (dbResult === 1) {
		// EL 수정( elResult = 200: 성공 )
		const elResult = await issuesModel.issueElUpdate(params, issueId);
	} else {
		response.status = 500;
		response.message = 'Fail';
	}

	const response = {
		status: 200,
		message: 'Success',
	};

	return response;
};

/** 10대 이슈 다운로드 */
export const downloadIssue = async (issueId) => {
	const fileInfo = await issuesModel.getFileDb(issueId);
	const data = fileInfo[0];
	// const file = `${data[IssueReportTable.filePath]}/${data[IssueReportTable.fileId]}.${data[IssueReportTable.fileExt]}`;
	const file = `${data[IssueReportTable.filePath]}${data[IssueReportTable.fileId]}.${data[IssueReportTable.fileExt]}`;
	const resFile = {
		file: file,
		fileName: data[IssueReportTable.originFileName],
	};
	return resFile;
};

/** 10대 이슈 삭제 */
export const deleteIssue = async (issueId) => {
	const response = {
		status: 200,
		message: 'Success',
	};
	// Maraia DB 저장( dbResult = 1:성공)
	const dbResult = await issuesModel.updateByDeleteYnDb(issueId);
	if (dbResult === 1) {
		// EL 수정( elResult = 200: 성공 )
		const elResult = await issuesModel.updateByDeleteYnEl(issueId);
	} else {
		response.status = 500;
		response.message = 'Fail';
	}

	return response;
};
