import dayjs from 'dayjs';
import { IssuesReportIndex } from '../../../model/index.js';

/** 10대 이슈 목록 */
export const issues = (data) => {
	return {
		issuesId: data[IssuesReportIndex.issueId],
		title: data[IssuesReportIndex.title],
		abstract: data[IssuesReportIndex.abstract],
		keywords: data[IssuesReportIndex.keywords],
		fileId: data[IssuesReportIndex.fileId],
		fileExt: data[IssuesReportIndex.fileExt],
		filePath: data[IssuesReportIndex.filePath],
		originFileName: data[IssuesReportIndex.originFileName],
		publishDate: dayjs(data[IssuesReportIndex.publishDate]).format('YYYY-MM-DD'),
		year: data[IssuesReportIndex.year],
		regDt: dayjs(data[IssuesReportIndex.regDate]).format('YYYY-MM-DD'),
	};
};

/** 10대 이슈 상세정보 */
export const issue = (data) => {
	return {
		issuesId: data[IssuesReportIndex.issueId],
		title: data[IssuesReportIndex.title],
		abstract: data[IssuesReportIndex.abstract],
		keywords: data[IssuesReportIndex.keywords],
		fileId: data[IssuesReportIndex.fileId],
		fileExt: data[IssuesReportIndex.fileExt],
		filePath: data[IssuesReportIndex.filePath],
		originFileName: data[IssuesReportIndex.originFileName],
		publishDate: dayjs(data[IssuesReportIndex.publishDate]).format('YYYY-MM-DD'),
		year: data[IssuesReportIndex.year],
		regDt: dayjs(data[IssuesReportIndex.regDate]).format('YYYY-MM-DD'),
	};
};

dayjs().format('YYYY-MM-DD HH:mm:ss');
