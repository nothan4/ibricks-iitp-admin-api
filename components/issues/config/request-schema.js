import * as yup from 'yup';

/* 10대 이슈 관리 목록 Request */
export const issuesList = {
	query: yup.object({
		size: yup.number().positive().integer().default(10),
		page: yup.number().positive().integer().default(1),
		startDate: yup.string().trim(),
		endDate: yup.string().trim(),
		keyword: yup.string().trim(),
	}),
};

/** 10대 이슈 상세정보 */
export const issueId = {
	params: yup.object({
		issueId: yup.string().trim().required(),
	}),
};

/** 10대 이슈 다운로드 */
export const issueIdBody = {
	body: yup.object({
		issueId: yup.string().trim().required(),
	}),
};
