import { ApiModel, IssuesReportIndex } from '../../../model/index.js';

export const issueConfig = () => {
	return {
		[ApiModel.ISSUE_LIST]: {
			index: IssuesReportIndex.index,
			sort: IssuesReportIndex.regDate,
			range: IssuesReportIndex.publishDate,
			simpleQuery: [
				IssuesReportIndex.titleKeyword,
				IssuesReportIndex.titleKobrick,
				IssuesReportIndex.titleNgram,
				IssuesReportIndex.titleStandard,
			],
			source: [
				IssuesReportIndex.issueId,
				IssuesReportIndex.title,
				IssuesReportIndex.abstract,
				IssuesReportIndex.keywords,
				IssuesReportIndex.fileId,
				IssuesReportIndex.fileExt,
				IssuesReportIndex.filePath,
				IssuesReportIndex.originFileName,
				IssuesReportIndex.publishDate,
				IssuesReportIndex.year,
				IssuesReportIndex.regDate,
			],
		},
		[ApiModel.ISSUE_DETAIL]: {
			index: IssuesReportIndex.index,
			filter: IssuesReportIndex.issueId,
			source: [
				IssuesReportIndex.issueId,
				IssuesReportIndex.title,
				IssuesReportIndex.abstract,
				IssuesReportIndex.keywords,
				IssuesReportIndex.fileId,
				IssuesReportIndex.fileExt,
				IssuesReportIndex.filePath,
				IssuesReportIndex.originFileName,
				IssuesReportIndex.publishDate,
				IssuesReportIndex.year,
				IssuesReportIndex.regDate,
			],
		},
	};
};
