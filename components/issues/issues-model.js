import dbConnection from '../../lib/knex/db-connection.js';
import { ApiModel, IssueReportTable, IssuesReportIndex } from '../../model/index.js';
import * as indexConfig from './config/index-config.js';
import { isEmpty } from '../../util/index.js';
import { search, update, write } from '../../lib/elasticsearch/client.js';
import IssueReportIndex from '../../model/index/issue-report-index.js';

/** 필드 정보 */
const issueConfig = indexConfig.issueConfig();

/** 10대 이슈 조회 */
export const issues = async (params) => {
	const { size, page, startDate, endDate, keyword } = params;
	const issueField = issueConfig[ApiModel.ISSUE_LIST];
	const dynamicMustQuery = [];

	// 발행일 기간 조회
	const publishDateQuery =
		isEmpty(startDate) && isEmpty(endDate)
			? null
			: {
					range: {
						[issueField.range]: {
							gte: startDate,
							lte: endDate,
						},
					},
			  };
	if (publishDateQuery != null) dynamicMustQuery.push(publishDateQuery);

	// 제목 검색
	const titleSimpleQuery = isEmpty(keyword)
		? null
		: {
				simple_query_string: {
					query: keyword,
					fields: issueField.simpleQuery,
				},
		  };
	if (titleSimpleQuery != null) dynamicMustQuery.push(titleSimpleQuery);

	// 삭제 제외
	const deleteQuery = {
		match: {
			[IssueReportIndex.deleteFlag]: 'N',
		},
	};
	dynamicMustQuery.push(deleteQuery);

	// 목록 Query
	const issuesListQuery = {
		index: issueField.index,
		body: {
			size: size,
			from: (page - 1) * size,
			query: {
				bool: {
					must: dynamicMustQuery,
				},
			},
			sort: {
				[issueField.sort]: {
					order: 'desc',
				},
			},
			_source: issueField.source,
		},
	};

	const searchResult = await search(issuesListQuery);

	return { searchResult };
};

export const issue = async (issueId) => {
	const issueField = issueConfig[ApiModel.ISSUE_DETAIL];

	/** 공고 정보 Query */
	const issueDetailQuery = {
		index: issueField.index,
		body: {
			query: {
				bool: {
					filter: [
						{
							term: {
								[issueField.filter]: issueId,
							},
						},
					],
				},
			},
			_source: issueField.source,
		},
	};

	const searchResult = await search(issueDetailQuery);

	return { searchResult };
};

/** 10대 이슈 - 등록(MariaDB) */
export const issueDbInsert = async (params) => {
	const {
		issueId,
		issueKeywords,
		publishDate,
		issueTitle,
		summaryContents,
		issueYear,
		fileId,
		filePath,
		fileExt,
		originalFilename,
		nowDate,
	} = params;

	// 등록
	const result = await dbConnection(IssueReportTable.index).insert({
		[IssueReportTable.issueId]: issueId,
		[IssueReportTable.keywords]: issueKeywords,
		[IssueReportTable.publishDate]: publishDate,
		[IssueReportTable.title]: issueTitle,
		[IssueReportTable.abstract]: summaryContents,
		[IssueReportTable.year]: issueYear,
		[IssueReportTable.fileId]: fileId,
		[IssueReportTable.filePath]: filePath,
		[IssueReportTable.fileExt]: fileExt,
		[IssueReportTable.originFileName]: originalFilename,
		[IssueReportTable.regDate]: nowDate,
		[IssueReportTable.modifiDate]: nowDate,
	});

	return result;
};

/** 10대 이슈 - 등록(EL) */
export const issueElInsert = async (params) => {
	const {
		issueId,
		issueKeywords,
		publishDate,
		issueTitle,
		summaryContents,
		issueYear,
		fileId,
		filePath,
		fileExt,
		originalFilename,
		nowDate,
	} = params;

	const index = IssuesReportIndex.index;
	const _id = issueId;
	const payload = {
		[IssuesReportIndex.issueId]: issueId,
		[IssuesReportIndex.title]: issueTitle,
		[IssuesReportIndex.abstract]: summaryContents,
		[IssuesReportIndex.keywords]: String(issueKeywords).split(','),
		[IssuesReportIndex.publishDate]: publishDate,
		[IssuesReportIndex.year]: issueYear,
		[IssuesReportIndex.deleteFlag]: 'N',
		[IssuesReportIndex.fileId]: fileId,
		[IssuesReportIndex.originFileName]: originalFilename,
		[IssuesReportIndex.filePath]: filePath,
		[IssuesReportIndex.fileExt]: fileExt,
		[IssuesReportIndex.regDate]: nowDate,
		[IssuesReportIndex.modifiDate]: nowDate,
	};

	const result = await write(index, '_doc', payload, _id);

	return result;
};

/** 10대 이슈 - 수정(MariaDB) */
export const issueDbUpdate = async (params, issueId) => {
	const {
		issueKeywords,
		publishDate,
		issueTitle,
		summaryContents,
		issueYear,
		fileId,
		filePath,
		fileExt,
		originalFilename,
		nowDate,
	} = params;

	// 수정
	let result = null;
	if (!isEmpty(fileId)) {
		result = await dbConnection(IssueReportTable.index)
			.where(IssueReportTable.issueId, '=', issueId)
			.update({
				[IssueReportTable.keywords]: issueKeywords,
				[IssueReportTable.publishDate]: publishDate,
				[IssueReportTable.title]: issueTitle,
				[IssueReportTable.abstract]: summaryContents,
				[IssueReportTable.year]: issueYear,
				[IssueReportTable.modifiDate]: nowDate,
				[IssueReportTable.fileId]: fileId,
				[IssueReportTable.filePath]: filePath,
				[IssueReportTable.fileExt]: fileExt,
				[IssueReportTable.originFileName]: originalFilename,
			});
	} else {
		result = await dbConnection(IssueReportTable.index)
			.where(IssueReportTable.issueId, '=', issueId)
			.update({
				[IssueReportTable.keywords]: issueKeywords,
				[IssueReportTable.publishDate]: publishDate,
				[IssueReportTable.title]: issueTitle,
				[IssueReportTable.abstract]: summaryContents,
				[IssueReportTable.year]: issueYear,
				[IssueReportTable.modifiDate]: nowDate,
			});
	}

	return result;
};

/** 10대 이슈 - 수정(EL) */
export const issueElUpdate = async (params, issueId) => {
	const {
		issueKeywords,
		publishDate,
		issueTitle,
		summaryContents,
		issueYear,
		fileId,
		filePath,
		fileExt,
		originalFilename,
		nowDate,
	} = params;

	const index = IssuesReportIndex.index;
	const _id = issueId;
	let payload = null;
	if (!isEmpty(fileId)) {
		payload = {
			[IssuesReportIndex.title]: issueTitle,
			[IssuesReportIndex.abstract]: summaryContents,
			[IssuesReportIndex.keywords]: String(issueKeywords).split(','),
			[IssuesReportIndex.publishDate]: publishDate,
			[IssuesReportIndex.year]: issueYear,
			[IssuesReportIndex.deleteFlag]: 'N',
			[IssuesReportIndex.fileId]: fileId,
			[IssuesReportIndex.originFileName]: originalFilename,
			[IssuesReportIndex.filePath]: filePath,
			[IssuesReportIndex.fileExt]: fileExt,
			[IssuesReportIndex.modifiDate]: nowDate,
		};
	} else {
		payload = {
			[IssuesReportIndex.issueId]: issueId,
			[IssuesReportIndex.title]: issueTitle,
			[IssuesReportIndex.abstract]: summaryContents,
			[IssuesReportIndex.keywords]: String(issueKeywords).split(','),
			[IssuesReportIndex.publishDate]: publishDate,
			[IssuesReportIndex.year]: issueYear,
			[IssuesReportIndex.modifiDate]: nowDate,
		};
	}

	const result = await update(index, _id, payload);

	return result;
};

/** 10대 이슈 - 다운로드 정보 Select */
export const getFileDb = async (issueId) => {
	const result = await dbConnection(IssueReportTable.index)
		.where(IssueReportTable.issueId, '=', issueId)
		.select(
			IssueReportTable.fileId,
			IssueReportTable.filePath,
			IssueReportTable.fileExt,
			IssueReportTable.originFileName
		);

	return result;
};

/** 10대 이슈 - 삭제유무 수정(MariaDB) */
export const updateByDeleteYnDb = async (issueId) => {
	const result = await dbConnection(IssueReportTable.index)
		.where(IssueReportTable.issueId, '=', issueId)
		.update({
			[IssueReportTable.deleteFlag]: 'Y',
		});

	return result;
};

/** 10대 이슈 - 삭제유무 수정(El) */
export const updateByDeleteYnEl = async (issueId) => {
	const index = IssuesReportIndex.index;
	const _id = issueId;
	const payload = {
		[IssuesReportIndex.deleteFlag]: 'Y',
	};

	const result = await update(index, _id, payload);

	return result;
};
