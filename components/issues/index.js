import express from 'express';
import * as schema from './config/request-schema.js';
import { validate } from '../../lib/middleware/validate.js';
import * as issuesController from './issues-controller.js';

export const router = express.Router();

/* 10대 이슈 목록 */
router.get(
	'',
	validate(schema.issuesList),
	issuesController.issuesList
	/*
  #swagger.tags = ['issues']
  #swagger.summary = '10대 이슈 목록'
  #swagger.description = '10대 이슈 목록'
  #swagger.parameters['size'] = { 
    in: 'query',
    description: '응답 결과수',
    required: false,
    type: 'integer',
    default: 10
  }
  #swagger.parameters['page'] = { 
    in: 'query',
    description: '페이지',
    required: false,
    type: 'integer',
    default: 1
  }
  #swagger.parameters['startDate'] = { 
    in: 'query',
    description: '발행 시작일(yyyy-mm-dd)',
    required: false,
    type: 'string'
  }
  #swagger.parameters['endDate'] = { 
    in: 'query',
    description: '발행 종료일(yyyy-mm-dd)',
    required: false,
    type: 'string'
  }
  #swagger.parameters['keyword'] = { 
    in: 'query',
    description: '키워드 검색',
    required: false,
    type: 'string'
  }
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS',
      result: {
        itemList: [
          {
            issueId: '',
            issueTitle: '',
            issueKeywords: '',
            publishDate: ''
          }
        ]
      }
    }
  }
  */
);

/* 10대 이슈 상세 정보 */
router.get(
	'/:issueId',
	validate(schema.issueId),
	issuesController.issuesDetail
	/*
  #swagger.tags = ['issues']
  #swagger.summary = '10대 이슈 상세 정보'
  #swagger.description = '10대 이슈 상세 정보'
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS',
      result: {
        itemList: [
          {
            issueId: '',
            issueTitle: '',
            issueKeywords: '',
            publishDate: '',
            summaryContents: ''
          }
        ]
      }
    }
  }
  */
);

/* 10대 이슈 등록 */
router.post(
	'',
	issuesController.issuesInsert
	/*
  #swagger.tags = ['issues']
  #swagger.summary = '10대 이슈 등록'
  #swagger.description = '10대 이슈 등록'
  #swagger.consumes = ['multipart/form-data']
  #swagger.parameters['issueFile'] = {
    in: 'formData',
    type: 'file',
    required: 'true',
    description: '첨부파일',
  }
  #swagger.parameters['body'] = {
    in: 'body',
    description: 'request body',
    schema: {
      issueTitle: '',
      issueKeywords: [''],
      publishDate: '',
      summaryContents: '',
      issueYear: ''
    }
  }
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS',
      result: {
        itemList: [
          {
            issueId: '',
            issueTitle: '',
            issueKeywords: '',
            publishDate: '',
            summaryContents: ''
          }
        ]
      }
    }
  }
  */
);

/* 10대 이슈 수정 */
router.put(
	'/:issueId',
	validate(schema.issueId),
	issuesController.issuesUpdate
	/*
  #swagger.tags = ['issues']
  #swagger.summary = '10대 이슈 수정'
  #swagger.description = '10대 이슈 수정'
  #swagger.consumes = ['multipart/form-data']
  #swagger.parameters['file'] = {
    in: 'formData',
    type: 'file',
    required: 'true',
    description: '첨부파일',
  }
  #swagger.parameters['body'] = {
    in: 'body',
    description: 'request body',
    schema: {
      issueTitle: '',
      issueKeywords: [''],
      publishDate: '',
      summaryContents: ''
    }
  }
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS',
      result: {
        itemList: [
          {
            issueId: '',
            issueTitle: '',
            issueKeywords: '',
            publishDate: '',
            summaryContents: ''
          }
        ]
      }
    }
  }
  */
);

/* 10대 이슈 다운로드 */
router.post(
	'/download',
	validate(schema.issueIdBody),
	issuesController.issuesDownload
	/*
  #swagger.tags = ['issues']
  #swagger.summary = '10대 이슈 다운로드'
  #swagger.description = '10대 이슈 다운로드'
  #swagger.parameters['body'] = {
    in: 'body',
    description: 'request body',
    schema: {
      issueId: ''
    }
  }
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS'
    }
  }
  */
);

/* 10대 이슈 삭제 */
router.patch(
	'/:issueId',
	validate(schema.issueId),
	issuesController.issuesDelete
	/*
  #swagger.tags = ['issues']
  #swagger.summary = '10대 이슈 삭제'
  #swagger.description = '10대 이슈 삭제'
  #swagger.responses[200] = {
    description: 'success',
    schema: {
      status: 200,
      message: 'SUCCESS'
    }
  }
  */
);
