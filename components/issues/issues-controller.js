import { asyncWrapper } from '../../lib/middleware/async-wrapper.js';
import * as issuesService from './issues-service.js';
import path from 'path';

/** 10대 이슈 목록 */
export const issuesList = asyncWrapper(async (req, res, next) => {
	const params = req.validated.query;
	const response = await issuesService.getIssues(params);
	res.send(response);
});

/** 10대 이슈 상세 정보 */
export const issuesDetail = asyncWrapper(async (req, res, next) => {
	const issueId = req.validated.params.issueId;
	const response = await issuesService.getIssue(issueId);
	res.send(response);
});

/** 10대 이슈 등록 */
export const issuesInsert = asyncWrapper(async (req, res, next) => {
	const response = await issuesService.insertIssue(req);
	res.send(response);
});

/** 10대 이슈 수정 */
export const issuesUpdate = asyncWrapper(async (req, res, next) => {
	const issueId = req.validated.params.issueId;
	const response = await issuesService.updateIssue(req, issueId);
	res.send(response);
});

/** 10대 이슈 다운로드 */
export const issuesDownload = asyncWrapper(async (req, res, next) => {
	const issueId = req.validated.body.issueId;
	const response = await issuesService.downloadIssue(issueId);
	res.download(response.file, encodeURIComponent(path.basename(response.fileName)));
});

/** 10대 이슈 삭제 */
export const issuesDelete = asyncWrapper(async (req, res, next) => {
	const issueId = req.validated.params.issueId;
	const response = await issuesService.deleteIssue(issueId);
	res.send(response);
});
