import axios from 'axios';

/** 'GET' || 'POST' */
const METHOD = 'GET' || 'POST';
/** Request TimeOut 30초 */
const TIME_OUT = 30000;

/**
 *
 * @param {METHOD} method
 * @param {string} url
 * @param {Object} params
 * @returns
 */
const baseRequestApi = async (method = METHOD, url, params) => {
	return axios({
		timeout: TIME_OUT,
		method: method,
		url: url,
		data: params,
	});
};

export default baseRequestApi;
