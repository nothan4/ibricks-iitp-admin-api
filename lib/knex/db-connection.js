import knex from 'knex';
import config from 'config';

const dbConfig = {
	client: 'mysql',
	connection: {
		host: config.get('db.host'),
		port: config.get('db.port'),
		user: config.get('db.user'),
		password: config.get('db.pw'),
		database: config.get('db.database'),
	},
};

const dbConnection = knex(dbConfig);

export default dbConnection;
