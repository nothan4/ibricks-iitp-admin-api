import { Client } from '@elastic/elasticsearch';
import config from 'config';
import { BadGatewayError } from '../errors/bad-gateway-error.js';
import { CustomError } from '../errors/custom-error.js';
import { errors as esErrors } from '@elastic/elasticsearch';

import { Logger } from '../logger/logger.js';
const logger = Logger(import.meta.url);

const client = new Client({
	node: config.get('elasticsearch.nodes'),
});

const handleError = (err) => {
	logger.error(`${err.name}: ${err.message}`);
	if (err instanceof esErrors.ResponseError) {
		logger.error(JSON.stringify(err.body.error));
	}
	throw new BadGatewayError();
};

export const search = async (params) => {
	try {
		console.log(JSON.stringify(params));
		const result = await client.search(params);
		return result;
	} catch (err) {
		handleError(err);
	}
};

export const msearch = async (params) => {
	try {
		const result = await client.msearch(params);
		return result;
	} catch (err) {
		handleError(err);
	}
};

export const doc = async (params) => {
	try {
		const result = await client.get(params);
		return result;
	} catch (err) {
		if (err.statusCode === 404) {
			throw new CustomError(404, 'Not Found Data ID: ' + params.id);
		} else {
			handleError(err);
		}
	}
};

export const write = async (index, docType, payload, _id) => {
	const param = {
		refresh: true,
		index: index,
		type: docType,
		id: _id,
		body: payload,
		refresh: true,
	};

	try {
		const result = await client.index(param);
		return result;
	} catch (err) {
		logger.error('could not write querylog');
		handleError(err);
	}
};

// export const scroll = async (mode, scroll, scrollId) => {
//   var param = {
//     scrollId: scrollId,
//     rest_total_hits_as_int: constants.es_version === 7 ? true : undefined,
//     scroll: scroll,
//   };

//   return client[mode].scroll(param).catch((err) => {
//     handleError(err);
//   });
// };

export const scroll = async (scroll, scrollId) => {
	var param = {
		scrollId: scrollId,
		scroll: scroll,
	};

	return client.scroll(param).catch((err) => {
		handleError(err);
	});
};

/** EL Update */
export const update = async (index, _id, payload) => {
	const param = {
		index: index,
		id: _id,
		body: {
			doc: payload,
		},
		refresh: true,
	};

	try {
		const result = await client.update(param);
		return result;
	} catch (err) {
		logger.error('could not el update');
		handleError(err);
	}
};

/** EL Upsert */
export const upsert = async (index, _id, payload) => {
	const param = {
		index: index,
		id: _id,
		body: {
			doc: payload,
			doc_as_upsert: true,
		},
		refresh: true,
	};

	try {
		const result = await client.update(param);
		return result;
	} catch (err) {
		logger.error('could not el update');
		handleError(err);
	}
};

/** EL Bulk updateByQuery */
export const updateByQuery = async (index, payload) => {
	const param = {
		index: index,
		body: payload,
		conflicts: 'proceed',
		refresh: true,
	};

	try {
		console.log(JSON.stringify(param));
		const result = await client.updateByQuery(param);
		return result;
	} catch (err) {
		logger.error('could not el updateByQuery');
		handleError(err);
	}
};

/** Delete */
export const elDelete = async (index, _id) => {
	const param = {
		index: index,
		id: _id,
		refresh: true,
	};

	try {
		const result = await client.delete(param);
		return result;
	} catch (err) {
		logger.error('could not el delete');
		handleError(err);
	}
};

/** SELECT - COUNT */
export const searchCount = async (index, payload) => {
	const param = {
		index: index,
		body: payload,
	};
	try {
		const result = await client.count(param);
		return result;
	} catch (err) {
		logger.error('could not el count');
		handleError(err);
	}
};
