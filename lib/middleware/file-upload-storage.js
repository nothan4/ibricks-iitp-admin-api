import multer from 'multer';
import config from 'config';
import { uuid } from '../../util/index.js';

const uploadPath = config.get('upload.temp');
const fileUploadStorage = multer.diskStorage({
	destination: function (req, file, cb) {
		cb(null, uploadPath);
	},
	filename: function (req, file, cb) {
		const fileId = uuid();
		const originalName = file.originalname;
		const fileExt = originalName.substring(originalName.lastIndexOf('.') + 1, originalName.length).toLowerCase();
		const newFile = `${fileId}.${fileExt}`;
		cb(null, newFile);
	},
});

export default fileUploadStorage;
