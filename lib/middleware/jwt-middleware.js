import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';
dotenv.config();
import { findByUser } from '../../components/accounts/accounts-model.js';
import { CustomError } from '../errors/custom-error.js';
import { isEmpty } from '../../util/index.js';
import { ErrorMessage } from '../../message/index.js';

export default async (req, res, next) => {
	try {
		console.log(req.header['x-forwarded-for'] || req.connection.remoteAddress);
		console.log(req.cookies);
		if (req.headers.authorization) {
			let userId;
			let auth;
			jwt.verify(req.headers.authorization.split(' ')[0], process.env.ACCESS_TOKEN_SECRET, (err, payload) => {
				if (err) {
					throw new CustomError(ErrorMessage.EXPIRATION_TOKEN.status, ErrorMessage.EXPIRATION_TOKEN.message);
				}
				userId = payload.userId;
				auth = payload.auth;
			});
			const userRes = await findByUser(userId);
			const user = userRes.searchResult.body.hits.hits[0]._source;

			if (isEmpty(user)) {
				// 사용자를 찾을 수 없습니다
				throw new CustomError(ErrorMessage.EXPIRATION_TOKEN.status, ErrorMessage.EXPIRATION_TOKEN.message);
			}

			if (auth) {
				if (req.path.indexOf('/demand/notice') > -1 && auth.indexOf('DEMAND_NOTICE') === -1) {
					throw new CustomError(ErrorMessage.FORBIDDEN.status, ErrorMessage.FORBIDDEN.message);
				}

				if (req.path.indexOf('/demand/survey') > -1 && auth.indexOf('DEMAND_SURVEY') === -1) {
					throw new CustomError(ErrorMessage.FORBIDDEN.status, ErrorMessage.FORBIDDEN.message);
				}

				if (req.path.indexOf('/ict-codes') > -1 && auth.indexOf('ICT_CODE') === -1) {
					throw new CustomError(ErrorMessage.FORBIDDEN.status, ErrorMessage.FORBIDDEN.message);
				}

				if (req.path.indexOf('/issues') > -1 && auth.indexOf('ISSUE') === -1) {
					throw new CustomError(ErrorMessage.FORBIDDEN.status, ErrorMessage.FORBIDDEN.message);
				}

				if (req.path.indexOf('/users') > -1 && auth.indexOf('ACCOUNT_ADMIN') === -1) {
					throw new CustomError(ErrorMessage.FORBIDDEN.status, ErrorMessage.FORBIDDEN.message);
				}

				if (req.path.indexOf('/guests') > -1 && auth.indexOf('ACCOUNT_GUEST') === -1) {
					throw new CustomError(ErrorMessage.FORBIDDEN.status, ErrorMessage.FORBIDDEN.message);
				}
			}
		} else {
			// 잘못된 접근입니다. 로그인 후 이동하여 주십시오
			throw new CustomError(ErrorMessage.EXPIRATION_TOKEN.status, ErrorMessage.EXPIRATION_TOKEN.message);
		}
		next();
	} catch (e) {
		next(e);
	}
};
