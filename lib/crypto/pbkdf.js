import crypto from 'crypto';
import util from 'util';
import { isEmpty } from '../../util/index.js';

const pbkdf2Encryption = async (password, saltParam) => {
	try {
		const randomBytesPromise = util.promisify(crypto.randomBytes);
		const pbkdf2Promise = util.promisify(crypto.pbkdf2);

		const buf = await randomBytesPromise(64);
		const salt = isEmpty(saltParam) ? buf.toString('base64') : saltParam;
		const hashedPassword = await pbkdf2Promise(password, salt, 160704, 64, 'sha512');
		const user = {
			salt: salt,
			password: hashedPassword.toString('base64'),
		};
		return user;
	} catch (err) {
		throw err;
	}
};

export { pbkdf2Encryption };
