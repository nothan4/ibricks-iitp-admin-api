/** UUID */
export { default as uuid } from './uuid.js';

/** UTIL */
export { default as isEmpty } from './is-empty.js';
