/** 빈값 체크
 * [], {} 도 빈값으로 처리
 * @returns {boolean} true: 빈값
 */
const isEmpty = (value) => {
	if (
		value == '' ||
		value == null ||
		value == undefined ||
		(value != null && typeof value == 'object' && !Object.keys(value).length)
	) {
		return true;
	} else {
		return false;
	}
};

export default isEmpty;
